<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Order;

class OrderCancel
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $user, $cancelOrder, $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $cancelOrder
     * @param string $message
     */
    public function __construct(User $user, Order $cancelOrder, $message = '')
    {
        $this->cancelOrder = $cancelOrder;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * @return Order
     */

   public function getCancelOrder ()
   {
       return $this->cancelOrder;
   }

    /**
     * @return User
     */


   public function getUser ()
   {
       return $this->user;
   }

    /**
     * @return string
     */
   public function getMessage ()
   {
       return $this->message;
   }
}
