<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Models\User;
use App\Models\Order;

class OrderDelivered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user, $deliveredOrder, $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $deliveredOrder
     * @param string $message
     */
    public function __construct(User $user, Order $deliveredOrder, $message = '')
    {
        $this->deliveredOrder = $deliveredOrder;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * @return User
     */


    public function getUser ()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return Order
     */
    public function getDeliveredOrder()
    {
        return $this->deliveredOrder;
    }
}
