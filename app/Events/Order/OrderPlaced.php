<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Order;

class OrderPlaced
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user, $placedOrder;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $placedOrder
     */
    public function __construct(User $user, Order $placedOrder)
    {
        $this->placedOrder = $placedOrder;
        $this->user = $user;
    }

    /**
     * @return Order
     */

    public function getPlacedOrder ()
    {
        return $this->placedOrder;
    }

    /**
     * @return User
     */


    public function getUser ()
    {
        return $this->user;
    }
}
