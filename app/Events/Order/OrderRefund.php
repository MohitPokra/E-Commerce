<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Order;

class OrderRefund
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user, $refundedOrder, $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $refundedOrder
     * @param string $message
     */
    public function __construct(User $user, Order $refundedOrder, $message = '')
    {
        $this->refundedOrder = $refundedOrder;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * @return Order
     */

    public function getRefundedOrder ()
    {
        return $this->refundedOrder;
    }

    /**
     * @return User
     */
    public function getUser ()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}
