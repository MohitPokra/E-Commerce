<?php

namespace App\Events\Order;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Models\User;
use App\Models\Order;

class OrderShipped
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user, $shippedOrder, $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Order $shippedOrder
     * @param string $message
     */
    public function __construct(User $user, Order $shippedOrder, $message = '')
    {
        $this->shippedOrder = $shippedOrder;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * @return Order
     */

    public function getShippedOrder ()
    {
        return $this->shippedOrder;
    }

    /**
     * @return User
     */


    public function getUser ()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
