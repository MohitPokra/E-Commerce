<?php

namespace App\Http\Controllers\Api\Auth;

use App\Support\Enum\UserStatus;
use Auth;
use Exception;
use Laravel\Socialite\Facades\Socialite;
use App\Events\User\LoggedIn;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Auth\Social\ApiAuthenticateRequest;
use App\Repositories\User\UserRepository;
use App\Services\Auth\Social\SocialManager;
use App\Models\User;
use Carbon\Carbon;

class SocialLoginController extends ApiController
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var SocialManager
     */
    private $socialManager;

    /**
     * @param UserRepository $users
     * @param SocialManager $socialManager
     */
    public function __construct(UserRepository $users, SocialManager $socialManager)
    {
        $this->users = $users;
        $this->socialManager = $socialManager;
    }

    public function index(ApiAuthenticateRequest $request)
    {
        try {
            $socialUser = Socialite::driver($request->network)->userFromToken($request->social_token);
        } catch (Exception $e) {
            return $this->errorInternalError("Could not connect to specified social network.");
        }

        $user = $this->users->findBySocialId(
            $request->network,
            $socialUser->getId()
        );

        if (! $user) {
            if (! settings('reg_enabled')) {
                return $this->errorForbidden("Only users who already created an account can log in.");
            }

            $user = $this->socialManager->associate($socialUser, $request->network);
        }

        if ($user->isBanned()) {
            return $this->errorForbidden("Your account is banned by administrators.");
        }

        $token = Auth::guard('api')->login($user);

        event(new LoggedIn);

        return $this->respondWithArray([
            'token' => $token
        ]);
    }

//
//    public function facebookHandleProviderCallback($provider) {
//
//        $loggedInUser =  Socialite::driver($provider)->fields([
//            'first_name', 'last_name', 'email', 'gender', 'birthday', 'location', 'hometown'
//        ])->stateless()->user();
//
//        $user = $this->users->findByEmail($loggedInUser->email);
//
//        if ($user && $user->isBanned()) {
//            return $this->errorForbidden("Your account is banned by administrators.");
//        }
//
//        if($user && count($user)) {
//
//            $user->provider = $provider;
//            $user->provider_id = $user->id;
//            if(!$user->avatar && $loggedInUser->avatar) {
//                $user->avatar = $loggedInUser->avatar;
//            }
//            if(!$user->birthday && $loggedInUser->user['birthday']) {
//                $birthday = Carbon::createFromFormat('d/m/Y', $loggedInUser->user['birthday'])->format('Y-m-d');
//                $user->birthday = $birthday;
//            }
//            $user->provider = $provider;
//            $user->provider_id = $loggedInUser->id;
//            $user->save();
//            $token = Auth::guard('api')->login($user);
//
//            return $this->respondWithArray([
//                'token' => $token
//            ]);
//        }
//        else {
//
//            $user = new user();
//            $user->username = $loggedInUser->user['first_name']. ' ' . $loggedInUser->user['last_name'];
//            $user->last_name = $loggedInUser->user['last_name'];
//            $user->first_name = $loggedInUser->user['first_name'];
//            $user->email = $loggedInUser->email;
//            $user->provider = $provider;
//            $user->provider_id = $loggedInUser->id;
//            $user->avatar = $loggedInUser->avatar;
//            $user->status = UserStatus::ACTIVE;
//            if($loggedInUser->user['birthday']) {
//                $birthday = Carbon::createFromFormat('d/m/Y', $loggedInUser->user['birthday'])->format('Y-m-d');
//                $user->birthday = $birthday;
//            }
//            if($loggedInUser->user['hometown'] && $loggedInUser->user['hometown']['name']) {
//                $user->address =  $loggedInUser->user['hometown']['name'];
//            }
//            $user->save();
//            $token = Auth::guard('api')->login($user);
//
//            return $this->respondWithArray([
//                'token' => $token
//            ]);
//
//        }
//    }
//
//    public function handleProviderCallback ($provider) {
//
//        $loggedInUser = Socialite::driver($provider)->user();
//        $user = User::where('email', '=', $loggedInUser->email)->first();
//
//        if ($user && $user->isBanned()) {
//            return $this->errorForbidden("Your account is banned by administrators.");
//        }
//
//        if($user && count($user)) {
//            $user->provider = $provider;
//            $user->provierd_id = $user->id;
//            if(!$user->avatar && $loggedInUser->avatar) {
//                $user->avatar = $loggedInUser->avatar;
//            }
//            $user->provider = $provider;
//            $user->provider_id = $loggedInUser->id;
//            $user->save();
//            $token = Auth::guard('api')->login($user);
//
//            return $this->respondWithArray([
//                'token' => $token
//            ]);
//        }
//        else{
//
//            $array = explode(" ", $loggedInUser->name);
//            $user = new user();
//            $user->username = $loggedInUser->name;
//            $user->last_name = array_pop($array);
//            $user->first_name = implode(" ", $array);
//            $user->email = $loggedInUser->email;
//            $user->provider = $provider;
//            $user->provider_id = $loggedInUser->id;
//            $user->avatar = $loggedInUser->avatar;
//            $user->status = UserStatus::ACTIVE;
//            $user->save();
//
//            $token = Auth::guard('api')->login($user);
//
//            return $this->respondWithArray([
//                'token' => $token
//            ]);
//
//        }
//
//    }

}
