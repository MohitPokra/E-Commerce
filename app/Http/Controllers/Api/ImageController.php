<?php

namespace App\Http\Controllers\Api;

use App\Support\Enum\Imaggable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Requests\Image\ImageDeleteRequest;
use App\Http\Requests\Image\ImageUploadRequest;
use App\Models\Image;

class ImageController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function imageUpload ($request) {

        $immagable = Imaggable::lists();

        if (! array_key_exists($request->type, $immagable)) {
            return response(['message' => 'Type Provided is not valid.'],422);
        }

        // Get UploadedFile
        $uploadedFile = $request->file;
        $folder = $immagable[$request->type]['storageDirectory'];

        // Get image crop data
        if ($request->has('crop') && $request->crop == 'true') {
            $crop = [
                'width' => $request->width,
                'height' => $request->height,
                'x' => $request->x,
                'y' => $request->y
            ];
        }
        else {
            $crop = null;
        }

        // Get Resize Data
        if ($request->has('resize')) {
            if($request->resize == 'true') {
                $resize = [
                    'width' => $request->width,
                    'height' => $request->height,
                ];
            }
            else {
                $resize = null;
            }
        }
        else if ($immagable[$request->type]['resize'] !== null) {
            $resize = $immagable[$request->type]['resize'];
        }
        else {
            $resize = null;
        }

        $newName = self::generateNewFileName($uploadedFile->getClientOriginalExtension());

        $tempPath = storage_path('app/temp/' . $newName);

        // Check if folder exits or not. If not then create the folder
        if(!\File::exists(storage_path('app/public/' . $folder))) {
            \File::makeDirectory(storage_path('app/public/' . $folder), 0775, true);
        }

        // Move file to temporary path.
        $uploadedFile->move(storage_path('app/temp'), $newName);

        // Perform Crop
        if (!empty($crop)) {
            $image = \Image::make($tempPath);

            $image->crop(round($crop['width']), round($crop['height']), round($crop['x']), round($crop['y']));

            $image->save();
        }

        // Perform Resize
        if (!empty($resize)) {
            $image = \Image::make($tempPath);

            $image->resize(round($resize['width']), round($resize['height']));

            $image->save();
        }

        // Move file to original path, and delete temp path file.
        \Storage::putFileAs('public/'.$folder, new \Illuminate\Http\File($tempPath), $newName, 'public');
        \File::delete($tempPath);


        // Get meta type attribute.
        $metaType = $this->getMetaTypeAttribute ($request, $immagable, $folder);

        $fileObj = new Image();
        $fileObj->original_name = $uploadedFile->getClientOriginalName();
        $fileObj->given_name = $newName;
        $fileObj->folder = $folder;
        $fileObj->meta_type = $metaType;

        // For morph relations.
        $fileObj->imaggable_type = $immagable[$request->type]['className'];
        $fileObj->imaggable_id = $request->id;
        $fileObj->save();

        return [
            'name' => $newName,
            'url' => asset_url($folder . '/' . $newName),
        ];
    }

    /**
     * The request always needs to have the attribute type and corresponding id
     * Additionally you can also pass an attribute meta_type which show the meta
     * type of the image in particular type for example 'avatar' can be passed
     * meta_type if type is 'user'. The list of valid type and meta type can be
     * found with help of Imaggable::lists() method.
     * @param ImageUploadRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function upload(ImageUploadRequest $request) {

        if(is_array($request->file)) {
            $returnArray = array();
            foreach ($request->file as $value) {

                $array = new Request();
                $array->type = $request->type;
                $array->id = $request->id;
                $array->file = $value;
                $data = $this->imageUpload($array);
                array_push($returnArray, $data);
            }

            return response()->json(array(
                'status' => 'success',
            ));

        }
        else {
            $data = $this->imageUpload($request);
            return response ($data);
        }

    }

    /**
     * @param ImageDeleteRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(ImageDeleteRequest $request)
    {
        $immagable = Imaggable::lists();

        if (! array_key_exists($request->type, $immagable)) {
            return response(['message' => 'Type Provided is not valid.'],422);
        }
        $folder = $immagable[$request->type]['storageDirectory'];

        $path = storage_path('app/public/'.$folder.'/'.$request->name);

        $image = Image::whereGivenName($request->name)
            ->whereImaggableId($request->id)
            ->delete();
        \File::delete($path);

        return response('Deleted success fully.', 200);
    }


    /**
     * Generate a new unique file name
     * @param $ext
     * @return string
     */
    public static function generateNewFileName($ext)
    {
        $newName = md5(microtime().$ext);

        return ($ext === '') ? $newName : $newName . '.' . $ext;
    }

    private function getMetaTypeAttribute ($request, $immagable, $folder) {

        if($request->meta_type) {
            $metaType = $request->meta_type;
            if(!array_key_exists($immagable[$request->type]['meta_type'], $metaType)) {
                return response('Invalid image type',400);
            }

            $image = Image::where('meta_type', $metaType)
                ->where('imaggable_id', $request->id)
                ->where('immagable_type', $immagable[$metaType]['className'])->first();
            if ($image) {

                $path = storage_path('app/public/' . $folder . '/' . $image->given_name);

                $image->delete();

                \File::delete($path);
            }
        } else {
            $model = (new $immagable[$request->type]['className']())::findOrFail($request->id);

            $existingImages = $model->images()->get()->keyBy('meta_type')->toArray();

            $metaType = '';
            foreach ($immagable[$request->type]['meta_type'] as $availableType) {
                if ($availableType && !array_key_exists($availableType, $existingImages)){
                    $metaType = $availableType;
                    break;
                };
            }

            if(empty($metaType)) {
                if (count($existingImages) > 0) {
                    $firstMetaType = $immagable[$request->type]['meta_type'][0];
                    $path = storage_path('app/public/' . $folder . '/' . $existingImages[$firstMetaType]['given_name']);

                    Image::find($existingImages[$firstMetaType]['id'])->delete();

                    \File::delete($path);
                }

                $metaType = $immagable[$request->type]['meta_type'][0];
            }
        }

        return $metaType;
    }
}
