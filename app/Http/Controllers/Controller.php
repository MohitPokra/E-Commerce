<?php

namespace App\Http\Controllers;

use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;

abstract class  Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data = [];

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    public function __get($name) {
        return $this->data[$name];
    }

    public function __isset($name) {
        return isset($this->data[$name]);
    }

    public function __construct()
    {
        $this->setGlobalData();
    }
    
    private function setGlobalData()
    {
        $categoryRepository = app()->make(CategoryRepository::class);
        $brandRepository = app()->make(BrandRepository::class);
        $productRepository = app()->make(ProductRepository::class);

        $this->categories = $categoryRepository->all();

        $this->brands = $brandRepository->all();

        $this->noImageUrl = asset_url('defaults/noimage.png');

        // Set all data related to the account.
        $this->middleware(function ($request, $next) use($productRepository) {

            $this->account = Auth::check() ? Auth::user() : null;

            if ($this->account !== null) {

                $this->cart = $productRepository->getCartProducts($this->account);

                $this->cartTotal = $this->cart->sum(function($data) {
                    return ($data->on_sale ? $data->sale_price : $data->price) * $data->quantity;
                });
            }

            return $next($request);
        });
    }
}
