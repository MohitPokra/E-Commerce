<?php

namespace App\Http\Controllers\Vendor\MailTracking;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use jdavidbakr\MailTracker\Model\SentEmail;
use jdavidbakr\MailTracker\Model\SentEmailUrlClicked;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Sent email search
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postSearch(Request $request)
    {
        session(['mail-tracker-index-search'=>$request->search]);
        return redirect(route('mailTracker_Index'));
    }

    /**
     * Clear search
     */
    public function clearSearch()
    {
        session(['mail-tracker-index-search'=>null]);
        return redirect(route('mailTracker_Index'));
    }

    /**
     * Index.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        session(['mail-tracker-index-page'=>request()->page]);
        $search = session('mail-tracker-index-search');

        $query = SentEmail::query();

        if($search) {
            $terms = explode(" ",$search);
            foreach($terms as $term) {
                $query->where(function($q) use($term) {
                    $q->where('sender','like','%'.$term.'%')
                        ->orWhere('recipient','like','%'.$term.'%')
                        ->orWhere('subject','like','%'.$term.'%');
                });
            }
        }
        $query->orderBy('created_at','desc');

        $this->emails = $query->paginate(config('mail-tracker.emails-per-page'));

        return \View('emailTrakingViews::index', $this->data);
    }

    /**
     * Show Email.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowEmail($id)
    {
        $this->email = SentEmail::where('id',$id)->first();
        return \View('emailTrakingViews::show', $this->data);
    }

    /**
     * Url Detail.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUrlDetail($id)
    {
        $this->details = SentEmailUrlClicked::where('sent_email_id',$id)->get();
        if(!$this->details) {
            return back();
        }
        return \View('emailTrakingViews::url_detail', $this->data);
    }

    /**
     * SMTP Detail.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSMTPDetail($id)
    {
        $this->details = SentEmail::find($id);
        if(!$this->details) {
            return back();
        }
        return \View('emailTrakingViews::smtp_detail', $this->data);
    }
}
