<?php

namespace App\Http\Controllers\Web;

use App\Models\Address;
use App\Models\Country;
use App\Repositories\Order\OrderRepository;
use App\Support\Enum\OrderStatus;

class AccountController extends SeoController
{
    public function __construct()
    {
        parent::__construct();

        $this->setCounts();
    }

    /**
     * Set count data that need to show in all account pages side bar.
     */
    private function setCounts() {
        $this->middleware( function ($request, $next) {
            $this->orderCount = $this->account->orderCount();
            $this->wishListCount = $this->account->wishListCount();
            return $next($request);
        });
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function reorder()
    {
        $this->meta->setTitle("Test Title");

        return "Reorder page";
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function profile()
    {
        $this->meta->setTitle("Test Title");

        return view('account.profile', $this->data);
    }

    /**
     * Shows the contact page
     *
     * @param OrderRepository $orderRepository
     * @return Response
     */
    public function order(OrderRepository $orderRepository)
    {
        $this->orders = $orderRepository->getAllUserOrders($this->account);
        $this->orderStatus = OrderStatus::lists();
        $this->meta->setTitle("Test Title");

        return view('account.orders', $this->data);
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function tickets()
    {
        $this->meta->setTitle("Test Title");

        return view('account.tickets', $this->data);
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function singleTickets()
    {
        $this->meta->setTitle("Test Title");

        return view('account.single-ticket', $this->data);
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function address()
    {
        $this->meta->setTitle("Test Title");

        $this->countries = Country::all();

        $authUser = auth()->user();

        $this->contactAddress = Address::where('user_id', $authUser->id)
            ->whereIn('type', ['contact', 'same'])
            ->first();

        if($this->contactAddress->type == 'contact') {
            $this->shippingAddress = Address::where('user_id', $authUser->id)
                ->where('type', 'shipping')
                ->first();
        }
        return view('account.address', $this->data);
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function wishList()
    {
        $this->meta->setTitle("Test Title");

        return view('account.wishlist', $this->data);
    }

}
