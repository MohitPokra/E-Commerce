<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Address\AddressUpdateRequest;
use App\Models\Address;
use App\Repositories\Address\AddressRepository;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function __construct(AddressRepository $addressRepository)
    {
        parent::__construct();
        $this->addressRepository = $addressRepository;
    }

    public function index ()
    {
        //
    }

    public function update (AddressUpdateRequest $request)
    {
        $contactType = $request->same_address ? 'same' : 'contact';

        $this->addressRepository->saveCurrentAddress($request, $contactType);

        if ($contactType === 'contact') {
            $this->addressRepository->saveShippingAddress($request);
        }
        else {
            $this->addressRepository->destroyShippingAddress();
        }

        return response('Updated successfully', 200);
    }

    public function destroy ($slug)
    {
        //
    }


}
