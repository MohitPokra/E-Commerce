<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Admin\Brand\BrandCreateRequest;
use App\Http\Requests\Admin\Brand\BrandUpdateRequest;
use App\Models\Brand;
use App\Repositories\Brand\BrandRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BrandsController extends Controller
{
    public function __construct(BrandRepository $brandRepository)
    {
        parent::__construct();
        $this->brandRepository = $brandRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * show brands on admin panel
     */

    public function index () {
        return view('admin.brand.brands', $this->data);
    }

    /**
     * @return mixed
     * return data-table data to admin panel brands page
     * @throws \Exception
     */

    public function ajax_brands () {

        $brands = $this->brands;
        return Datatables::of($brands)
            ->addColumn('actions', function ($row) {
                $brand = json_encode($row->toArray());
                $html = '';
                $html .= ' <button class="btn btn-primary btn-xs" onclick=\'OpenEditModel('.$brand.',"'.$row->slug.'")\' ><span class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                $html .= '<button class="btn btn-danger btn-xs" data-toggle="modal" onclick="OpenDeleteModel(this,\''.$row->slug.'\')" data-target="#delete" data-placement="top"><span class="fa fa-trash"></span></button>';

                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->removeColumn('updated_at')
            ->removeColumn('created_at')
            ->removeColumn('id')
            ->make(true);

    }

    public function store(BrandCreateRequest $request) {

        $data = $request->only('name', 'meta_title', 'meta_description');
        $this->brandRepository->create($data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Brand Created successfully'
        ));
    }

    public function edit($slug, BrandRepository $brandRepository) {

        $this->brand = $brandRepository->findBySlug($slug);
        return view('admin.brand.edit', $this->data);
    }

    public function update (BrandUpdateRequest $request, $slug) {

        $data = $request->only('name', 'slug', 'meta_title', 'meta_description');
        $this->brandRepository->updateBySlug($slug,$data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Brand Updated successfully'
        ));

    }

    public function destroy ($slug) {

        $this->brandRepository->deleteBySlug($slug);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Brand Deleted successfully'
        ));
    }


}
