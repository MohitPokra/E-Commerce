<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Cart\AddToCartRequest;
use App\Http\Requests\Cart\RemoveFromCartRequest;
use App\Models\Cart;
use App\Models\User;
use App\Repositories\Product\ProductRepository;

class CartController extends SeoController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Shows the All product related information to the user cart.
     *
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function cart(ProductRepository $productRepository)
    {
        $this->meta->setTitle("Cart");

        $account = User::find(auth()->user()->id);
        $this->products = $productRepository->getCartProducts($account);
        $this->similarProducts = [];

        return view('cart.cart', $this->data);
    }

    /**
     * Show the Page with complete Product description.
     * @param $slug
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function shopSingle($slug, ProductRepository $productRepository)
    {
        $this->meta->setTitle("Single Product");
        $this->product = $productRepository->findBySlugWithReviews($slug);

        if (auth()->check()) {
            $this->productCart = Cart::where('user_id', auth()->user()->id)
                ->where('product_id', $this->product->id)
                ->first();
        }
        $this->similarProducts = [];

        return view('cart.shop-single', $this->data);
    }

    /**
     * @param AddToCartRequest $request
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function add (AddToCartRequest $request)
    {
        $cart = Cart::where('user_id', auth()->user()->id)
            ->where('product_id', $request->product_id)
            ->first();

        if(empty($cart)) {
            $cart = new Cart();
            $cart->user_id = auth()->user()->id;
            $cart->quantity = $request->has('quantity') ? $request->quantity : 1;
            $cart->product_id = $request->product_id;
            $cart->save();
        }
        else {
            $cart->quantity = $request->has('quantity') ? $request->quantity : 1;
            $cart->save();
        }

        if($request->ajax()) {
            return ['status' => 'success', 'message' => 'Product added to cart successfully.'];
        }
        return back()->with(['status' => 'success', 'message' => 'Product added to cart successfully.']);
    }

    /**
     * @param $id
     * @param RemoveFromCartRequest $request
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function remove ($id, RemoveFromCartRequest $request)
    {
        $user = auth()->user();

        if ( $id == 'all') {
            Cart::where('user_id', $user->id)->delete();
        }
        else {
            $cart = Cart::find($id);
            if ($user->id !== $cart->user_id) {
                return ['status' => 'error', 'message' => 'You are not allowed to delete this item.'];
            }
            $cart->delete();
        }

        if($request->ajax()) {

//            $navbarCart = view('layouts.partials.navbar_cart', compact('cart', 'cartTotal'))->render();
            return ['status' => 'success', 'message' => 'Product removed from cart successfully.'];
        }
        return back()->with(['status' => 'success', 'message' => 'Product removed from cart successfully.']);
    }
}
