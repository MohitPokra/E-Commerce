<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Admin\Category\CategoryCreateRequest;
use App\Http\Requests\Admin\Category\CategoryUpdateRequest;
use App\Http\Requests\Admin\Order\OrderCreateRequest;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CategoriesController extends Controller
{
    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * show all categories on admin panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index () {

        return view('admin.category.categories', $this->data);
    }

    /**
     * return data-table data to admin panel categories
     */

    public function ajax_categories () {

        $categories = $this->categories;

        return Datatables::of($categories)
            ->addColumn('actions', function ($row) {
                $html = '';
                 $category = json_encode($row->toArray());
                $html .= ' <button class="btn btn-primary btn-xs edit-button"  onclick=\'editCategory('.$category.',"'.$row->slug.'")\'><span class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                $html .= '<button class="btn btn-danger btn-xs delete-button" data-toggle="modal" onclick="destroyCategory(this,\''.$row->slug.'\')" data-target="#delete" data-placement="top"><span class="fa fa-trash"></span></button>';

                return $html;
            })
            ->editColumn('is_featured' , function ($row) {
                if($row->is_featured === 1)
                    return 'Yes';
                else
                    return 'No';
            })
            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->removeColumn('updated_at')
            ->removeColumn('created_at')
            ->removeColumn('meta_title')
            ->removeColumn('meta_description')
            ->removeColumn('id')
            ->make(true);

    }

    public function edit($slug, CategoryRepository $categoryRepository) {
        $this->category = $categoryRepository->findBySlug($slug);
        return view('admin.category.edit', $this->data);
    }

    public function update (CategoryUpdateRequest $request, $slug) {

        if(!$request->is_featured) {
            $request['is_featured'] = 0;
        }

        $data = $request->only('name', 'slug', 'is_featured', 'meta_title', 'meta_description');
        $this->categoryRepository->updateBySlug($slug, $data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Category Updated successfully'
        ));
    }

    public function destroy ($slug) {

        $this->categoryRepository->deleteBySlug($slug);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Category Deleted successfully'
        ));
    }

    public function store (CategoryCreateRequest $request) {

        $data = $request->only('name','is_featured', 'meta_title', 'meta_description');
        $this->categoryRepository->create($data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Category Created successfully'
        ));
    }


}
