<?php

namespace App\Http\Controllers\Web;

class ContactController extends SeoController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function show()
    {
        $this->meta->setTitle("Test Title");

        return "Contact Page";
    }

}
