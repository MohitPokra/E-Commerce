<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\Activity\ActivityRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Auth;
use Carbon\Carbon;
use Spatie\Analytics\AnalyticsFacade;
use Spatie\Analytics\Period;


class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ActivityRepository
     */
    private $activities;

    /**
     * DashboardController constructor.
     * @param UserRepository $userRepository
     * @param ActivityRepository $activities
     * @param OrderRepository $orderRepository
     */
    public function __construct(UserRepository $userRepository, ActivityRepository $activities, OrderRepository $orderRepository)
    {
        parent::__construct();
        $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->activities = $activities;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Displays dashboard based on user's role.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->hasRole('Admin')) {
            return $this->adminDashboard();
        }
        return $this->defaultDashboard();
    }

    /**
     * Displays dashboard for admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function adminDashboard()
    {

        $this->usersPerMonth = $this->userRepository->countOfNewUsersPerMonth(
            Carbon::now()->subYear()->startOfMonth(),
            Carbon::now()->endOfMonth()
        );

        $analyticsData = AnalyticsFacade::performQuery(
            new Period(Carbon::yesterday(), Carbon::today()),
            'ga:users,ga:pageviews,ga:bounces',
            ['dimensions' => 'ga:date,ga:pageTitle,ga:country,ga:city,ga:cityId,ga:countryIsoCode']);

        $countryWiseViews = [];
        $this->pageView = 0;
        $this->visitors = 0;
        $this->bounces = 0;
        foreach ($analyticsData['rows'] ?? [] as $dataRow) {
            $this->visitors += (int) $dataRow[6];
            $this->pageView += (int) $dataRow[7];
            $this->bounces += (int) $dataRow[8];
            if(empty($countryWiseViews[(string) $dataRow[5]])) {
                $countryWiseViews[(string) $dataRow[5]] = (int) $dataRow[7];
            }else {
                $countryWiseViews[(string) $dataRow[5]] += (int) $dataRow[7];
            }
        }

        $mapData = [];
        foreach ($countryWiseViews as $key => $value) {
            array_push($mapData, ['key' => strtolower($key), 'value' => $value]);
        }

        $this->mapData = json_encode($mapData);

        $this->todayOrder = $this->orderRepository->todayOrder()->count();
        $this->yesterdayOrder = $this->orderRepository->yesterdayOrder()->count();
        $this->todaySale = $this->orderRepository->todaySale();
        $this->yesterdaySale = $this->orderRepository->yesterdaySale();
        $this->activities = $this->activities->paginateActivities();

        $this->stats = [
            'total' => $this->userRepository->count(),
            'new' => $this->userRepository->newUsersCount(),
            'banned' => $this->userRepository->countByStatus(UserStatus::BANNED),
            'unconfirmed' => $this->userRepository->countByStatus(UserStatus::UNCONFIRMED)
        ];

        $this->latestRegistrations = $this->userRepository->latest(7);

        return view('admin.dashboard.index', $this->data);
    }


    public function recentActivity () {


    }



    public function getSalesData ($date) {


        $data = $this->orderRepository->salePerDayOfMonth($date);

        $labels = array();
        $series = array();

        foreach($data as $key => $em) {
            array_push($labels, $key);
            array_push($series, $em);
        }

        return response()->json(array(
            'labels' =>  json_encode($labels),
            'series' =>  json_encode($series)
        ));

    }

    /**
     * Displays default dashboard for non-admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function defaultDashboard()
    {
        $activities = $this->activities->userActivityForPeriod (
            Auth::user()->id,
            Carbon::now()->subWeeks(2),
            Carbon::now()
        )->toArray();

        return view('dashboard.default', compact('activities'));
    }
}
