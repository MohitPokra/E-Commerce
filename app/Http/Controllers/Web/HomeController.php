<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;

class HomeController extends SeoController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Shows the shop's homepage.
     *
     * @param CategoryRepository $categoryRepository
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, ProductRepository $productRepository)
    {
        $this->meta->setTitle(setting('site_name'));

        \Session::forget(['success', 'error']);

        // Top Categories
        $this->topCategories = $categoryRepository->getHomePageCategories();

        // Featured Products
        $this->featuredProduct = $productRepository->getFeatured(4);

        return view('home', $this->data);
    }

    public function deals()
    {
        return "Deals Page";
    }

    public function shipping()
    {
        return "Shipping policy page";
    }

    public function story()
    {
        return "Our Story";
    }

}
