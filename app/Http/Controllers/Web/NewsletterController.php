<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;

class NewsletterController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Shows the contact page
     *
     * @return Response
     */
    public function subscribe(Request $request)
    {

        return "Subscribed";
    }

}
