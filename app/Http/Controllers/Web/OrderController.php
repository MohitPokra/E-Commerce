<?php

namespace App\Http\Controllers\Web;

use App\Events\Order\OrderCancel;
use App\Events\Order\OrderDelivered;
use App\Events\Order\OrderShipped;
use App\Http\Requests\Admin\Order\OrderCreateRequest;
use App\Http\Requests\admin\Order\OrderStoreRequeset;
use App\Http\Requests\Admin\Order\OrderUpdateRequest;
use App\Http\Requests\Admin\Order\shippingInfoUpdateRequest;
use App\Http\Requests\Admin\Order\statusUpdateRequest;
use App\Http\Requests\Order\AddressUpdateRequest;
use App\Http\Requests\Request;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Country;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Repositories\Address\AddressRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\OrderStatus;
use App\Support\Enum\UserStatus;
use Yajra\DataTables\DataTables;
use App\Models\User;
use Carbon\Carbon;
use DB;

class OrderController extends SeoController
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        parent::__construct();
        $this->orderRepository = $orderRepository;
        $this->orderStatus = OrderStatus::lists();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *  show orders page to admin panel
     */

    public function index () {

        return view('admin.order.orders', $this->data);
    }

    /**
     * @return
     * @throws \Exception
     */
    public function ajax_orders () {

        $orders = $this->orderRepository->getAllOrders();

        return Datatables::of($orders)
            ->addColumn('actions', function ($row) {
                $html = '';
                $html .= '<button class="btn btn-primary btn-xs" onclick="window.location=\''.route("admin.orders.edit", [$row->hash]).'\'"
                                                   ><span
                                                        class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                $html .= '<button class="btn btn-danger btn-xs" data-toggle="modal" onclick="destroyOrder('.$row->id.')"
                                                    data-target="#delete" data-placement="top"><span
                                                        class="fa fa-trash"></span></button>';

                return $html;
            })
            ->editColumn('username', function($row) {
                return  $row->user()->first()->username;
            })
            ->editColumn('email', function($row) {
                return  $row->user()->first()->email;
            })
            ->editColumn('delivery_address', function ($row) {
                $html = '<div class="order-column">'. $row->delivery_address. '</div>';

                return $html;
            })->editColumn('status', function ($row) {
                $html = '';

                if ($row->status == $this->orderStatus['CANCELLED']) {
                    $html .= '<span class="badge badge-dark">Cancelled</span>';
                }
                elseif($row->status == $this->orderStatus['DELIVERED']) {
                    $html .= '<span class="badge badge-success">Delivered</span>';
                }
                elseif($row->status == $this->orderStatus['SHIPPED']) {
                    $html .= '<span class="badge badge-info">Shipped</span>';
                }
                elseif($row->status == $this->orderStatus['UNSHIPPED']) {
                    $html .= '<span class="badge badge-default">Un Shipped</span>';
                }
                elseif($row->status == $this->orderStatus['PENDING']) {
                    $html .= '<span class="badge badge-warning">Pending</span>';
                }
                else {
                    $html .= '<span class="badge badge-warning">Need Address</span>';
                }

                return $html;
            })
            ->editColumn('bill_amount', function($row) {
                $html = '$' .  round($row->bill_amount, 2);

                return $html;
            })
            ->editColumn('order_date', function ($row) {
                $orderDate = Carbon::parse($row->order_date);
                return $orderDate->toFormattedDateString();
            })
            ->editColumn('expected_delivery_date', function ($row) {
                $expectedDeliveryDate = Carbon::parse($row->expected_delivery_date);
                return $expectedDeliveryDate->toFormattedDateString();
            })
            ->editColumn('delivery_date', function ($row) {
                $deliveryDate = Carbon::parse($row->delivery_date);
                return $deliveryDate->toFormattedDateString();
            })
            ->addIndexColumn()
            ->rawColumns(['delivery_address', 'actions', 'status'])
            ->removeColumn('updated_at')
            ->removeColumn('created_at')
            ->removeColumn('amount')
            ->removeColumn('hash')
            ->removeColumn('total_tax')
            ->removeColumn('total_discount')
            ->removeColumn('id')
            ->make(true);
    }

    public function create(UserRepository $user) {

        $this->users  = $user->lists();
        $this->statuses =OrderStatus::lists();

        return view('admin.order.create', $this->data);
    }

    public function edit($hash, UserRepository $userRepository, ProductRepository $productRepository)
    {
        $isAdmin = $userRepository->find(auth()->user()->id)->hasRole('Admin');
        $this->order = Order::where('hash', $hash);

        if($isAdmin) {
            $this->order = $this->order
                ->where('user_id', auth()->user()->id);
        }

        $this->order = $this->order->with(['user', 'address', 'payment'])
            ->firstOrFail();

        $this->orderProducts = $productRepository->getOrderProducts($this->order);
        $this->countries = Country::all();

        $this->addressAvailable = false;

        if (!empty($this->order->address)){
            $this->address = $this->order->address;
            $this->addressAvailable = true;
        }
        $this->disableAddressEdit = !(empty($this->order->address) || ($this->addressAvailable && in_array($this->order->status, [$this->orderStatus['ADDRESS'], $this->orderStatus['PENDING'], $this->orderStatus['UNSHIPPED']])));

        return view('admin.order.edit', $this->data);
    }

    public function update(OrderUpdateRequest $request) {
        $data = [];
        if($request->expected_delivery_date){
            $data['expected_delivery_date'] =  Carbon::parse($request->expected_delivery_date);
        }
        if($request->total_tax){
            $data['total_tax'] =  Carbon::parse($request->total_tax);
        }
        if($request->total_discount){
            $data['total_discount'] =  Carbon::parse($request->total_discount);
        }

        $this->orderRepository->update($request->hash, $data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Order Updated Successfully'
        ));
    }

    public function store(OrderCreateRequest $request) {

        $request['hash'] = substr(strtoupper(bin2hex(random_bytes(16))), 22);
        $request['order_date'] =  Carbon::parse($request->order_date);
        unlink($request['delivery_date']);
        $request['expected_delivery_date'] =  Carbon::parse($request->expected_delivery_date);
        $this->orderRepository->create($request->all());

        return response()->json(array(
            'status' => 'success',
            'message' => 'Order Created Successfully'
        ));

    }

    public function destroy ($id) {

        $this->orderRepository->delete($id);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Order Deleted successfully'
        ));
    }

    /**
     * @param OrderRepository $orderRepository
     * @return array
     * @throws \Exception
     */
    public function checkout (OrderRepository $orderRepository)
    {
        DB::beginTransaction();
        $resp = [];
        $cart = Cart::query();
        $cart = $cart->where('user_id', auth()->user()->id)->get();

        // Check for availability of the products
        foreach ($cart as $key => $item) {
            $product = $item->product;

            if(!($product->is_available && $item->quantity < $product->stock_quantity)) {
                array_push($resp, ['name' => $product->name]);
            };
        }

        // Return if any of the products not in stock.
        if (count($resp) > 0) {
            DB::commit();
            $errorNotification = '<div class="alert alert-danger alert-notification" id="cart-product-error" style="margin-bottom: 30px;"><div>The following product'.((count($resp) > 1) ? 's are': ' is').' out of stock.</div><br><ul>';
            foreach ($resp as $item) {
                $errorNotification .= '<li>'.$item['name'].'</li>';
            }
            $errorNotification .= '</ul><p>Please remove '.(count($resp) > 1 ? 'these' : 'this').' from the cart before checkout.</p></div>';
            return response($errorNotification, 422, []);
        }

        $authUser = auth()->user();

        // Create an empty order.
        $order = $orderRepository->createBlankOrder();

        // Update product stock quantity, Update OrderProduct and calculate order amount.
        foreach ($cart as $key => $item) {
            $product = $item->product;
            $product->stock_quantity -= $item->quantity;

            if($product->stock_quantity <= 1) {
                $product->in_stock = 0;
            }

            $product->save();

            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'quantity' => $item->quantity,
                'amount' => $product->price,
                'on_sale' => $product->on_sale,
                'sale_amount' => $product->sale_price,
                'discount' => 0
            ]);

            $order->amount += ($product->on_sale ? $product->sale_price : $product->price) * $item->quantity;
            $order->total_discount += $product->discount * $item->quantity;
        }

        $order->total_tax = 0;

        $order->bill_amount = ($order->amount - $order->total_discount + $order->total_tax);

        $order->save();

        // Remove items from the cart.
        Cart::where('user_id', $authUser->id)->delete();

        DB::commit();

        \Session::put('success', 'Order stored temporarily, now provide shipping address.');
        // Return with success and order hash for further processing of the order.
        return response()->json(array(
            'status' => 'success',
            'message' => 'Update Address',
            'hash' => $order->hash
        ));
    }

    /**
     * @param $hash
     * @param ProductRepository $productRepository
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addressGet ($hash,UserRepository $userRepository , ProductRepository $productRepository) {

        $isAdmin = $userRepository->find(auth()->user()->id)->hasRole('Admin');
        $this->order = Order::where('hash', $hash);

        if($isAdmin) {
            $this->order = $this->order
                ->where('user_id', auth()->user()->id);
        }

        $this->order = $this->order
            ->where('user_id', auth()->user()->id)
            ->firstOrFail();

        $this->disableAddressEdit = $this->order->status != $this->orderStatus['ADDRESS'];

        $this->shippingAddress = $this->disableAddressEdit ? $this->order->address()->first() : User::find(auth()->user()->id)->shippingAddress();

        // Featured Products
        $this->featuredProduct = $productRepository->getFeatured(4);

        $this->countries = Country::all();

        return view('checkout.address', $this->data);
    }

    /**
     * @param AddressUpdateRequest $request
     * @param AddressRepository $addressRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function addressPost (AddressUpdateRequest $request, AddressRepository $addressRepository)
    {
        $order = Order::where('hash', $request->hash)->first();

        $addressRepository->saveOrderAddress($order, $request);

        if($order->status = $this->orderStatus['ADDRESS']) {
            $order->status = $this->orderStatus['PENDING'];
            $order->save();
        }

        \Session::put('success', 'Address stored successfully.');
        return response()->json(array(
            'status' => 'success',
            'message' => 'Create Payment',
            'hash' => $order->hash
        ));
    }

    /**
     * @param shippingInfoUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function shippingInfoPost (shippingInfoUpdateRequest $request)
    {
        $order = Order::where('hash', $request->hash)->with('user')->first();

        $this->orderRepository->saveShippingInfo($order, $request);

        $order->status = $this->orderStatus['SHIPPED'];
        $order->save();

        event(new OrderShipped($order->user, $order));

        return response()->json(array(
            'status' => 'success',
            'message' => 'Order Shipped Successfully.',
            'hash' => $order->hash
        ));
    }

    /**
     * @param statusUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusUpdate (statusUpdateRequest $request)
    {
        $order = Order::where('hash', $request->hash)->with('user')->first();

        $order->status = $this->orderStatus[strtoupper($request->status)];

        if($request->status === $this->orderStatus['DELIVERED']) {
            $order->delivery_date = Carbon::today();
            $order->expected_delivery_date = Carbon::today();
            event(new OrderDelivered($order->user, $order));
        }
        if($request->status === $this->orderStatus['CANCELLED']) {
            event(new OrderCancel($order->user, $order));
        }

        $order->save();

        return response()->json(array(
            'status' => 'success',
            'message' => 'Order Status Updated Successfully.',
            'hash' => $order->hash
        ));
    }

    /**
     * @param $hash
     * @param ProductRepository $productRepository
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymentGet($hash, ProductRepository $productRepository) {

        $this->order = Order::where('hash', $hash)
            ->with('payment')
            ->where('user_id', auth()->user()->id)
            ->firstOrFail();

        if($this->order->status == $this->orderStatus['ADDRESS']) {
            return redirect()->route('order.address.get', $hash)->withErrors('First Provide Address.');
        }

        $this->disablePayment = $this->order->status != $this->orderStatus['PENDING'];

        // Featured Products
        $this->featuredProduct = $productRepository->getFeatured(4);

        return view('checkout.payment', $this->data);
    }

    /**
     * @param $hash
     * @param ProductRepository $productRepository
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reviewGet($hash, UserRepository $userRepository, ProductRepository $productRepository) {

        $isAdmin = $userRepository->find(auth()->user()->id)->hasRole('Admin');
        $this->order = Order::where('hash', $hash);

        if($isAdmin) {
            $this->order = $this->order
                ->where('user_id', auth()->user()->id);
        }

        $this->order = $this->order
            ->with(['address', 'payment'])
            ->firstOrFail();

        // Featured Products
        $this->featuredProduct = $productRepository->getFeatured(4);

        // Order Products
        $this->orderProducts = $productRepository->getOrderProducts($this->order);

        return view('checkout.review', $this->data);
    }

    /**
     * @param $hash
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function completeGet($hash, UserRepository $userRepository)
    {
        $isAdmin = $userRepository->find(auth()->user()->id)->hasRole('Admin');

        $this->order = Order::where('hash', $hash);

        if($isAdmin) {
            $this->order = $this->order
                ->where('user_id', auth()->user()->id);
        }

        $this->order = $this->order->firstOrFail();

        if(empty($this->order)) {
            return back()->withErrors('Invalid Order.');
        }

        return view('checkout.complete', $this->data);
    }

    /**
     * @param $hash
     * @param UserRepository $userRepository
     * @param ProductRepository $productRepository
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function detailsGet($hash, UserRepository $userRepository, ProductRepository $productRepository) {

        $isAdmin = $userRepository->find(auth()->user()->id)->hasRole('Admin');
        $this->order = Order::where('hash', $hash);

        if($isAdmin) {
            $this->order = $this->order
                ->where('user_id', auth()->user()->id);
        }

        $this->order = $this->order->firstOrFail();

        $this->products = $productRepository->getOrderProducts($this->order);

        return response(['data' => view('partials.orderModal', $this->data)->render()], 200);
    }
}
