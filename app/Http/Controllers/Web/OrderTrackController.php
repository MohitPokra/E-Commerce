<?php

namespace App\Http\Controllers\Web;

use App\Models\Order;
use App\Repositories\Product\ProductRepository;
use App\Http\Controllers\Controller;
use App\Support\Enum\OrderStatus;

class OrderTrackController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->orderStatus = OrderStatus::lists();
    }

    /**
     * @param $hash
     * @param ProductRepository $productRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trackingGet($hash, ProductRepository $productRepository)
    {
        $this->order = Order::where('hash', $hash)->firstOrFail();

        // Order Products
        $this->products = $productRepository->getOrderProducts($this->order);
        return view('tracking.tracking', $this->data);
    }
}
