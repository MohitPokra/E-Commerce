<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Page\PageRepository;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends SeoController
{
    protected $pages;

    /**
     * Constructor that adds in dependencies
     *
     * @param PageRespository $repo
     */
    public function __construct(PageRepository $repo)
    {
        $this->pages = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Renders a particular page.
     */
    public function render($slug)
    {
        $page = $this->pages->findBySlug($slug);

        $data = array();
        $data['title'] = $page->title;
        $data['content'] = $page->content;

        return view('pages.'.$page->layout, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
