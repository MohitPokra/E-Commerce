<?php

namespace App\Http\Controllers\Web;

use App\Events\Order\OrderPlaced;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PayPalCallbackRequest;
use App\Http\Requests\Payment\PayPalPaymentRequest;
use App\Http\Requests\Payment\StripePaymentRequest;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Payment\PaymentRepository;
use App\Support\Enum\OrderStatus;

/** All Paypal Details class **/

use Session;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PayPalPayment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaymentController extends Controller
{
    private $paymentRepository;
    private $apiContext;

    public function __construct(PaymentRepository $paymentRepository)
    {
        parent::__construct();

        $this->paymentRepository = $paymentRepository;

        /** setup PayPal api context **/
        $payPalConf = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential($payPalConf['client_id'], $payPalConf['secret']));
        $this->apiContext->setConfig($payPalConf['settings']);
    }

    /**
     * @param StripePaymentRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function stripePayment (StripePaymentRequest $request)
    {
        $order = Order::where('hash', $request->hash)->first();
        $user = User::find(auth()->user()->id);

        /** @var TYPE_NAME $exception */
        try{

            $response = $user->charge($order->bill_amount * 100, [
                'source' => $request->stripeToken,
                'receipt_email' => $request->stripeEmail,
            ]);

            $this->paymentSuccess($order, $user, 'stripe', $response);

        } catch (\Exception $exception) {
            if(env('APP_DEBUG')) {
                throw $exception;
            }
            return response(['Payment could not be succeeded'], 500);
        }

        \Session::put('success','Payment success');
        return redirect()
            ->route('order.review.get', $order->hash);
    }

    /**
     * @param PayPalPaymentRequest $request
     * @param OrderRepository $orderRepository
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function payPalPayment (PayPalPaymentRequest $request, OrderRepository $orderRepository)
    {
        $order = Order::where('hash', $request->hash)
            ->with('address')
            ->first();
        $orderProduct = $orderRepository->getOrderProducts($request->hash);

        $orderAddress = $order->address;

        \Session::forget(['success', 'error']);
        /** @var TYPE_NAME $exception */
        try{
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');
            $items = [];
            $currency = strtoupper(env('CURRENCY_NAME'));

            foreach ($orderProduct as $oProduct) {
                $item = new Item();
                $item->setName($oProduct->name)
                ->setCurrency($currency)
                    ->setSku($oProduct->sku)
                    ->setQuantity($oProduct->quantity)
                    ->setPrice($oProduct->on_sale ? $oProduct->sale_price : $oProduct->price - $oProduct->discount);
                array_push($items, $item);
            }

            $item = new Item();
            $item->setName('tax')
                ->setCurrency($currency)
                ->setQuantity(1)
                ->setPrice($order->total_tax);
            $item->setName('discount')
                ->setCurrency($currency)
                ->setQuantity(1)
                ->setPrice(-1 * $order->total_discount);
            array_push($items, $item);

            $item_list = new ItemList();
            $item_list->setItems($items);

            $amount = new Amount();
            $amount->setCurrency($currency)
                ->setTotal($order->bill_amount);

//            $details = new Details();
//            $details->setTax($order->total_tax);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription(setting('site_name').' order checkout payment.');

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(route('payments.paypal.callback', $order->hash))
            ->setCancelUrl(route('order.payment.get', $order->hash));

//            $shippingAddress = new ShippingAddress();
//            $shippingAddress->setRecipientName($orderAddress->name)
//                ->setLine1($orderAddress->line1)
//                ->setLine2($orderAddress->line2)
//                ->setCity($orderAddress->city)
//                ->setCountryCode($orderAddress->country)
//                ->setPhone($orderAddress->phone)
//                ->setPostalCode($orderAddress->zip);

            $payPalPayment = new PayPalPayment();
            $payPalPayment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            try {
                $payPalPayment->create($this->apiContext);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {

                if (\Config::get('app.debug')) {
                    return redirect()->route('order.payment.get', $order->hash)
                        ->withErrors(['Connection timeout']);
//                    echo "Exception: " . $ex->getMessage() . PHP_EOL;
//                    $err_data = json_decode($ex->getData(), true);
//                    exit;
                } else {
                    \Session::put('error','');
                    return redirect()->route('order.payment.get', $order->hash)
                        ->withErrors(['Some error occur, sorry for inconvenience']);
                }
            }

            foreach($payPalPayment->getLinks() as $link) {
                if($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            // Add payment ID to session
            Session::put('paypal_payment_id_'.$order->hash, $payPalPayment->getId());
            if(isset($redirect_url)) {

                // redirect to PayPal
                return redirect()->away($redirect_url);
            }

            return redirect()->route('order.payment.get', $order->hash)->withErrors(['Unknown error occurred']);

        } catch (\Exception $exception) {
            return redirect()->route('order.payment.get', $order->hash)->withErrors(['Unknown error occurred']);
        }
    }

    /**
     * @param $hash
     * @param PayPalCallbackRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function payPalCallback ($hash, PayPalCallbackRequest $request)
    {
        // Get the payment ID before session clear
        $payment_id = Session::get('paypal_payment_id_'.$hash);

        // clear the session payment ID
        Session::forget('paypal_payment_id_'.$hash);
        if (empty($request->get('PayerID')) || empty($request->get('token'))) {
            \Session::put('error','Payment failed');
            return redirect()->route('order.payment.get', $hash);
        }
        $payment = PayPalPayment::get($payment_id, $this->apiContext);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));

        //Execute the payment
        $result = $payment->execute($execution, $this->apiContext);

        if ($result->getState() == 'approved') {

            $order = Order::where('hash', $request->hash)->first();
            $user = User::find(auth()->user()->id);

            $this->paymentSuccess($order, $user, 'paypal', $result);

            \Session::put('success','Payment success');
            return redirect()->route('order.review.get', $hash);
        }
        \Session::put('error','Payment failed');
        return redirect()->route('order.payment.get', $hash);
    }

    /**
     * @param Order $order
     * @param User $user
     * @param $paymentMethod
     * @param $response
     * @throws \Exception
     */
    private function paymentSuccess(Order $order, User $user, $paymentMethod, $response) {
        $orderStatus = OrderStatus::lists();

        // Perform Database queries.

        \DB::beginTransaction();

        $payment = new Payment();
        $payment->order_id = $order->id;
        $payment->payment_method = $paymentMethod;
        $payment->status = 'success';
        $payment->response = $response;
        $payment->save();

        $order->status = $orderStatus['UNSHIPPED'];
        $order->save();

        \DB::commit();

        event(new OrderPlaced($user, $order));
    }
}
