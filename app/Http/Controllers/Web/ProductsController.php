<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Admin\Product\ProductCreateRequest;
use App\Http\Requests\Admin\Product\ProductUpdateRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Search\SearchRepository;
use App\Support\Enum\Imaggable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepository;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class ProductsController extends Controller
{
    public  $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        parent::__construct();
        $this->productRepository = $productRepository;
        $this->pageTitle = 'Products';
        $this->metaDescription = 'E-Commerce Site';
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Show products data on admin-panel
     */
    public function index ()
    {
        return view('admin.product.products', $this->data);
    }

    public function edit ($id, CategoryRepository $category, BrandRepository $brand)
    {

       $this->product = Product::where('id', $id)
            ->with(['images' => function ($query) {
                $this->productRepository
                    ->withProductImages($query)
                    ->whereIn('meta_type', Imaggable::lists()['product']['meta_type']);
            }])->first();

        $this->categories = $category->lists();
        $this->brands  = $brand->lists();

        return view('admin.product.edit', $this->data);
    }

    public function create(CategoryRepository $category, BrandRepository $brand)
    {
        $this->categories = $category->lists();
        $this->brands  = $brand->lists();

        return view('admin.product.create', $this->data);
    }

    /**
     * @param ProductRepository $productRepository
     * @return mixed
     * @throws \Exception
     * Return data to products for data-tables
     */
    public function ajax_products (ProductRepository $productRepository)
    {
        $products = $productRepository->all();

        return Datatables::of($products)
            ->addColumn('actions', function ($row) {
                $html = '';
                $html .= '<button class="btn btn-primary btn-xs" onclick="window.location=\''.route("admin.products.edit", [$row->id]).'\'"><span
                                                        class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                $html .= '<button class="btn btn-danger btn-xs" data-toggle="modal" onclick="destroyProduct(this,'.$row->id.')" data-target="#delete" data-placement="top"><span
                                                        class="fa fa-trash"></span></button>';

                return $html;
            })
            ->addIndexColumn()
            ->editColumn('sale_ends_date', function ($row) {
                if($row->sale_ends_date) {
                    return $row->sale_ends_date->toFormattedDateString();
                }

                return null;
            })
            ->editColumn('price', function ($row) {
                $html = '$' . round($row->price, 2);

                if ($row->on_sale == 1) {
                    $html .= ' <em>(on sale at <strong>$' . round($row->sale_price, 2) . '</strong> till <strong>' . $row->sale_ends_date->toFormattedDateString() . '</strong>)</em>';
                }

                return $html;
            })
            ->editColumn('status', function ($row) {
                $html = '';

                if ($row->is_available == 1) {
                    $html .= '<span class="badge badge-success">Available</span>';
                }
                else {
                    $html .= '<span class="badge badge-warning">Not Available</span>';
                }

                $html .= ' ';

                if ($row->in_stock == 1) {
                    $html .= '<span class="badge badge-success">In Stock</span>';
                }
                else {
                    $html .= '<span class="badge badge-danger">Out of Stock</span>';
                }

                return $html;
            })
            ->rawColumns(['status', 'actions', 'price'])
            ->removeColumn('updated_at')
            ->removeColumn('created_at')
            ->removeColumn('id')
            ->removeColumn('slug')
            ->make(true);
    }

    public function store (ProductCreateRequest $request) {

        $request['sale_ends_date'] =  Carbon::parse($request->sale_ends_date);

        if(!$request->in_stock) {
            $request['stock_quantity'] = 0;
        }

        $this->productRepository->create($request->all());

        return response()->json(array(
            'status' => 'success',
            'message' => 'Product Added successfully'
        ));

    }

    public function update (ProductUpdateRequest $request, $slug) {

        $request['sale_ends_date'] =  Carbon::parse($request->sale_ends_date);
        if(!$request->in_stock) {
            $request['in_stock'] = 0;
            $request['stock_quantity'] = 0;
        }
        $this->productRepository->updateBySlug($slug,$request->all());

        return response()->json(array(
            'status' => 'success',
            'message' => 'Product Updated successfully'
        ));
    }

    public function destroy ($slug) {

       $this->productRepository->deleteBySlug($slug);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Product Deleted successfully'
        ));
    }

    /**
     * @param Request $request
     * @param SearchRepository $searchRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function all(Request $request, SearchRepository $searchRepository) {
        \Session::forget(['success', 'error']);
        $page = $request->page ?: 1;
        $searchResults = $searchRepository->search('', 12, $page);

        $this->products = $searchResults->items();

        $this->pagination = [
            'current_page' => $searchResults->currentPage(),
            'last_page' => $searchResults->lastPage(),
            'current_page_url' => $searchResults->resolveCurrentPath(),
        ];

        $this->pageTitle = 'Shop Products';

        if($request->ajax()) {
            return view('shop.ajax', $this->data)->render();
        }
        return view('shop.main', $this->data);
    }

    public function categories(Request $request) {
    }

    public function brands() {}
    public function filter($filter, SearchRepository $searchRepository) {
        \Session::forget(['success', 'error']);

        $this->products = $searchRepository->searchByFilters($filter);
        return view('shop.main', $this->data);
    }

    /**
     * @param $category
     * @param Request $request
     * @param SearchRepository $searchRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function category($category, Request $request, SearchRepository $searchRepository)
    {
        \Session::forget(['success', 'error']);

        $page = $request->page ?: 1;
        $category = Category::where('slug', $category)->first();
        $searchResults = $searchRepository->searchByCategory($category,12, $page);

        $this->products = $searchResults->items();

        $this->pagination = [
            'current_page' => $searchResults->currentPage(),
            'last_page' => $searchResults->lastPage(),
            'current_page_url' => $searchResults->resolveCurrentPath(),
        ];

        $this->pageTitle = $category->name;
        $this->metaDescription = $category->meta_description;

        if($request->ajax()) {
            return view('shop.ajax', $this->data)->render();
        }
        return view('shop.main', $this->data);
    }

    /**
     * @param $brand
     * @param Request $request
     * @param SearchRepository $searchRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function brand($brand, Request $request, SearchRepository $searchRepository) {
        \Session::forget(['success', 'error']);

        $page = $request->page ?: 1;
        $brand = Brand::where('slug', $brand)->first();
        $searchResults = $searchRepository->searchByBrand($brand,12, $page);

        $this->products = $searchResults->items();

        $this->pagination = [
            'current_page' => $searchResults->currentPage(),
            'last_page' => $searchResults->lastPage(),
            'current_page_url' => $searchResults->resolveCurrentPath(),
        ];
        $this->pageTitle = $brand->name;
        $this->metaDescription = $brand->meta_description;

        if($request->ajax()) {
            return view('shop.ajax', $this->data)->render();
        }
        return view('shop.main', $this->data);
    }

    /**
     * @param Request $request
     * @param SearchRepository $searchRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Throwable
     */
    public function search(Request $request, SearchRepository $searchRepository) {
        \Session::forget(['success', 'error']);

        $page = $request->page ?: 1;
        $query = $request->get('query') ? : '';
        $searchResults = $searchRepository->search($query, 12, $page);

        $this->products = $searchResults->items();

        $this->pagination = [
            'current_page' => $searchResults->currentPage(),
            'last_page' => $searchResults->lastPage(),
            'current_page_url' => $searchResults->resolveCurrentPath(),
        ];

        $this->pageTitle = 'Search Results';

        if($request->ajax()) {
            return view('shop.ajax', $this->data)->render();
        }
        return view('shop.main', $this->data);
    }

    public function product($category, $product) {
        \Session::forget(['success', 'error']);

        $this->products = $category.$product;
        return view('shop.main', $this->data);
    }
}
