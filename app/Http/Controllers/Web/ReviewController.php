<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Review\ReviewStoreRequest;
use App\Models\Review;

class ReviewController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store (ReviewStoreRequest $request) {
        $review = new Review();

        $review->product_id = $request->product_id;
        $review->user_id = auth()->user()->id;
        $review->name = $request->name;
        $review->email = $request->email;
        $review->rating = $request->rating;
        $review->subject = $request->subject;
        $review->review = $request->review;
        $review->save();

        return back()->with(['status' => 'success']);
    }
}
