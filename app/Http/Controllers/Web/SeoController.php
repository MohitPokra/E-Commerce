<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use SEOMeta;
use OpenGraph;
use Twitter;

class SeoController extends Controller
{
    protected $meta;
    protected $og;
    protected $twitter;

    public function __construct()
    {
        parent::__construct();
        $this->meta = app('seotools.metatags');
        $this->og = app('seotools.opengraph');
        $this->twitter = app('seotools.twitter');
    }
}
