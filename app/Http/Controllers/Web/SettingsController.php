<?php

namespace App\Http\Controllers\Web;

use App\Events\Settings\Updated as SettingsUpdated;
use Illuminate\Http\Request;
use Settings;
use App\Http\Controllers\Controller;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class SettingsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * show setting page on admin panel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index () {

        return view('admin.settings.shop', $this->data);
    }

    /**
     * Display general settings page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function general()
    {
        return view('settings.general');
    }

    /**
     * Display Authentication & Registration settings page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function auth()
    {
        return view('settings.auth');
    }

    /**
     * Handle application settings update.
     *
     * @param Request $request
     * @return mixed
     */
    public function personalizeSetting(Request $request)
    {
        $textData = null;
        $fileData = null;

        foreach($request->all() as $key => $value) {
            if(!$request->hasFile($key)) {
                $data[$key] = $value;
            }
            else {
                $fileData[$key] = $value;
            }
        }

        if(count($fileData)) {
            $data = $this->imageUpload($fileData, $data);
        }

        unset($data['_token']);
        $this->updateSettings($data);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Setting Updated successfully'
        ));

    }

    /**
     * Handle application settings update.
     *
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $this->updateSettings($request->except("_token"));

        return back()->withSuccess(trans('app.settings_updated'));
    }

     public function imageUpload ($fileData, $data) {

        foreach($fileData as $key => $value) {
                $uploadedFile = $value;
                if(!\File::exists(storage_path('app/public/setting/personalize'))) {
                    \File::makeDirectory(storage_path('app/public/setting/personalize'), 0775, true);
                }
                $uploadedFile->move(storage_path('app/public/setting/personalize'), $key);
                $data[$key] = asset_url('setting/personalize/' . $key);
        }

        return $data;

    }

    /**
     * Update settings and fire appropriate event.
     *
     * @param $input
     */
    private function updateSettings($input)
    {
        foreach ($input as $key => $value) {
            Settings::set($key, $value);
        }

        Settings::save();

        event(new SettingsUpdated);
    }

    /**
     * Enable system 2FA.
     *
     * @return mixed
     */
    public function enableTwoFactor()
    {
        $this->updateSettings(['2fa.enabled' => true]);

        return back()->withSuccess(trans('app.2fa_enabled'));
    }

    /**
     * Disable system 2FA.
     *
     * @return mixed
     */
    public function disableTwoFactor()
    {
        $this->updateSettings(['2fa.enabled' => false]);

        return back()->withSuccess(trans('app.2fa_disabled'));
    }

    /**
     * Enable registration captcha.
     *
     * @return mixed
     */
    public function enableCaptcha()
    {
        $this->updateSettings(['registration.captcha.enabled' => true]);

        return back()->withSuccess(trans('app.recaptcha_enabled'));
    }

    /**
     * Disable registration captcha.
     *
     * @return mixed
     */
    public function disableCaptcha()
    {
        $this->updateSettings(['registration.captcha.enabled' => false]);

        return back()->withSuccess(trans('app.recaptcha_disabled'));
    }

    /**
     * Display notification settings page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notifications()
    {
        return view('settings.notifications');
    }
}
