<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\StaticPage\StaticPageStoreRequest;
use App\Models\StaticPage;
use App\Repositories\StaticPage\StaticPageRepository;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Settings;

class StaticPageController extends Controller
{
    protected $staticPageRepository;

    public function __construct(StaticPageRepository $staticPageRepository)
    {
        Parent::__construct();
        $this->staticPageRepository = $staticPageRepository;
    }

    public function index () {

      return view('admin.settings.index', $this->data);
    }

    public function store (StaticPageStoreRequest $request) {

        $page = StaticPage::create([
            'name' => $request->text,
            'detail' => $request->description,
        ]);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Page Created successfully'
        ));
    }

    public function show ($slug) {

        $page = StaticPage::where('slug', $slug)
            ->first();

        $this->pageTitle = $page->name;
        $this->pageContent = $page->detail;

        return view('staticPage.static_page', $this->data);
    }

    public function edit ($slug) {

        $this->pageData = $this->staticPageRepository->findBySlug($slug);

        return view('admin.settings.edit', $this->data);
    }

    public function update ($slug, StaticPageStoreRequest $request) {

        $page = StaticPage::create([
            'name' => $request->text,
            'detail' => $request->description,
        ]);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Page Updated successfully'
        ));
    }

    public function ajaxStaticPage () {

        $pages = $this->staticPageRepository->all();

        return Datatables::of($pages)
            ->addColumn('actions', function ($row) {
                $html = '';
                $html .= '<button class="btn btn-primary btn-xs" onclick="window.location=\''.route("admin.setting.static.edit", [$row->slug]).'\'"><span
                                                        class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                $html .= '<button class="btn btn-danger btn-xs delete-button" data-toggle="modal" onclick="destroyPage(this,\''.$row->slug.'\')" data-target="#delete" data-placement="top"><span class="fa fa-trash"></span></button>';
                return $html;

            })
            ->addColumn('url', function($row) {
                $html = '<a href="'.url("/pages/{$row->slug}").'">'.url("/pages/{$row->slug}").'</a>';
                return $html;

            })
            ->editColumn('name', function($row) {
                return ucwords($row->name);
            })
            ->addIndexColumn()
            ->rawColumns(['actions', 'url'])
            ->removeColumn('id')
            ->removeColumn('slug')
            ->removeColumn('detail')
            ->removeColumn('id')
            ->make(true);

    }

    public function create () {

        return view('admin.settings.create', $this->data);
    }

    public function destroy ($slug) {

        $this->staticPageRepository->deleteBySlug($slug);

        return response()->json(array(
            'status' => 'success',
            'message' => 'Page Deleted successfully'
        ));
    }
}
