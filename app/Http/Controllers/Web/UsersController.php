<?php

namespace App\Http\Controllers\Web;

use App\Events\User\Banned;
use App\Events\User\Deleted;
use App\Events\User\TwoFactorDisabledByAdmin;
use App\Events\User\TwoFactorEnabledByAdmin;
use App\Events\User\UpdatedByAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\EnableTwoFactorRequest;
use App\Http\Requests\User\UpdateDetailsRequest;
use App\Http\Requests\User\UpdateLoginDetailsRequest;
use App\Models\Category;
use App\Models\Country;
use App\Repositories\Activity\ActivityRepository;
use App\Repositories\Country\CountryRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\UserRepository;
use App\Services\Upload\UserAvatarManager;
use App\Support\Enum\UserStatus;
use App\Models\User;
use App\Models\Role;
use Auth;
use Authy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * UsersController constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        parent::__construct();
        $this->middleware('auth');
//        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
//        $this->middleware('permission:users.manage');
        $this->users = $users;
    }

    /**
     * Display paginated list of all users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.customer.index', $this->data);
    }

    public function ajax_customers () {

            $customers = $this->users->all();

            return Datatables::of($customers)
                ->addColumn('actions', function ($row) {
                    $html = '';
                    $html .= ' <button class="btn btn-primary btn-xs" onclick="window.location=\''.route("admin.customers.edit", [$row->id]).'\'"><span
                                                        class="fa fa-pencil"></span></button>&nbsp;&nbsp;';
                    $html .= '<button class="btn btn-danger btn-xs delete-button" data-toggle="modal" onclick="destroyCustomer('.$row->id.')"
                                                    data-target="#delete" data-placement="top"><span
                                                        class="fa fa-trash"></span></button>';

                    return $html;
                })
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return $row->first_name . ' ' . $row->last_name;
                })
                ->editColumn('role', function($row) {
                    return $row->role->display_name;
                })
                ->editColumn('last_login', function($row) {
                    if (!$row->last_login) {
                        return '-';
                    }
                    else {
                        return $row->last_login->toFormattedDateString();
                    }
                })
                ->editColumn('status', function($row) {
                    $html = '';

                    if ($row->status == 'Active') {
                        $html .= '<span class="badge badge-success">Active</span>';
                    }
                    else {
                        $html .= '<span class="badge badge-warning">Inactive</span>';
                    }

                    return $html;
                })
                ->editColumn('avatar', function($row) {
                    $imageUrl = $row->avatar;

                    if(!$imageUrl){
                        $imageUrl = asset_url('defaults/profile_image.jpg');
                    }

                    return '<img class="customer-avatar" src="' . $imageUrl . '">';
                })
                ->rawColumns(['avatar', 'actions', 'status'])
                ->removeColumn('created_at')
                ->removeColumn('updated_at')
                ->removeColumn('remember_token')
                ->removeColumn('confirmation_token')
                ->removeColumn('provider')
                ->removeColumn('provider_id')
                ->removeColumn('id')
                ->make(true);

    }

    /**
     * Displays user profile page.
     *
     * @param User $user
     * @param ActivityRepository $activities
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(User $user, ActivityRepository $activities)
    {
        $userActivities = $activities->getLatestActivitiesForUser($user->id, 10);

        return view('user.view', compact('user', 'userActivities'));
    }

    /**
     * Displays form for creating a new user.
     *
     * @param CountryRepository $countryRepository
     * @param RoleRepository $roleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(CountryRepository $countryRepository, RoleRepository $roleRepository)
    {
        $this->countries = $this->parseCountries($countryRepository);
        $this->roles = $roleRepository->lists();
        $this->statuses = UserStatus::lists();

        return view('admin.customer.create', $this->data);
    }

    /**
     * Parse countries into an array that also has a blank
     * item as first element, which will allow users to
     * leave the country field unpopulated.
     * @param CountryRepository $countryRepository
     * @return array
     */
    private function parseCountries(CountryRepository $countryRepository)
    {
        return [0 => 'Select a Country'] + $countryRepository->lists()->toArray();
    }

    /**
     * Stores new user into the database.
     *
     * @param CreateUserRequest $request
     * @return mixed
     */
    public function store(UserAvatarManager $avatarManager, CreateUserRequest $request)
    {
        // When user is created by administrator, we will set his
        // status to Active by default.
        $request['birthday'] =  Carbon::parse($request->birthday);

        if(!$request->reward_amount) {
            $request->reward_amount = 0;
        }

        $data = $request->all() + ['status' => UserStatus::ACTIVE];

        if (! array_get($data, 'country_id')) {
            $data['country_id'] = null;
        }

        // Username should be updated only if it is provided.
        // So, if it is an empty string, then we just leave it as it is.
        if (trim($data['username']) == '') {
            $data['username'] = null;
        }

        $user = $this->users->create($data);

        if($request->file('avatar')) {
            $name = $avatarManager->uploadAndCropAvatar(
                $user,
                $request->file('avatar'),
                $request->get('points')
            );

            $name = asset_url('upload/users/'.$name);
            $user = $this->users->update($user->id,['avatar' => $name]);
        }

        return response()->json(array(
            'status' => 'success',
            'message' => 'User Created successfully'
        ));
    }

    /**
     * Displays edit user form.
     *
     * @param User $user
     * @param CountryRepository $countryRepository
     * @param RoleRepository $roleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user, CountryRepository $countryRepository, RoleRepository $roleRepository, $id)
    {
        $this->edit = true;
        $this->countries = $this->parseCountries($countryRepository);
        $this->roles = $roleRepository->lists();
        $this->statuses = UserStatus::lists();
        $this->socialLogins = $this->users->getUserSocialLogins($user->id);
        $this->user = $this->users->find($id);

        return view(
            'admin.customer.edit',
           $this->data
        );
    }


    /**
     * Updates user details.
     *
     * @param User $user
     * @param UpdateDetailsRequest $request
     * @return mixed
     */
    public function updateDetails(UpdateDetailsRequest $request, UserAvatarManager $avatarManager,$id)
    {

        $request['birthday'] =  Carbon::parse($request->birthday);

        if(!$request->reward_amount) {
            $request->reward_amount = 0;
        }

        $data = $request->all();
        $user = $this->users->find($id);
        if (! array_get($data, 'country_id')) {
            $data['country_id'] = null;
        }


        if($request->file('avatar') && $user->avatar) {
            $avatarManager->deleteAvatarIfUploaded($user);
        }

        $this->users->update($user->id, $data);

        $this->users->setRole($user->id, $request->role_id);

        if($request->file('avatar')) {

            $name = $avatarManager->uploadAndCropAvatar(
                $user,
                $request->file('avatar'),
                $request->get('points')
            );

            $name = asset_url('upload/users/'.$name);
            $user = $this->users->update($user->id,['avatar' => $name]);
        }

//        event(new UpdatedByAdmin($user));

        // If user status was updated to "Banned",
        // fire the appropriate event.
        if ($this->userIsBanned($user, $request)) {
            event(new Banned($user));
        }

        return response()->json(array(
            'status' => 'success',
            'message' => 'User Updated successfully'
        ));

    }

    /**
     * Check if user is banned during last update.
     *
     * @param User $user
     * @param Request $request
     * @return bool
     */
    private function userIsBanned(User $user, Request $request)
    {
        return $user->status != $request->status && $request->status == UserStatus::BANNED;
    }

    /**
     * Update user's avatar from uploaded image.
     *
     * @param User $user
     * @param UserAvatarManager $avatarManager
     * @return mixed
     */
    public function updateAvatar(User $user, UserAvatarManager $avatarManager, Request $request)
    {
        $this->validate($request, ['avatar' => 'image']);

        $name = $avatarManager->uploadAndCropAvatar(
            $user,
            $request->file('avatar'),
            $request->get('points')
        );

        if ($name) {
            $this->users->update($user->id, ['avatar' => $name]);

            event(new UpdatedByAdmin($user));

            return redirect()->route('user.edit', $user->id)
                ->withSuccess(trans('app.avatar_changed'));
        }

        return redirect()->route('user.edit', $user->id)
            ->withErrors(trans('app.avatar_not_changed'));
    }

    /**
     * Update user's avatar from some external source (Gravatar, Facebook, Twitter...)
     *
     * @param User $user
     * @param Request $request
     * @param UserAvatarManager $avatarManager
     * @return mixed
     */
    public function updateAvatarExternal(User $user, Request $request, UserAvatarManager $avatarManager)
    {
        $avatarManager->deleteAvatarIfUploaded($user);

        $this->users->update($user->id, ['avatar' => $request->get('url')]);

        event(new UpdatedByAdmin($user));

        return redirect()->route('user.edit', $user->id)
            ->withSuccess(trans('app.avatar_changed'));
    }

    /**
     * Update user's login details.
     *
     * @param User $user
     * @param UpdateLoginDetailsRequest $request
     * @return mixed
     */
    public function updateLoginDetails($id, UpdateLoginDetailsRequest $request)
    {
        $data = $request->all();
        $user = $this->users->find($id);

        if (trim($data['password']) == '') {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $this->users->update($user->id, $data);

        event(new UpdatedByAdmin($user));
        return response()->json(array(
            'status' => 'success',
            'message' => 'User Login Details Updated successfully'
        ));
    }

    /**
     * Removes the user from database.
     *
     * @param User $user
     * @return $this
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->id == Auth::id()) {
            return redirect()->route('user.list')
                ->withErrors(trans('app.you_cannot_delete_yourself'));
        }

        $this->users->delete($user->id);

//        event(new Deleted($user));

        return response()->json(array(
            'status' => 'success',
            'message' => 'User Deleted successfully'
        ));
    }

    /**
     * Enables Authy Two-Factor Authentication for user.
     *
     * @param User $user
     * @param EnableTwoFactorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function enableTwoFactorAuth(User $user, EnableTwoFactorRequest $request)
    {

        if (Authy::isEnabled($user)) {
            return response()->json(array(
                'status' => 'error',
                'message' => 'Two factor authentication is already enabled'
            ));
        }

        $user->setAuthPhoneInformation($request->country_code, $request->phone_number);

        Authy::register($user);

        $user->save();

        event(new TwoFactorEnabledByAdmin($user));

        return response()->json(array(
            'status' => 'success',
            'message' => 'Two factor authentication added successfully'
        ));
    }

    /**
     * Disables Authy Two-Factor Authentication for user.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function disableTwoFactorAuth(User $user)
    {
        if (! Authy::isEnabled($user)) {
            return response()->json(array(
                'status' => 'error',
                'message' => 'Two factor authentication is already disabled'
            ));
        }

        Authy::delete($user);

        $user->save();

        event(new TwoFactorDisabledByAdmin($user));

        return response()->json(array(
            'status' => 'success',
            'message' => 'Two factor authentication disabled successfully'
        ));

    }


    /**
     * Displays the list with all active sessions for selected user.
     *
     * @param User $user
     * @param SessionRepository $sessionRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sessions(User $user, SessionRepository $sessionRepository)
    {
        $adminView = true;
        $sessions = $sessionRepository->getUserSessions($user->id);

        return view('user.sessions', compact('sessions', 'user', 'adminView'));
    }

    /**
     * Invalidate specified session for selected user.
     *
     * @param User $user
     * @param $session
     * @param SessionRepository $sessionRepository
     * @return mixed
     */
    public function invalidateSession(User $user, $session, SessionRepository $sessionRepository)
    {
        $sessionRepository->invalidateSession($session->id);

        return redirect()->route('user.sessions', $user->id)
            ->withSuccess(trans('app.session_invalidated'));
    }
}
