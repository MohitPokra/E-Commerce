<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/24/18
 * Time: 5:02 PM
 */

namespace App\Http\Requests\Address;

use App\Http\Requests\Request;
use App\Models\Product;

class AddressUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules()
    {
        $contactRule = [
            'country' => 'exists:countries,id',
            'city' => 'required|string',
            'zip' => 'required|string',
            'line1' => 'required|string',
            'line2' => 'string|nullable',
            'name' => 'string|nullable'
        ];

        $shippingRule = [
            'ship_country' => 'exists:countries,id',
            'ship_city' => 'required|string',
            'ship_zip' => 'required|string',
            'ship_line1' => 'required|string',
            'ship_line2' => 'string|nullable',
            'ship_name' => 'string|nullable'
        ];
         return  ($this->has('same_address') & $this->same_address == true) ? $contactRule : array_merge($contactRule, $shippingRule);

    }

    public function messages () {
        return [
            'country.exists' => 'Provided country does not exists in our database.',
            'ship_country.exists' => 'Provided country does not exists in our database.',
            'city.required' => 'City is required.',
            'ship_city.required' => 'City is required.',
            'zip.required' => 'Zip code is required.',
            'ship_zip.required' => 'Zip code is required.',
            'line1.required' => 'Address 1 is required.',
            'ship_line1.required' => 'Address 1 is required.',
            'line2.string' => 'Address 1 should be a string.',
            'ship_line2.string' => 'Address 1 should be a string.',
            'name' => 'Company should be a string.',
            'ship_name' => 'Name is Required.'
        ];
    }
}
