<?php

namespace App\Http\Requests\Admin\Brand;

use App\Http\Requests\Request;
use App\Models\Brand;

class BrandUpdateRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \Throwable
     */
    public function rules()
    {
        $brand = Brand::where('slug', $this->route('slug'))->firstOrFail();
        return [
            'name' => 'required|max:191',
            'slug' => 'required|unique:brands,slug,'.$brand->id.'|max:191',
        ];
    }
}
