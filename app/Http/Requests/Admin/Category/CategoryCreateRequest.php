<?php

namespace App\Http\Requests\Admin\Category;

use App\Http\Requests\Request;

class CategoryCreateRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'is_featured' => 'required|boolean',
        ];
    }
}
