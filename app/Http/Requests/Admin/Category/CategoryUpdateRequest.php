<?php

namespace App\Http\Requests\Admin\Category;

use App\Http\Requests\Request;
use App\Models\Category;

class CategoryUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = Category::where('slug', $this->route('slug'))->firstOrFail();
        return [
            'name' => 'required|max:191',
            'slug' => 'required|unique:categories,slug,'.$category->id.'|max:191',
        ];
    }
}
