<?php

namespace App\Http\Requests\Admin\Order;

use App\Http\Requests\Request;

class OrderCreateRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'status' => 'required|in:Pending,Unshipped,Shipped,Cancelled',
            'amount' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'total_discount' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'total_tax' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'bill_amount' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'delivery_address' => 'required|max:191',
            'order_date' => 'required',
            'expected_delivery_date' => 'required',
            'delivery_date' => 'required'
        ];
    }
}
