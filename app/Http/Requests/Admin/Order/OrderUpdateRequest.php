<?php

namespace App\Http\Requests\Admin\Order;

use App\Http\Requests\Request;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;

class OrderUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *  To-do status should be from UserStatus class
     * @return array
     */
    public function rules()
    {
        \Validator::extend('validOrder', function ($attribute, $value) {
            $isAdmin = \Auth::user()->hasRole('Admin');

            if(!$isAdmin) {
                return false;
            }
            $order = Order::where('hash', $value)->first();

            if(empty($order)) {
                return false;
            }

            return true;
        });
        return [
            'hash' => 'required|exists:users,hash|validOrder',
            'total_discount' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'total_tax' => 'required|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'expected_delivery_date' => 'sometimes|required|date'
        ];
    }

    public function messages () {
        return [
            'hash.exists' => 'Invalid, Order does not exists',
            'hash.valid_order' => 'You are not allowed to edit order info.'
        ];
    }
}
