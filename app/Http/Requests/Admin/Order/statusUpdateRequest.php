<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/24/18
 * Time: 5:02 PM
 */

namespace App\Http\Requests\Admin\Order;

use App\Http\Requests\Request;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use App\Support\Enum\OrderStatus;

class statusUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules()
    {
        \Validator::extend('validOrder', function ($attribute, $value) {
            $isAdmin = \Auth::user()->hasRole('Admin');

            if(!$isAdmin) {
                return false;
            }
            $order = Order::where('hash', $value)->first();

            if(empty($order)) {
                return false;
            }

            return true;
        });

        return  [
            'hash' => 'required|exists:orders,hash|validOrder',
            'status' => 'required|in:'.implode(',', array_values(OrderStatus::lists()))
        ];
    }

    public function messages () {
        return [
            'hash.exists' => 'Invalid, Order does not exists',
            'hash.valid_order' => 'Shipping info can not be updated for this order.'
        ];
    }
}
