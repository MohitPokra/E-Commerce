<?php

namespace App\Http\Requests\Admin\Product;

use App\Http\Requests\Request;

class ProductCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'short_name' => 'required|max:191',
            'is_gtin' => 'required|boolean',
            'price' => 'required|regex:/^\d{1,10}(\.\d{1,6})?$/|min:0.000001',
            'sale_price' => 'required|regex:/^\d{1,10}(\.\d{1,6})?$/',
            'on_sale' => 'required|boolean',
            'category_id' => 'required|exists:categories,id',
            'brand_id' => 'required|exists:brands,id',
            'is_available' => 'required|boolean',
            'in_stock' => 'boolean',
            'stock_quantity' => 'required_with:in_stock|integer|min:1',
            'is_shop_featured' => 'required|boolean',
            'is_category_featured' => 'required|boolean',
        ];
    }
}
