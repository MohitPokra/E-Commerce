<?php

namespace App\Http\Requests\Admin\Product;

use App\Http\Requests\Request;

class ProductUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
             'name' => 'required|max:191',
             'short_name' => 'required|max:191',
             'is_gtin' => 'required|boolean',
             'price' => 'required|regex:/^\d*(\.\d{1,6})?$/',
             'sale_price' => 'required|regex:/^\d*(\.\d{1,6})?$/',
             'on_sale' => 'required|boolean',
             'category_id' => 'required|exists:categories,id',
             'brand_id' => 'required|exists:brands,id',
             'is_available' => 'required|boolean',
             'in_stock' => 'boolean',
             'is_shop_featured' => 'required|boolean',
             'is_category_featured' => 'required|boolean',
        ];

        if($this->has('in_stock')) {
            $rules['stock_quantity'] = 'required_with:in_stock|integer|min:1';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'stock_quantity.min' => 'Stock quantity can not be 0 when stock is present.'
        ];
    }
}
