<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/24/18
 * Time: 5:02 PM
 */

namespace App\Http\Requests\Cart;

use App\Http\Requests\Request;
use App\Models\Product;

class AddToCartRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        \Validator::extend('available', function ($value) {
            $product = Product::find($this->product_id);
            if ($product->is_available && $product->stock_quantity > $value) {
                return true;
            }
            return false;
        });
        return [
            'product_id' => 'required|integer|exists:products,id',
            'quantity' => 'integer|available'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => 'Please select a product.',
            'product_id.exists' => 'Invalid Product.',
            'quantity.available' => 'The Product is no longer Available.'
        ];
    }
}
