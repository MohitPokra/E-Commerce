<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/24/18
 * Time: 5:02 PM
 */

namespace App\Http\Requests\Cart;

use App\Http\Requests\Request;
use App\Models\Product;

class RemoveFromCartRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
