<?php

namespace App\Http\Requests\Image;

use App\Http\Requests\Request;

class ImageDeleteRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required',
            'id' => 'required',
            'name' => 'required|exists:images,given_name'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'type.required' => 'Image type is required.',
            'id.required' => 'Image identifier is required.',
            'name.required' => 'Image name is required.',
        ];
    }
}
