<?php

namespace App\Http\Requests\Image;

use App\Http\Requests\Request;

class ImageUploadRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required',
            'id' => 'required',
        ];

        if(is_array($this->file)) {
            array_push($rules, ['file.*' => 'required|mimes:jpg,jpeg,jpe,png,webp,gif|max:8192']);
        } else {
            array_push($rules, ['file' => 'required|mimes:jpg,jpeg,jpe,png,webp,gif|max:8192']);
//            'file' => 'required|mimes:jpg,jpeg,jpe,png,webp,svg,gif,ico,bmp|max:8192'
        }

        return $rules;
    }

    public function messages() {
        return [
            'type.required' => 'Image type is required.',
            'id.required' => 'Image identifier is required.',
            'file.required' => 'File upload failed.',
            'file.*.required' => 'File upload failed.',
        ];
    }
}
