<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/24/18
 * Time: 5:02 PM
 */

namespace App\Http\Requests\Order;

use App\Http\Requests\Request;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use App\Support\Enum\OrderStatus;

class AddressUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules()
    {
        \Validator::extend('validOrder', function ($attribute, $value) {
            $orderStatus = OrderStatus::lists();
            $isAdmin = \Auth::user()->hasRole('Admin');
            $order = Order::where('hash', $value)
                ->where('status', $orderStatus['ADDRESS']);
            if($isAdmin) {
                $order->orWhere('status', $orderStatus['PENDING'])
                ->orWhere('status', $orderStatus['UNSHIPPED']);
            }else {
                $order->where('user_id', auth()->user()->id);
            }

            $order->firstOrFail();

            if(empty($order)) {
                return false;
            }

            return true;
        });

        return  [
            'hash' => 'required|exists:orders,hash|validOrder',
            'country' => 'exists:countries,id',
            'city' => 'required|string',
            'zip' => 'required|string',
            'line1' => 'required|string',
            'line2' => 'string|nullable',
            'name' => 'string|nullable'
        ];

    }

    public function messages () {
        return [
            'hash.exists' => 'Invalid does not exists',
            'hash.validOrder' => 'Address can not be changed for this order.',
            'country.exists' => 'Provided country does not exists in our database.',
            'city.required' => 'City is required.',
            'zip.required' => 'Zip code is required.',
            'line1.required' => 'Address 1 is required.',
            'line2.string' => 'Address 1 should be a string.',
            'name' => 'Company should be a string.',
        ];
    }
}
