<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/29/18
 * Time: 6:54 PM
 */

namespace App\Http\Requests\Payment;

use App\Http\Requests\Request;
use App\Models\Order;
use App\Support\Enum\OrderStatus;

class PayPalCallbackRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
