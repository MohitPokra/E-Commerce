<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/29/18
 * Time: 6:54 PM
 */

namespace App\Http\Requests\Payment;

use App\Http\Requests\Request;
use App\Models\Order;
use App\Support\Enum\OrderStatus;

class PayPalPaymentRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        \Validator::extend('pending', function ($attribute, $value) {
            $orderStatus = OrderStatus::lists();
            $order = Order::where('hash', $value)
                ->where('user_id', auth()->user()->id)
                ->where('status', $orderStatus['PENDING'])
                ->firstOrFail();

            if(empty($order)) {
                return false;
            }

            return true;
        });
        return [
            'hash' => 'required|exists:orders,hash|pending',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
