<?php

namespace App\Http\Requests\Review;

use App\Http\Requests\Request;
use App\Models\User;

class ReviewStoreRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'product_id' => 'required|exists:products,id',
            'name' => 'required|min:6',
            'email' => 'required|email',
            'subject' => 'required|string|max:256',
            'rating' => 'required|integer|max:5|min:1',
            'review' => 'required|string',
        ];

        return $rules;
    }
}
