<?php

namespace App\Http\Requests\StaticPage;

use App\Http\Requests\Request;

class StaticPageStoreRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'text' => 'required|string',
            'description' => 'required|string'
        ];
    }

}
