<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Models\User;

class UpdateDetailsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|unique:users,email,'. $this->route('id').'|max:191',
            'username' => 'nullable|unique:users,username,'.$this->route('id').'|max:191',
            'birthday' => 'nullable|date',
            'role_id' => 'required|exists:roles,id',
        ];

        if ($this->get('country_id')) {
            $rules += ['country_id' => 'exists:countries,id'];
        }

        return $rules;
    }
}
