<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/14/18
 * Time: 7:11 PM
 */

namespace App\Http\Traits;


use App\Models\Image;

trait Imaggable
{

    public function images() {
        return $this->morphMany(Image::class, 'imaggable');
    }

}
