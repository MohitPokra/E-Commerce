<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderCancel;
use App\Notifications\Order\OrderCancel as OrderCancelNotification;

class SendOrderCancelEmail
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderCancel $orderCancel
     * @return void
     */
    public function handle(OrderCancel $orderCancel)
    {
        $user = $orderCancel->getUser();
        $cancelOrder = $orderCancel->getCancelOrder();
        $message = $orderCancel->getMessage();

        $user->notify(new OrderCancelNotification($user , $cancelOrder, $message));
    }
}
