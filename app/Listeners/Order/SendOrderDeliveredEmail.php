<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderDelivered;
use App\Notifications\Order\OrderDelivered as OrderDeliveredNotification;

class SendOrderDeliveredEmail
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderDelivered $orderDelivered
     * @return void
     */
    public function handle(OrderDelivered $orderDelivered)
    {
        $user = $orderDelivered->getUser();
        $cancelOrder = $orderDelivered->getDeliveredOrder();
        $message = $orderDelivered->getMessage();

        $user->notify(new OrderDeliveredNotification($user , $cancelOrder, $message));
    }
}
