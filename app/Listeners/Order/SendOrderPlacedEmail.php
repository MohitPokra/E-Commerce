<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderPlaced;
use App\Notifications\Order\OrderPlaced as OrderPlacedNotification;

class SendOrderPlacedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderPlaced $orderPlaced
     * @return void
     */
    public function handle(OrderPlaced $orderPlaced)
    {
        $user = $orderPlaced->getUser();
        $order = $orderPlaced->getPlacedOrder();

        $user->notify(new OrderPlacedNotification($user, $order));
    }
}
