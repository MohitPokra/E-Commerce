<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderRefund;
use App\Notifications\Order\OrderRefund as OrderRefundNotification;

class SendOrderRefundEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderRefund $orderRefund
     * @return void
     */
    public function handle(OrderRefund $orderRefund)
    {
        $user = $orderRefund->getUser();
        $order = $orderRefund->getRefundedOrder();
        $message = $orderRefund->getMessage();

        $user->notify(new OrderRefundNotification($user, $order, $message));
    }
}
