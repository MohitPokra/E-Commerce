<?php

namespace App\Listeners\Order;

use App\Events\Order\OrderShipped;
use App\Notifications\Order\OrderShipped as OrderShippedNotification;

class SendOrderShippedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderShipped  $event
     * @return void
     */
    public function handle(OrderShipped $orderShipped)
    {
        $user = $orderShipped->getUser();
        $order = $orderShipped->getShippedOrder();
        $message = $orderShipped->getMessage();

        $user->notify(new OrderShippedNotification($user, $order, $message));
    }
}
