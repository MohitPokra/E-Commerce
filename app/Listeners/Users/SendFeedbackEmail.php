<?php

namespace App\Listeners\Users;

use App\Events\User\Feedback;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\User\Feedback as FeedbackNotification;

class SendFeedbackEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Feedback  $event
     * @return void
     */
    public function handle(Feedback $feedback)
    {
         $user = $feedback->getUser();
         $user->notify(new FeedbackNotification($user));
    }
}
