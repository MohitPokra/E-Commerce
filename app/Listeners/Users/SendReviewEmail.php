<?php

namespace App\Listeners\Users;

use App\Events\User\Review;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\User\Review as ReviewNotification;

class SendReviewEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Review  $event
     * @return void
     */
    public function handle(Review $review)
    {
        $user = $review->getUser();

        $user->notify(new ReviewNotification($review));
    }
}
