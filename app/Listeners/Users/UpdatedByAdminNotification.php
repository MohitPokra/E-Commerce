<?php

namespace App\Listeners\Users;

use App\Events\User\UpdatedByAdmin;
use App\Repositories\User\UserRepository;

class UpdatedByAdminNotification
{
    protected $userRepository;
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handle the event.
     *
     * @param  UpdatedByAdmin  $event
     * @return void
     */
    public function handle(UpdatedByAdmin $updatedByAdmin)
    {
       $updateUser = $updatedByAdmin->getUpdatedUser();

       $updateUser->notify(new UpdatedByAdmin($updateUser));

    }
}
