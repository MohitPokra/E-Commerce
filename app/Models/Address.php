<?php

namespace App\Models;

class Address extends BaseModel
{
    protected $table = 'addresses';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getCountry() {
        return Country::where('id', $this->country)->first();
    }
}
