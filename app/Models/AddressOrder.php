<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressOrder extends Model
{
    protected $table = 'address_order';

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function getCountry() {
        return Country::where('id', $this->country)->first();
    }
}
