<?php

namespace App\Models;

class Cart extends BaseModel
{
    protected $table = 'carts';

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
