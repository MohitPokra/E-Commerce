<?php

namespace App\Models;

use App\Http\Traits\Imaggable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\File;

class Category extends BaseModel implements HasMedia
{
    use HasMediaTrait, SoftDeletes,
        Imaggable, Sluggable;
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaCollections()
    {
        $this
        ->addMediaCollection('images')
        ->acceptsFile(function (File $file) {
            return $file->mimeType === 'image/png';
        });
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getFeaturedProductsCount()
    {
        return $this->products()->where('is_category_featured', true)->count();
    }

    public function featuredProducts()
    {
        return $this->products()->where('is_category_featured', true);
    }

}
