<?php

namespace App\Models;

class Country extends BaseModel
{


    protected $table = 'countries';

    public $timestamps = false;
}
