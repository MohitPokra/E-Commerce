<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/9/18
 * Time: 4:34 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Image extends BaseModel
{
    protected $table = 'images';

    public function imaggable()
    {
        return $this->morphTo();
    }
}
