<?php

namespace App\Models;

class Order extends BaseModel
{
    protected $table = 'orders';

    protected $guarded = [];

    protected $dates = [
        'order_date',
        'expected_delivery_date',
        'delivery_date',
        'shipping_date',
        'created_at',
        'updated_at'
    ];

    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function address() {
        return $this->hasOne(AddressOrder::class);
    }

    public function payment() {
        return $this->hasMany(Payment::class);
    }

    public function scopeProduct() {
        return $this->join('order_product', 'order_product.order_id', 'orders.id')
            ->join('products', 'order_product.product_id', 'products.id');
    }
}
