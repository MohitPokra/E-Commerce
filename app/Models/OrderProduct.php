<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/29/18
 * Time: 5:05 PM
 */

namespace App\Models;


class OrderProduct extends BaseModel
{
    protected $table = 'order_product';

    protected $fillable = [
        'order_id',
        'product_id',
        'amount',
        'on_sale',
        'sale_amount',
        'discount',
        'quantity'
    ];
}
