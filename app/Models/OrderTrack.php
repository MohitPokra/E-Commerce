<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTrack extends Model
{
    protected $table = 'order_track';
    public $timestamps = false;

    public function order () {
        return $this->belongsTo(Order::class);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function getDetailsAttribute($value) {
        return json_decode( $this->attributes['details']);
    }
}
