<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Page extends BaseModel implements HasMedia
{
    use HasMediaTrait;

    //




    public function registerMediaCollections()
    {
        $this
        ->addMediaCollection('images')
        ->acceptsFile(function (File $file) {
            return $file->mimeType === 'image/png';
        });
    }

    //
}
