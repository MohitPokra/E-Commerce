<?php

namespace App\Models;

class Payment extends BaseModel
{
    protected $table = 'payments';


    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function getResponseAttribute($value){
        return json_decode($this->attributes['response']);
    }
}
