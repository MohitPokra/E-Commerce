<?php

namespace App\Models;

use App\Http\Traits\Imaggable;
use App\Models\Category;
use App\Models\Brand;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Console\PackageDiscoverCommand;

class Product extends BaseModel implements HasMedia
{
    use HasMediaTrait,
        Imaggable, Sluggable;

    protected $dates = ['sale_ends_date', 'created_at', 'updated_at'];
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('available', function (Builder $builder) {
            if (!(auth()->check() && strtolower(Role::find( auth()->user()->role )->first()->name) == 'admin')) {
                $builder->where('products.is_available', '!=', 0);
            }
        } );
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function review()
    {
        return $this->hasMany(Review::class);
    }

    public function registerMediaCollections()
    {
        $this
        ->addMediaCollection('images')
        ->acceptsFile(function (File $file) {
            return $file->mimeType === 'image/png';
        });
    }

    public function getPercentOff()
    {
      return round(100*(($this->price - $this->sale_price) / $this->price));

    }

    public function getMainMediaUrl()
    {
        return $this->getMediaUrl('product_main');
    }

    public function getMainMediaThumbnailUrl()
    {

    }

    public function getCategoryMainMediaUrl()
    {
        return $this->getMediaUrl('catalog_main');
    }

    public function getCategorySecondaryMediaUrl()
    {
        return $this->getMediaUrl('catalog_secondary');
    }

    private function getMediaUrl($type)
    {
        return $this->getMedia('images', ['type'=>$type])->first()->getUrl();
    }
}
