<?php

namespace App\Models;

class Review extends BaseModel
{
    protected $table = 'reviews';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
