<?php

namespace App\Models;

class Setting extends BaseModel
{
    protected $table = 'settings';
    public $timestamps = false;

    public function setValueAttribute($value) {
        $this->attributes['value'] = json_encode($value);
    }

    public function getValueAttribute($value) {
        return json_decode( $this->attributes['value']);
    }
}
