<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;

class StaticPage extends BaseModel
{
    use Sluggable;

    public $timestamps = false;

    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
