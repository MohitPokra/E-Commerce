<?php

namespace App\Models;

use App\Http\Traits\Imaggable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Presenters\UserPresenter;
use App\Services\Auth\Api\TokenFactory;
use App\Services\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;
use App\Services\Auth\TwoFactor\Contracts\Authenticatable as TwoFactorAuthenticatableContract;
use App\Services\Logging\UserActivity\Activity;
use App\Support\Authorization\AuthorizationUserTrait;
use App\Support\Enum\UserStatus;
use Illuminate\Auth\Passwords\CanResetPassword;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements TwoFactorAuthenticatableContract, JWTSubject
{
    use TwoFactorAuthenticatable,
        CanResetPassword,
        PresentableTrait,
        AuthorizationUserTrait,
        LogsActivity,
        Notifiable,
        Billable,
        Imaggable;

    protected $presenter = UserPresenter::class;

    protected static $logAttributes = ['id', 'username'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'username', 'first_name', 'last_name', 'phone', 'avatar',
        'address', 'country_id', 'birthday', 'last_login', 'confirmation_token', 'status',
        'remember_token', 'role_id', 'provider', 'provider_id', 'reward_amount'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['birthday', 'last_login'];

    /**
     * Always encrypt password when it is updated.
     *
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = trim($value) ?: null;
    }

    public function gravatar()
    {
        if ($this->attributes['avatar']) {
            return $this->attributes['avatar'];
        }

        $hash = hash('md5', strtolower(trim($this->attributes['email'])));

        return sprintf("//www.gravatar.com/avatar/%s", $hash);
    }

    public function review()
    {
        return $this->hasMany(Review::class);
    }

    public function isUnconfirmed()
    {
        return $this->status == UserStatus::UNCONFIRMED;
    }

    public function isActive()
    {
        return $this->status == UserStatus::ACTIVE;
    }

    public function isBanned()
    {
        return $this->status == UserStatus::BANNED;
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function orderCount()
    {
        return $this->join('orders', 'orders.user_id', 'users.id')->where('users.id', $this->id)->count();
    }

    public function wishListCount()
    {
        return $this->join('carts', 'carts.user_id', 'users.id')->where('users.id', $this->id)->count();
    }

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id');
    }

    public function role () {
        return $this->belongsTo(Role::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->id;
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
//        $token = app(TokenFactory::class)->forUser($this);
//
//        return [
//            'jti' => $token->id
//        ];
    }

    public function contactAddress()
    {
        return self::select('addresses.*')
            ->join('addresses', 'addresses.user_id', 'users.id')
            ->whereIn('type', ['contact', 'same'])
            ->first();
    }

    public function shippingAddress()
    {
        return self::select('addresses.*')
            ->join('addresses', 'addresses.user_id', 'users.id')
            ->whereIn('type', ['shipping', 'same'])
            ->first();
    }
}
