<?php

namespace App\Notifications\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Order;
use App\Models\User;

class OrderDelivered extends Notification
{
    use Queueable;
    protected $order;
    protected $user;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Order $order
     * @param string $message
     */
    public function __construct(User $user, Order $order, $message = '')
    {
        $this->user = $user;
        $this->order = $order;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage =  (new MailMessage)
            ->subject(trans('app.order_delivered'))
            ->greeting(trans('app.greeting') .' '. ucfirst($this->user->first_name))
            ->salutation(setting('site_name'));

        if($this->message != ''){
            $mailMessage->line($this->message);
        }else{
            $mailMessage->line(trans('app.order_delivered_line'));
        }

        $mailMessage->action(trans('app.tack_order_button_text'), route('order.tracking.get', $this->order->hash))
            ->line(trans('app.thank_you_for_using_our_app'));

        return $mailMessage;
    }
}
