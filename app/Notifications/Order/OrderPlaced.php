<?php

namespace App\Notifications\Order;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Order;
use App\Models\User;

class OrderPlaced extends Notification
{
    protected $order;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
        return (new MailMessage)
            ->subject(trans('app.order_placed'))
            ->greeting(trans('app.greeting') .' '. ucfirst($this->user->first_name))
            ->salutation(setting('site_name'))
            ->line(trans('app.order_placed_line1'))
            ->line(trans('app.order_placed_line2') . $this->order->hash)
            ->line(trans('app.order_placed_line3'))
            ->action(trans('app.tack_order_button_text'), route('order.tracking.get', $this->order->hash))
            ->line(trans('app.thank_you_for_using_our_app'));
    }
}
