<?php

namespace App\Providers;

use App\Repositories\Address\AddressRepository;
use App\Repositories\Address\EloquentAddress;
use App\Repositories\Order\EloquentOrder;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Payment\EloquentPayment;
use App\Repositories\Payment\PaymentRepository;
use App\Repositories\Search\EloquentSearch;
use App\Repositories\Search\SearchRepository;
use App\Repositories\StaticPage\EloquentStaticPage;
use App\Repositories\StaticPage\StaticPageRepository;
use Carbon\Carbon;
use App\Repositories\Activity\ActivityRepository;
use App\Repositories\Activity\EloquentActivity;
use App\Repositories\Brand\BrandRepository;
use App\Repositories\Brand\EloquentBrand;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\EloquentCategory;
use App\Repositories\Country\CountryRepository;
use App\Repositories\Country\EloquentCountry;
use App\Repositories\Image\EloquentImage;
use App\Repositories\Image\ImageRepository;
use App\Repositories\Page\PageRepository;
use App\Repositories\Page\EloquentPage;
use App\Repositories\Permission\EloquentPermission;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\EloquentProduct;
use App\Repositories\Role\EloquentRole;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\DbSession;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\EloquentUser;
use App\Repositories\User\UserRepository;

use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        PageRepository::class => EloquentPage::class,
        BrandRepository::class => EloquentBrand::class,
        CategoryRepository::class => EloquentCategory::class,
        ProductRepository::class => EloquentProduct::class,
        UserRepository::class => EloquentUser::class,
        PermissionRepository::class => EloquentPermission::class,
        SessionRepository::class => DbSession::class,
        CountryRepository::class => EloquentCountry::class,
        ImageRepository::class => EloquentImage::class,
        OrderRepository::class => EloquentOrder::class,
        SearchRepository::class => EloquentSearch::class,
        AddressRepository::class => EloquentAddress::class,
        PaymentRepository::class => EloquentPayment::class,
        StaticPageRepository::class => EloquentStaticPage::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cashier::useCurrency('usd', '$');
        Carbon::setLocale(config('app.locale'));
        config(['app.name' => settings('app_name')]);
        \Illuminate\Database\Schema\Builder::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ActivityRepository::class, EloquentActivity::class);
        $this->app->singleton(RoleRepository::class, EloquentRole::class);


        if ($this->app->environment('local')) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
    }
}
