<?php

namespace App\Providers;

use App\Repositories\Brand\BrandRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;

use App\Repositories\Brand\EloquentBrand;
use App\Repositories\Category\EloquentCategory;
use App\Repositories\Product\EloquentProduct;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{


    /**
     * @var EloquentBrand
     */
    private $brands;

    /**
     * @var EloquentCategory
     */
    private $categories;

    /**
     * @var EloquentProducts
     */
    private $products;

    /**
     * ComposerServiceProvider constructor.
     *
     * @param BrandRepository $brands
     * @param CategoryRepository $categories
     * @param ProductRepository $products
     *
     * @return void
     */
    public function boot(BrandRepository $brands, CategoryRepository $categories, ProductRepository $products)
    {
        $this->brands = $brands;
        $this->categories = $categories;
        $this->products = $products;

        view()->composer(
            'home',
            function ($view) {
                $view->with('categories', $this->categories);
                $view->with('brands', $this->brands);
            }
        );

        view()->composer(
            'shop.nav',
            function ($view) {
                $view->with('categories', $this->categories);
                $view->with('brands', $this->brands);
            }
        );

        //register partials
        view()->composer(
            'home.topcategories',
            function ($view) {
                $view->with('categories', $this->categories);
            }
        );
        view()->composer(
            'home.brands',
            function ($view) {
                $view->with('brands', $this->brands);
            }
        );
        view()->composer(
            'home.featuredproducts',
            function ($view) {
                $view->with('products', $this->products);
            }
        );

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
