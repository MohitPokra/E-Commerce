<?php

namespace App\Providers;

use App\Events\Order\OrderCancel;
use App\Events\Order\OrderDelivered;
use App\Events\Order\OrderPlaced;
use App\Events\Order\OrderRefund;
use App\Events\Order\OrderShipped;
use App\Events\User\Banned;
use App\Events\User\Feedback;
use App\Events\User\LoggedIn;
use App\Events\User\Registered;
use App\Events\User\Review;
use App\Events\User\UpdatedByAdmin;
use App\Listeners\Order\SendOrderDeliveredEmail;
use App\Listeners\Users\InvalidateSessionsAndTokens;
use App\Listeners\Login\UpdateLastLoginTimestamp;
use App\Listeners\PermissionEventsSubscriber;
use App\Listeners\Registration\SendConfirmationEmail;
use App\Listeners\Registration\SendSignUpNotification;
use App\Listeners\RoleEventsSubscriber;
use App\Listeners\UserEventsSubscriber;
use App\Listeners\Users\SendFeedbackEmail;
use App\Listeners\Users\SendReviewEmail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\Order\SendOrderCancelEmail;
use App\Listeners\Order\SendOrderPlacedEmail;
use App\Listeners\Order\SendOrderRefundEmail;
use App\Listeners\Order\SendOrderShippedEmail;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendConfirmationEmail::class,
            SendSignUpNotification::class,
        ],
        LoggedIn::class => [
            UpdateLastLoginTimestamp::class
        ],
        Banned::class => [
            InvalidateSessionsAndTokens::class
        ],

        OrderPlaced::class => [
            SendOrderPlacedEmail::class
        ],

        OrderShipped::class => [
            SendOrderShippedEmail::class
        ],

        OrderCancel::class => [
            SendOrderCancelEmail::class
        ],

        OrderRefund::class => [
            SendOrderRefundEmail::class
        ],

        OrderDelivered::class => [
            SendOrderDeliveredEmail::class
        ],

        Review::class => [
            SendReviewEmail::class
        ],

        Feedback::class => [
            SendFeedbackEmail::class
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        UserEventsSubscriber::class,
        RoleEventsSubscriber::class,
        PermissionEventsSubscriber::class
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
