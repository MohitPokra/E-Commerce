<?php

namespace App\Providers;

use jdavidbakr\MailTracker\MailTrackerServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class MailTrackerServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish pieces
        $this->publishConfig();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->publishViews();

        // Hook into the mailer
        $this->registerSwiftPlugin();

        // Install the routes
        $this->installRoutes();
    }

    /**
     * Install the needed routes
     *
     * @return void
     */
    protected function installRoutes()
    {
        $config = $this->app['config']->get('mail-tracker.route', []);
        $config['namespace'] = 'jdavidbakr\MailTracker';

        if (!$this->isLumen()) {
            Route::group($config, function()
            {
                Route::get('t/{hash}', 'MailTrackerController@getT')->name('mailTracker_t');
                Route::get('l/{url}/{hash}', 'MailTrackerController@getL')->name('mailTracker_l');
                Route::post('sns', 'SNSController@callback')->name('mailTracker_SNS');
            });
        } else {
            $app = $this->app;
            $app->group($config, function () use ($app) {
                $app->get('t', 'MailTrackerController@getT')->name('mailTracker_t');
                $app->get('l', 'MailTrackerController@getL')->name('mailTracker_l');
                $app->post('sns', 'SNSController@callback')->name('mailTracker_SNS');
            });
        }
        // Install the Admin routes
        $config_admin = $this->app['config']->get('mail-tracker.admin-route', []);
        $config_admin_controller = $this->app['config']->get('mail-tracker.admin-controller');

        $config_admin['namespace'] = $config_admin_controller['namespace'] ?? 'jdavidbakr\MailTracker';

        if (!$this->isLumen()) {
            Route::group($config_admin, function()
            {
                Route::get('/', 'AdminController@getIndex')->name('mailTracker_Index');
                Route::post('search', 'AdminController@postSearch')->name('mailTracker_Search');
                Route::get('clear-search', 'AdminController@clearSearch')->name('mailTracker_ClearSearch');
                Route::get('show-email/{id}', 'AdminController@getShowEmail')->name('mailTracker_ShowEmail');
                Route::get('url-detail/{id}', 'AdminController@getUrlDetail')->name('mailTracker_UrlDetail');
                Route::get('smtp-detail/{id}', 'AdminController@getSmtpDetail')->name('mailTracker_SmtpDetail');
            });
        } else {
            $app = $this->app;
            $app->group($config_admin, function () use ($app) {
                $app->get('/', 'AdminController@getIndex')->name('mailTracker_Index');
                $app->post('search', 'AdminController@postSearch')->name('mailTracker_Search');
                $app->get('clear-search', 'AdminController@clearSearch')->name('mailTracker_ClearSearch');
                $app->get('show-email/{id}', 'AdminController@getShowEmail')->name('mailTracker_ShowEmail');
                $app->get('url-detail/{id}', 'AdminController@getUrlDetail')->name('mailTracker_UrlDetail');
                $app->get('smtp-detail/{id}', 'AdminController@getSmtpDetail')->name('mailTracker_SmtpDetail');
            });
        }
    }
}
