<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
     const WEB_NAMESPACE = 'App\Http\Controllers\Web';

     const API_NAMESPACE = 'App\Http\Controllers\Api';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'namespace' => RouteServiceProvider::WEB_NAMESPACE,
            'middleware' => 'web',
        ], function ($router) {
            require base_path('routes/public/auth.php');
            require base_path('routes/public/pages.php');
            require base_path('routes/public/profile.php');
            require base_path('routes/public/shop.php');
            require base_path('routes/web.php');
        });

        Route::group([
            'namespace' => RouteServiceProvider::WEB_NAMESPACE,
            'middleware' => ['web', 'auth'],
        ], function ($router) {
            require base_path('routes/admin/dashboard.php');
            require base_path('routes/public/order.php');
            require base_path('routes/public/payments.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => RouteServiceProvider::API_NAMESPACE,
            'prefix' => 'api',
        ], function () {
            require base_path('routes/api.php');
        });
    }
}
