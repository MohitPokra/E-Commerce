<?php

namespace App\Providers;

use App\Events\Order\SendOrderShippedEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderShippedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendOrderShippedEmail  $event
     * @return void
     */
    public function handle(SendOrderShippedEmail $event)
    {
        //
    }
}
