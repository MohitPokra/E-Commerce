<?php

namespace App\Repositories\Address;

use App\Http\Requests\Address\AddressUpdateRequest;
use App\Http\Requests\Order\AddressUpdateRequest as OrderAddressUpdateRequest;
use App\Models\Order;

interface AddressRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function getUserAddress($id);

    /**
     * Create new brand.
     *
     * @param array $data
     * @return Brand
     */
    public function create(array $data);

    /**
     * Update specified Brand.
     *
     * @param $id Brand Id
     * @param array $data
     * @return Brand
     */
    public function update($id, array $data);

    /**
     * Remove Brand from repository.
     *
     * @param $id Brand Id
     * @return bool
     */
    public function delete($id);

    /**
     * @param AddressUpdateRequest $request
     * @param $contactType
     * @return mixed
     */
    public function saveCurrentAddress(AddressUpdateRequest $request, $contactType);

    /**
     * @param AddressUpdateRequest $request
     * @return mixed
     */
    public function saveShippingAddress(AddressUpdateRequest $request);

    /**
     * @param Order $order
     * @param OrderAddressUpdateRequest $request
     * @return mixed
     */
    public function saveOrderAddress(Order $order,OrderAddressUpdateRequest $request);

    /**
     * @return mixed
     */
    public function destroyShippingAddress();
}
