<?php

namespace App\Repositories\Address;

use App\Http\Requests\Address\AddressUpdateRequest;
use App\Http\Requests\Order\AddressUpdateRequest as OrderAddressUpdateRequest;
use App\Models\Address;
use App\Models\AddressOrder;
use App\Models\Order;

class EloquentAddress implements AddressRepository
{
    /**
     * {@inheritdoc}
     */
    public function getUserAddress($id)
    {
        return Address::where('user_id', $id)
            ->whereIn('type', ['same', 'shipping'])
            ->firstOrFail();
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $brand = Address::create($data);

        return $brand;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        $address = $this->find($id);

        $address->update($data);

        return $address;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $address = $this->find($id);

        return $address->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function saveCurrentAddress(AddressUpdateRequest $request, $contactType)
    {
        $authUser = auth()->user();
        $currentAddress = Address::where('user_id', $authUser->id)
            ->whereIn('type', ['contact', 'same'])
            ->first();

        if (empty($currentAddress)) {
            $currentAddress = new Address();
        }

        $currentAddress->user_id = $authUser->id;
        $currentAddress->type = $contactType;
        $currentAddress->name = $request->name ? : '';
        $currentAddress->line1 = $request->line1;
        $currentAddress->line2 = $request->line2 ? : '';
        $currentAddress->city = $request->city;
        $currentAddress->country = $request->country ? : null;
        $currentAddress->zip = $request->zip;
        $currentAddress->save();
    }

    /**
     * {@inheritdoc}
     */
    public function saveShippingAddress(AddressUpdateRequest $request)
    {
        $authUser = auth()->user();
        $currentAddress = Address::where('user_id', $authUser->id)
            ->where('type', 'shipping')
            ->first();

        if (empty($currentAddress)) {
            $currentAddress = new Address();
        }

        $currentAddress->user_id = $authUser->id;
        $currentAddress->type = 'shipping';
        $currentAddress->name = $request->ship_name ? : '';
        $currentAddress->line1 = $request->ship_line1;
        $currentAddress->line2 = $request->ship_line2 ? : '';
        $currentAddress->city = $request->ship_city;
        $currentAddress->country = $request->ship_country ? : null;
        $currentAddress->zip = $request->ship_zip;
        $currentAddress->save();
    }

    /**
     * {@inheritdoc}
     */
    public function saveOrderAddress(Order $order,OrderAddressUpdateRequest $request)
    {
        $currentAddress = AddressOrder::where(['order_id' => $order->id])->first();

        if(empty($currentAddress)) {
            $currentAddress = new AddressOrder();
        }

        $currentAddress->order_id = $order->id;
        $currentAddress->first_name = $request->first_name;
        $currentAddress->last_name = $request->last_name ? : '';
        $currentAddress->email = $request->email;
        $currentAddress->phone = $request->phone;
        $currentAddress->name = $request->name ? : '';
        $currentAddress->line1 = $request->line1;
        $currentAddress->line2 = $request->line2 ? : '';
        $currentAddress->city = $request->city;
        $currentAddress->country = $request->country ? : null;
        $currentAddress->zip = $request->zip;
        $currentAddress->save();
    }

    /**
     * {@inheritdoc}
     */
    public function destroyShippingAddress()
    {
        Address::where('user_id', auth()->user()->id)
            ->where('type', 'shipping')
            ->delete();
    }
}
