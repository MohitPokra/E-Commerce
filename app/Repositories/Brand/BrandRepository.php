<?php

namespace App\Repositories\Brand;

use App\Models\Brand;

interface BrandRepository
{
    /**
     * @param $query
     * @return mixed
     */
    public function withBrandImages ($query);
    /**
     * Get all brands.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Lists all brands into $key => $column value pairs.
     *
     * @param string $column
     * @param string $key
     * @return mixed
     */
    public function lists($column = 'name', $key = 'id');

    /**
     * Find brand by id.
     *
     * @param $id Brand Id
     * @return Brand|null
     */
    public function find($id);

    /**
     * Find Brand by name:
     *
     * @param $name
     * @return mixed
     */
    public function findByName($name);

    /**
     * Create new brand.
     *
     * @param array $data
     * @return Brand
     */
    public function create(array $data);

    /**
     * Update specified Brand.
     *
     * @param $id Brand Id
     * @param array $data
     * @return Brand
     */
    public function update($id, array $data);

    public function updateBySlug($slug, array $data);

    /**
     * Remove Brand from repository.
     *
     * @param $id Brand Id
     * @return bool
     */
    public function delete($id);

    public function deleteBySlug($slug);

    public function findBySlug($slug);
}
