<?php

namespace App\Repositories\Brand;

use App\Models\Brand;
use App\Support\Enum\Imaggable;
use DB;

class EloquentBrand implements BrandRepository
{
    /**
     * {@inheritdoc}
     */
    public function withBrandImages ($query) {
        $brandImaggable = Imaggable::lists()['brand'];
        $brandImageUrl = asset_url($brandImaggable['storageDirectory']);
        $noImageUrl = asset_url('defaults/noimage.png');

        $assertion = 'IF(`images`.`given_name` != "", CONCAT("'.$brandImageUrl.'/", images.given_name), "'.$noImageUrl.'") as brand_image_url';

        return $query->selectRaw('images.*,'.$assertion)
            ->whereIn('meta_type', Imaggable::lists()['brand']['meta_type'])->orderBy('meta_type');

    }
    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Brand::all();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Brand::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $brand = Brand::create($data);

        return $brand;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        $brand = $this->find($id);

        $brand->update($data);

        return $brand;
    }

    public function updateBySlug($slug, array $data) {

        $category = Brand::where('slug', '=', $slug)->firstOrFail();

        $category->update($data);

        return $category;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $brand = $this->find($id);

        return $brand->delete();
    }

    public function deleteBySlug($slug)
    {
         return Brand::where('slug', '=', $slug)->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function lists($column = 'name', $key = 'id')
    {
        return Brand::pluck($column, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName($name)
    {
        return Brand::where('name', $name)->first();
    }


    public function findBySlug($slug)
    {
        return Brand::with(['images' => function ($query) {
            $this->withBrandImages($query);
        }])->where('slug', $slug)->firstOrFail();
    }

}
