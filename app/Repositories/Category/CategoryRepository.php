<?php

namespace App\Repositories\Category;

use App\Models\Category;

interface CategoryRepository
{
    /**
     * @param $query
     * @return mixed
     */
    public function withCategoryImages ($query);

    /**
     * Get all categorys.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Lists all categorys into $key => $column value pairs.
     *
     * @param string $column
     * @param string $key
     * @return mixed
     */
    public function lists($column = 'name', $key = 'id');

    /**
     * Find category by id.
     *
     * @param $id Category Id
     * @return Category|null
     */
    public function find($id);

    /**
     * Find Category by name:
     *
     * @param $name
     * @return mixed
     */
    public function findByName($name);

    /**
     * Create new category.
     *
     * @param array $data
     * @return Category
     */
    public function create(array $data);

    /**
     * Update specified Category.
     *
     * @param $id Category Id
     * @param array $data
     * @return Category
     */
    public function update($id, array $data);

    public function updateBySlug($slug, array $data);

    /**
     * Remove Category from repository.
     *
     * @param $id Category Id
     * @return bool
     */
    public function delete($id);

    public function deleteBySlug($slug);

    /**
     * Gets the featured categories.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFeatured();

    /**
     * Gets the featured categories.
     * @param $limit how many to return
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFeaturedLimit($limit);

    /**
     * Gets the featured categories.
     * @return integer
     */
    public function getFeaturedCount();

    /**
     * Gets all categories to show on home page.
     * @return mixed
     */
    public function getHomePageCategories();

    public function findBySlug($slug);

}
