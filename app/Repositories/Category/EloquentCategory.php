<?php

namespace App\Repositories\Category;

use App\Support\Enum\Imaggable;
use App\Models\Category;
use DB;

class EloquentCategory implements CategoryRepository
{
    /**
     * {@inheritdoc}
     */
    public function withCategoryImages ($query) {
        $categoryImaggable = Imaggable::lists()['category'];
        $categoryImageUrl = asset_url($categoryImaggable['storageDirectory']);
        $noImageUrl = asset_url('defaults/noimage.png');

        $assertion = 'IF(`images`.`given_name` != "", CONCAT("'.$categoryImageUrl.'/", images.given_name), "'.$noImageUrl.'") as category_image_url';

        return $query->selectRaw('images.*,'.$assertion)
            ->whereIn('meta_type', Imaggable::lists()['category']['meta_type'])->orderBy('meta_type');

    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Category::all();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Category::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $category = Category::create($data);

        return $category;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        $category = $this->find($id);

        $category->update($data);

        return $category;
    }

    public function updateBySlug($slug, array $data) {

        $category = Category::where('slug', '=', $slug)->firstOrFail();

        $category->update($data);

        return $category;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $category = $this->find($id);

        return $category->delete();
    }

    public function deleteBySlug($slug)
    {
        return Category::where('slug', '=', $slug)->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function lists($column = 'name', $key = 'id')
    {
        return Category::pluck($column, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName($name)
    {
        return Category::where('name', $name)->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getFeatured()
    {
        return Category::where('is_featured', true)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getFeaturedLimit($limit)
    {
        return Category::where('is_featured', true)->take($limit)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getFeaturedCount()
    {
        return Category::where('is_featured', true)->count();
    }

    /**
     * {@inheritdoc}
     */
    public function getHomePageCategories () {
        return Category::selectRaw('categories.*, min(products.price) as starting_price')
            ->with([
                'images' => function ($query) {
                    $this->withCategoryImages($query);
                }
            ])
            ->leftJoin('products', 'products.category_id', 'categories.id')
            ->groupBy('categories.id')
            ->get();
    }

    public function findBySlug($slug)
    {
        return Category::with(['images' => function ($query) {
            $this->withCategoryImages($query);
        }])->where('slug', $slug)->firstOrFail();
    }

}
