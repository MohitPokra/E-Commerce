<?php

namespace App\Repositories\Image;

use App\Models\Image;
use DB;

class EloquentImage implements ImageRepository
{
    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Image::all();
    }
}
