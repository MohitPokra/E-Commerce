<?php

namespace App\Repositories\Image;

use App\Models\Brand;

interface ImageRepository
{
    /**
     * Get all brands.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();
}
