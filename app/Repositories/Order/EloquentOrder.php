<?php

namespace App\Repositories\Order;

use App\Http\Requests\Admin\Order\shippingInfoUpdateRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Support\Enum\OrderStatus;
use Carbon\Carbon;
use DB;
use Spatie\SchemaOrg\Car;

class EloquentOrder implements OrderRepository
{
    /**
     * {@inheritdoc}
     */
    public function getAllUserOrders(User $user)
    {
        return Order::where('user_id', $user->id)->get();
    }
    /**
     * {@inheritdoc}
     */
    public function getAllUserOrdersWithProduct(User $user)
    {
        return Order::product()->where('user_id', $user->id)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOrders()
    {
        return Order::all();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Order::find($id);
    }


    /**
     * {@inheritdoc}
     */
    public function getOrderProducts($hash) {
        return Product::select('products.*', 'order_product.quantity')
            ->leftJoin('order_product', 'order_product.product_id', 'products.id')
            ->leftJoin('orders', 'orders.id', 'order_product.order_id')
            ->where('orders.hash', $hash)
            ->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentInformation (Order $order) {
        return $order->payment()->first();
    }

    public function create (array $data) {

        $order = Order::create($data);

        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function createBlankOrder() {
        $orderStatus = OrderStatus::lists();
        $hash = substr(strtoupper(bin2hex(random_bytes(16))), 22);
        return Order::create([
            'user_id' => auth()->user()->id,
            'hash' => $hash,
            'status' => $orderStatus['ADDRESS'],
            'amount' => 0,
            'total_discount' => 0,
            'total_tax' => 0,
            'bill_amount' => 0,
            'order_date' => Carbon::today(),
            'expected_delivery_date' => Carbon::today()->addDays(7),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    public function update($hash, array $data) {

        $order = Order::where('hash', $hash)->firstOrFail();

        $order->update($data);

        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function saveShippingInfo(Order $order, shippingInfoUpdateRequest $request) {
        $order->shipping_date = Carbon::parse($request->shipping_date);
        $order->shipping_tracking_no = $request->shipping_tracking_no;
        $order->shipping_service = $request->shipping_service;
        $order->save();
        return $order;
    }

    public function delete($id)
    {
        $order = $this->find($id);

        return $order->delete();
    }

    public function todayOrder()
    {
        $todayOrder = Order::where('order_date', Carbon::today())->get();

        return $todayOrder;
    }

    public function todaySale () {

       $todaySale = Order::where('order_date', Carbon::today())->sum('bill_amount');

       return $todaySale;
    }

    public function yesterdayOrder()
    {
        $yesterdayOrder = Order::where('order_date', Carbon::yesterday())->get();

        return $yesterdayOrder;
    }

    public function yesterdaySale()
    {
        $yesterdaySale = Order::where('order_date', Carbon::yesterday())->sum('bill_amount');

        return $yesterdaySale;
    }

    public function salePerDayOfMonth ($date)
    {
        $date = Carbon::createFromFormat('M-Y', $date)->toDateTimeString();

        $from = Carbon::parse($date)->startOfMonth();
        $to = Carbon::parse($date)->endOfMonth();

        $result = Order::select(DB::raw('SUM(bill_amount) as total_expense'), 'order_date')
            ->whereBetween('order_date', [$from, $to])
            ->groupBy('order_date')
            ->get()->keyBy('order_date')->toarray();

        $counts = [];

        for($i = 1 ; $i <= $from->daysInMonth; $i++) {

            $key = $from->copy()->addDay($i-1)->toDateTimeString();

            if(array_key_exists($key, $result)) {
                $counts[$i] = $result[$key]['total_expense'];
            }
            else {
                $counts[$i] = 0;
            }
        }

        return $counts;

    }
}
