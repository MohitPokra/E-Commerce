<?php

namespace App\Repositories\Order;

use App\Http\Requests\Admin\Order\shippingInfoUpdateRequest;
use App\Models\Order;
use App\Models\User;

interface OrderRepository
{
    /**
     * Get all the orders related to the provided user.
     *
     * @param User $user
     * @return mixed
     */
    public function getAllUserOrders(User $user);

    /**
     * Get all the orders and product in the order related to the provided user.
     *
     * @param User $user
     * @return mixed
     */
    public function getAllUserOrdersWithProduct(User $user);

    /**
     * Get all orders
     */
    public function getAllOrders();

    /**
     * Get given id order
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Get given id order
     * @param $hash
     * @return mixed
     */
    public function getOrderProducts($hash);

    /**
     * @param Order $order
     * @return mixed
     */
    public function getPaymentInformation (Order $order);

    /**
     * create new order
     * @param array $data
     * @return Order user
     */
    public function create(array $data);

    /**
     * create blank order
     * @return Order user
     */
    public function createBlankOrder();

    /**
     * update order
     * @param $id
     * @param array $data
     * @return updated value
     */
    public function update($id, array $data);

    /**
     * update order
     * @param Order $order
     * @param shippingInfoUpdateRequest $request
     * @return updated value
     */
    public function saveShippingInfo(Order $order, shippingInfoUpdateRequest $request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    public function todayOrder();

    public function todaySale ();

    public function yesterdayOrder();

    public function yesterdaySale();

    public function salePerDayOfMonth($date);
}
