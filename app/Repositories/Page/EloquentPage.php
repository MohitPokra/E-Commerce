<?php

namespace App\Repositories\Page;

use App\Models\Page;
use DB;

class EloquentPage implements PageRepository
{
    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Page::all();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Page::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $page = Page::create($data);

        return $page;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        $page = $this->find($id);

        $page->update($data);

        return $page;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $page = $this->find($id);

        return $page->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function lists($column = 'title', $key = 'id')
    {
        return Page::pluck($column, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function findBySlug($slug)
    {
        return Page::where('slug', $slug)->firstOrFail();
    }
}
