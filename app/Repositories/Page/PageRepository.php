<?php

namespace App\Repositories\Page;

use App\Models\Page;

interface PageRepository
{
    /**
     * Get all pages.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Lists all pages into $key => $column value pairs.
     *
     * @param string $column
     * @param string $key
     * @return mixed
     */
    public function lists($column = 'name', $key = 'id');

    /**
     * Find page by id.
     *
     * @param $id Page Id
     * @return Page|null
     */
    public function find($id);

    /**
     * Find Page by slug
     *
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug);

    /**
     * Create new page.
     *
     * @param array $data
     * @return Page
     */
    public function create(array $data);

    /**
     * Update specified Page.
     *
     * @param $id Page Id
     * @param array $data
     * @return Page
     */
    public function update($id, array $data);

    /**
     * Remove Page from repository.
     *
     * @param $id Page Id
     * @return bool
     */
    public function delete($id);

}
