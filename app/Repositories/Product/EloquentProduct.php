<?php

namespace App\Repositories\Product;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Search\SearchRepository;
use App\Support\Enum\Imaggable;
use DB;

class EloquentProduct implements ProductRepository
{
    /**
     * {@inheritdoc}
     */
    public function withProductImages ($query) {

        $productImaggable = Imaggable::lists()['product'];
        $productImageUrl = asset_url($productImaggable['storageDirectory']);
        $noImageUrl = asset_url('defaults/noimage.png');

        $assertion = 'IF(`images`.`given_name` != "", CONCAT("'.$productImageUrl.'/", images.given_name), "'.$noImageUrl.'") as product_image_url';

        return $query->selectRaw('images.*,'.$assertion)
            ->whereIn('meta_type', Imaggable::lists()['product']['meta_type'])->orderBy('meta_type');

    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return Product::all();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Product::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        $product = Product::create($data);

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        $product = $this->find($id);

        $product->update($data);

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $product = $this->find($id);

        return $product->delete();
    }

    public function deleteBySlug($slug)
    {
        return Product::where('slug', '=', $slug)->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function lists($column = 'name', $key = 'id')
    {
        return Product::pluck($column, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName($name)
    {
        return Product::where('name', $name)->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findBySlug($slug)
    {
        return Product::where('slug', $slug)->firstOrFail();
    }

    /**
     * {@inheritdoc}
     */
    public function findBySlugWithReviews($slug)
    {
        return Product::with(['images' => function ($query) {
            $this->withProductImages($query);
        }, 'review.user'])->where('slug', $slug)->firstOrFail();
    }

    /**
     * {@inheritdoc}
     */
    public function getFeatured($count = null)
    {
        $featuredProduct = Product::selectRaw('products.*')
            ->with(['images' => function ($query) {
                $this->withProductImages($query);
            }])
            ->where('products.is_category_featured', true)
            ->inRandomOrder();

        if ($count !== null) {
            $featuredProduct = $featuredProduct->take($count);
        }

        $featuredProduct = $featuredProduct->get();

        return $featuredProduct;
    }

    /**
     * {@inheritdoc}
     */
    public function getFeaturedLimit($limit)
    {
        return Product::where('is_shop_featured', true)->take($limit)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getFeaturedCount()
    {
        return Product::where('is_shop_featured', true)->count();
    }

    /**
     * {@inheritdoc}
     */
    public function getCartProducts(User $account)
    {
        return Product::selectRaw('
        `products`.`on_sale`,
        `products`.`price`,
        `products`.`sale_price`,
        `products`.`slug`,
        `products`.`name`,
        `products`.`id`,
        `products`.`stock_quantity`,
        `products`.`in_stock`,
        `products`.`is_available`,
        `carts`.`quantity`,
        `carts`.`id` as `cart_id`')
            ->with(['images' => function ($query) {
                $this->withProductImages($query);
            }])
            ->join('carts', 'products.id', 'carts.product_id')
            ->join('users', 'carts.user_id', 'users.id')
            ->where('users.id', $account->id)
            ->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderProducts(Order $order)
    {
        return Product::selectRaw('
        `products`.`on_sale`,
        `products`.`price`,
        `products`.`sale_price`,
        `products`.`slug`,
        `products`.`name`,
        `products`.`id`,
        `products`.`in_stock`,
        `products`.`stock_quantity`,
        `products`.`is_available`,
        `order_product`.`quantity`')
            ->with(['images' => function ($query) {
                $this->withProductImages($query);
            }])
            ->join('order_product', 'products.id', 'order_product.product_id')
            ->join('orders', 'order_product.order_id', 'orders.id')
            ->where('orders.id', $order->id)
            ->get();
    }

    public function updateBySlug($slug, array $data) {

        $product = Product::where('slug', '=', $slug)->firstOrFail();

        $product->update($data);

        return $product;
    }

}
