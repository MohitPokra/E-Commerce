<?php

namespace App\Repositories\Product;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;

interface ProductRepository
{
    /**
     * This is an helper function to get image data of product.
     * @param $query
     * @return mixed
     */
    public function withProductImages ($query);

    /**
     * Get all products.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Lists all products into $key => $column value pairs.
     *
     * @param string $column
     * @param string $key
     * @return mixed
     */
    public function lists($column = 'name', $key = 'id');

    /**
     * Find product by id.
     *
     * @param $id Product Id
     * @return Product|null
     */
    public function find($id);

    /**
     * Find Product by name:
     *
     * @param $name
     * @return mixed
     */
    public function findByName($name);

    /**
     * Find Product by slug:
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug);

    /**
     * Find Product by slug:
     * @param $slug
     * @return mixed
     */
    public function findBySlugWithReviews($slug);

    /**
     * Create new product.
     *
     * @param array $data
     * @return Product
     */
    public function create(array $data);

    /**
     * Update specified Product.
     *
     * @param $id Product Id
     * @param array $data
     * @return Product
     */
    public function update($id, array $data);

    /**
     * Remove Product from repository.
     *
     * @param $id Product Id
     * @return bool
     */
    public function delete($id);

    public function deleteBySlug($slug);

    /**
     * Gets the featured categories.
     *
     * @param null $count
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFeatured($count = null);

    /**
     * Gets the featured categories.
     * @param $limit how many to return
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFeaturedLimit($limit);

    /**
     * Gets the featured categories.
     * @return integer
     */
    public function getFeaturedCount();

    /**
     * Gets all products in cart of user.
     * @param User $account
     * @return mixed
     */
    public function getCartProducts (User $account);

    /**
     * Gets all products in cart of user.
     * @param Order $order
     * @return mixed
     */
    public function getOrderProducts(Order $order);

    public function updateBySlug($slug, array $data);
}
