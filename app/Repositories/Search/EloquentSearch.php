<?php

namespace App\Repositories\Search;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Product\ProductRepository;
use App\Support\Enum\Imaggable;
use DB;

class EloquentSearch implements SearchRepository
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function search($search = null, $perPage = 10, $pageNo = 1)
    {
        $query = Product::query();

        $query = $query->selectRaw('
        `products`.`on_sale`,
        `products`.`price`,
        `products`.`sale_price`,
        `products`.`slug`,
        `products`.`name`,
        `products`.`stock_quantity`,
        `products`.`in_stock`,
        `products`.`id`')
            ->leftJoin('categories', 'categories.id', 'products.category_id')
            ->leftJoin('brands', 'brands.id', 'products.brand_id')
            ->where(function ($q) use ($search) {
                $q->where('products.name', "like", "%".$search."%");
                $q->orWhere('products.slug', 'like', "%".$search."%");
                $q->orWhere('products.short_name', 'like', "%".$search."%");
                $q->orWhere('categories.name', "like", "%".$search."%");
                $q->orWhere('categories.slug', 'like', "%".$search."%");
                $q->orWhere('brands.name', "like", "%".$search."%");
                $q->orWhere('brands.slug', 'like', "%".$search."%");
            });

        $result = $query->orderBy('products.id', 'desc')
            ->with(['images' => function ($query) {
                $this->productRepository->withProductImages($query);
            }])
            ->paginate($perPage);

        if ($search) {
            $result->appends(['search' => $search]);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function searchByCategory(Category $category, $perPage = 10, $pageNo = 1)
    {
        return Product::where('category_id', $category->id)
            ->with(['images' => function ($query) {
                $this->productRepository->withProductImages($query);
            }])->paginate($perPage);
    }

    /**
     * {@inheritdoc}
     */
    public function searchByBrand(Brand $brand, $perPage = 10, $pageNo = 1)
    {
        return Product::where('brand_id', $brand->id)
            ->with(['images' => function ($query) {
                $this->productRepository->withProductImages($query);
            }])->paginate($perPage);
    }
}
