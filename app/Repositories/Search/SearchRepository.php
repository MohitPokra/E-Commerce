<?php

namespace App\Repositories\Search;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;

interface SearchRepository
{
    /**
     * Search
     * @param null $search
     * @param int $perPage
     * @return mixed
     */
    public function search($search = null, $perPage = 10);

    /**
     * Return All the products belonging to the given Category.
     * @param Category $category
     * @param int $perPage
     * @param int $pageNo
     * @return mixed
     */
    public function searchByCategory(Category $category, $perPage = 10, $pageNo = 1);

    /**
     * Return All the products belonging to the given Brand.
     * @param Brand $brand
     * @param int $perPage
     * @param int $pageNo
     * @return mixed
     */
    public function searchByBrand(Brand $brand, $perPage = 10, $pageNo = 1);
}
