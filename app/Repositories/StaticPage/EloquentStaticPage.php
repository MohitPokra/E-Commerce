<?php

namespace App\Repositories\StaticPage;

use App\Models\StaticPage;
use App\Services\Logging\UserActivity\Activity;
use Carbon\Carbon;
use DB;
use phpDocumentor\Reflection\DocBlock\StandardTagFactory;

class EloquentStaticPage implements StaticPageRepository
{
    public function getPageData ($type) {

        $pageData = StaticPage::where('type', '=', $type)->get();

        return $pageData;
    }

    public function all() {
       return StaticPage::all();
    }

    public function deleteBySlug ($slug) {
        return StaticPage::where('slug', $slug)->delete();
    }

    public function findBySlug($slug) {
        return StaticPage::where('slug', '=', $slug)->first();
    }


}
