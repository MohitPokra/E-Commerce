<?php

namespace App\Repositories\StaticPage;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;

interface StaticPageRepository
{
   public function getPageData ($type);

   public function all ();

   public function deleteBySlug ($slug);

    public function findBySlug($slug);
}
