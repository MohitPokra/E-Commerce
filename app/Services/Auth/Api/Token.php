<?php

namespace App\Services\Auth\Api;

use Illuminate\Database\Eloquent\Model;

class Token extends BaseModel
{
    protected $table = 'api_tokens';

    public $incrementing = false;
}
