<?php

namespace App\Services\Logging\UserActivity;

use App\Models\BaseModel;
use App\Models\User;

class Activity extends BaseModel
{
    const UPDATED_AT = null;

    protected $table = 'user_activity';

    protected $fillable = ['description', 'user_id', 'ip_address', 'user_agent'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
