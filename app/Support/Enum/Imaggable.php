<?php

namespace App\Support\Enum;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;

/**
 * This class handle all attributes related to table who belongs to images.
 * Here every entity consists of three attributes, first is name of directory
 * where images related to this entity will be stored and retrieved. Second Attribute
 * represents the class name of corresponding model class. and Third Attribute
 * represents name of type of images that can be stored in that particular entity.
 *
 * One thing that needs to be keep in mind is that a trait 'App\Http\Traits\Imaggable'
 * must be used in each Model class of each entity which is imaggable.
*/
class Imaggable
{
    const product = [
        'storageDirectory' => 'product',
        'className' => Product::class,
        'resize' => ['width' => 500, 'height' => 500],
        'meta_type' => ['product1','product2','product3','product4','product5']
    ];

    const user = [
        'storageDirectory' => 'user',
        'className' => User::class,
        'meta_type' => ['avatar', 'background']
    ];

    const category = [
        'storageDirectory' => 'category',
        'className' => Category::class,
        'resize' => ['width' => 500, 'height' => 500],
        'meta_type' => ['category1', 'category2', 'category3']
    ];

    const brand = [
        'storageDirectory' => 'brand',
        'className' => Brand::class,
        'resize' => ['width' => 500, 'height' => 500],
        'meta_type' => ['brand1', 'brand2']
    ];

    public static function lists()
    {
        return [
            'product' => self::product,
            'user' => self::user,
            'category' => self::category,
            'brand' => self::brand,
        ];
    }
}
