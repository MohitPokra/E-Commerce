<?php

namespace App\Support\Enum;

class OrderStatus
{
    const ADDRESS = 'Address';
    const PENDING = 'Pending';
    const UNSHIPPED = 'UnShipped';
    const SHIPPED = 'Shipped';
    const DELIVERED = 'Delivered';
    const CANCELLED = 'Cancelled';

    public static function lists()
    {
        return [
            'ADDRESS' => self::ADDRESS,
            'PENDING' => self::PENDING,
            'UNSHIPPED' => self::UNSHIPPED,
            'SHIPPED' => self::SHIPPED,
            'DELIVERED' => self::DELIVERED,
            'CANCELLED' => self::CANCELLED
        ];
    }
}
