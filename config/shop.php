<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop config
    |--------------------------------------------------------------------------
    |
    | This file is for storing the basic shop configuration.  No need for database
    | for static information.
    |
    */

    'email' => env('SHOP_CONTACT_EMAIL', 'help@eshop.com'),
    'phone' => env('SHOP_CONTACT_PHONE', false),
    'address' => [
        'name' => env('SHOP_COMPANY_NAME', 'E-Commerce Shop'),
        'line1' => env('SHOP_ADDRESS_1', '123 Some St.'),
        'line2' => env('SHOP_ADDRESS_2', 'false'),
        'city' => env('SHOP_CITY', 'Springfield'),
        'state' => env('SHOP_STATE', 'GA'),
        'zip' => env('SHOP_ZIP', '10001-0001'),
    ],
];
