<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');


            $table->string('slug')->unique();
            $table->string('name');
            $table->string('short_name')->unique();
            $table->string('sku')->nullable()->default(null);
            $table->string('mpn')->nullable()->default(null);
            $table->string('upc')->nullable()->default(null);
            $table->boolean('is_gtin')->default(true);
            $table->string('asin')->nullable()->default(null);
            $table->decimal('price', 10, 6);
            $table->decimal('sale_price', 10, 6)->default(0);
            $table->boolean('on_sale')->default(false);
            $table->datetime('sale_ends_date')->nullable();

            $table->text('excerpt')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);

            $table->integer('category_id')->unsigned()->index();
            $table->integer('brand_id')->unsigned()->index();

            $table->boolean('is_available')->default(false);
            $table->boolean('in_stock')->default(false);
            $table->boolean('is_shop_featured')->default(false);
            $table->boolean('is_category_featured')->default(false);

            $table->text('meta_title')->nullable()->default(null);
            $table->text('meta_description')->nullable()->default(null);
            $table->timestamps();


        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
