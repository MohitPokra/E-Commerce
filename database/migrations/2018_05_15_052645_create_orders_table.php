<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->string('hash');
            $table->string('status');
            $table->float('amount')->comment('This is the amount without any discount, offer and taxes.');
            $table->float('total_discount')->comment('Aggregated discount amount after calculating discount over all products in the order.');
            $table->float('total_tax')->comment('Aggregated tax amount after calculating all taxes over all products in the order.');
            $table->float('bill_amount')->comment('This amount includes original amount, all taxes and discount.');
            $table->string('delivery_address');
            $table->date('order_date');
            $table->date('expected_delivery_date');
            $table->date('delivery_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
