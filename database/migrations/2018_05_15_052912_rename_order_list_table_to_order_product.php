<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOrderListTableToOrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
            Schema::table('order_list', function (Blueprint $table) {
                $table->dropForeign('order_list_user_id_foreign');
                $table->dropColumn('user_id');
            });
        Schema::enableForeignKeyConstraints();

        Schema::rename('order_list', 'order_product');

        Schema::table('order_product', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->after('id');
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->float('amount')->after('product_id');
            $table->boolean('on_sale')->after('amount');
            $table->float('sale_amount')->after('on_sale');
            $table->float('discount')->after('sale_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('order_product', 'order_list');
    }
}
