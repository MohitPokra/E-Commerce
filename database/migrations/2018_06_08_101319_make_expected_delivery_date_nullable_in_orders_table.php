<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeExpectedDeliveryDateNullableInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_address');
            $table->date('expected_delivery_date')->nullable()->change();
            $table->date('delivery_date')->nullable()->change();
            $table->date('shipping_date')->nullable();
            $table->string('shipping_tracking_no')->nullable();
            $table->string('shipping_service')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
