<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/23/18
 * Time: 4:09 PM
 */

use App\Models\User;
use App\Models\Address;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AddressSeeder extends Seeder
{
    public function run(){
        $faker = Faker::create();

        $users = User::all();
        foreach ($users as $user) {
            Address::create([
                'user_id' => $user->id,
                'type' => 'contact',
                'name' => $faker->buildingNumber,
                'line1' => $faker->streetName,
                'line2' => '',
                'city' => $faker->city,
                'country' => $faker->country,
                'zip' => $faker->postcode,
            ]);
        }
    }
}
