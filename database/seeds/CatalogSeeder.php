<?php
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\AddressOrder;
use App\Models\Payment;
use \App\Support\Enum\OrderStatus;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
            'name'=>'Great Gut',
            'slug'=>'great-gut',
            'meta_title'=>'Great Gut® Prebiotic Fiber Blends',
        ]);

        Brand::create([
            'name'=>'Kirkland Science Labs',
            'slug'=>'kirkland-science-labs',
            'meta_title'=>'Kirkland Science Labs Powerful Supplement Stacks',
        ]);

        Brand::create([
            'name'=>'Trusted Organics',
            'slug'=>'trusted-organics',
            'meta_title'=>'Trusted Organics Ethically Sourced Suplements',
        ]);

        Category::create([
            'name'=>'Organic Supplements',
            'slug'=>'organic',
            'is_featured'=>true,
            'meta_title'=>'Our Organic and Wild Crafted Supplements',
        ]);

        Category::create([
            'name'=>'Prebiotic Fiber Blends',
            'slug'=>'prebiotic-fiber',
            'is_featured'=>true,
            'meta_title'=>'Our Prebiotic Fiber Blends',
        ]);

        Category::create([
            'name'=>'Supplement Stacks',
            'slug'=>'supplement-stacks',
            'is_featured'=>true,
            'meta_title'=>'Our Supplement Stacks',
        ]);

        $to = Brand::where('name','Trusted Organics')->first();
        $gg = Brand::where('name','Great Gut')->first();
        $ksl = Brand::where('name','Kirkland Science Labs')->first();

        $org = Category::where('name', 'Organic Supplements')->first();
        $pre = Category::where('name', 'Prebiotic Fiber Blends')->first();
        $wght = Category::where('name', 'Supplement Stacks')->first();

        Product::create([
            'slug'=>'activated-charcoal-capsules',
            'name'=>'Natural Activated Charcoal Capsules',
            'short_name'=>'Charcoal',
            'sku'=>'TRUSTED-CHAR-CAPS',
            'mpn'=>'TRUSTED-CHAR-CAPS',
            'upc'=>'00861691000331',
            'asin'=>'B071RSRVJT',
            'price'=>'15.74',
            'sale_price'=>'12.77',
            'on_sale'=>true,
            'sale_ends_date'=> Carbon::now()->addDays(3)->format('Y-m-d H:i:s'),
            'excerpt'=>'Our organic coconuts are ethically sourced from local farmers. The charcoal comes from fresh coconut shells that were sustainably harvested in a recent growing season.',
            'description'=>'<p><b>Trusted Organics - Organic Activated Coconut Charcoal</b></p><p>Our organic activated charcoal comes from the shells of organically grown coconuts that were <b>ethically sourced and sustainably harvested</b> by local in farmers in Chile, the Philipines, or Sri Lanka. </p> <br /><p><b>DIGESTION AID, DETOX, and HANGOVERS:</b> Activated charcoal has been used as all-natural relief for digestive problems by humans for centuries. It also helps cleanse the body from toxins that build up in the liver and colon, or any toxins created as a byproduct of metabolizing alcohol. <br /></p> <p><b>FACIAL MASKS and BLACKHEAD REMOVAL:</b> The powerful adsorptive properties of activated charcoal bind toxins and impurities on the skin. The activated charcoal soaks up the toxins and gently removes them, purifying and cleansing the skin .</p> <p><b>TEETH WHITENING:</b> Activated charcoal helps whiten teeth by adsorbing plaque and tiny particles that stain teeth. It promotes general oral health by changing the pH balance in your mouth, working to prevent cavities, bad breath, and even gum disease.</p><p><b>100% Satisfaction Guarantee</b> We provide the highest quality organic charcoal in the world. If you are not fully satisfied for any reason, just return your charcoal within 30 days for a full refund of your purchase price.</p><p>Join the tens of thousands of people who have used all-natural activated charcoal to cleanse toxins and impurities from their livers, colons, skin or mouths.</p><p><b>Buy Now With a 100% Guarantee, You Have Nothing To Lose; So Scroll Up To The Top, Click "Add to Cart" and Get Your Organic Charcoal To Your Door In 2 Days!</b></p>',
            'category_id'=> $org->id,
            'brand_id'=> $to->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Trusted Organics | Natural Activated Charcoal Capsules | For Gut Health, Detox, Teeth Whitening, Facial Masks, and Hangovers | Formed From Organic Coconut Shells | Money Back Guarantee! | 90 Count',
        ]);

        Product::create([
            'slug'=>'wild-oregano-oil',
            'name'=>'Wild Oregano Oil',
            'short_name'=>'Oregano',
            'sku'=>'TRUSTED-OREG-OIL',
            'mpn'=>'TRUSTED-OREG-OIL',
            'upc'=>'00861691000348',
            'asin'=>'B074HM96ZT',
            'price'=>'14.77',
            'excerpt'=>' Our Oregano Oil is ethically sourced and produced from and handpicked wild oregano herbal plants by local farmers in Turkey in a recent growing season.',
            'description'=>'<p><b>Trusted Organics - Wild Oregano Oil 1:2 Blend</b></p> <p>Our wild oregano oil comes from wild oregano plants that were <b>hand picked</b> by local farmers and then gently steam distilled by local craftsmen in rural Turkey. </p> <br /> <p><b>MEDITERRANEAN SOURCED:</b> Mediterranean <b>Origanum vulgare</b> is well known as being the purest and most potent oregano available. Our polyphenol % is standardized to 85% much greater potency than P73 oregano blends<br /></p> <p><b>POWERFUL ANTIFUNGAL:</b> The powerful antifungal properties of wild oregano oil is your all-natural alternative to synthentic, industrial cremes full of additives, dyes, and other chemicals.</p> <p><b>NATURES ANTIVIRAL:</b> Wild Oregano Oil has powerful, natural, antiviral properties. It has been used for ages to treat colds, sore throats, runny noses and countless other virus related ailments.</p> <p><b>100% Satisfaction Guarantee</b> We provide the highest quality wild oregano oil in the world. If you are not fully satisfied for any reason, just return your bottle within 30 days for a full refund of your purchase price.</p> <p>Join the tens of thousands of people who have used wild oregano oil as Mother Natures antiviral, antifungal medical super star!</p> <p><b>Buy Now With a 100% Guarantee, You Have Nothing To Lose; So Scroll Up To The Top, Click "Add to Cart" and Get Your Wild Mediterranean Oregano Oil To Your Door In 2 Days!</b></p>',
            'category_id'=> $org->id,
            'brand_id'=> $to->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Trusted Organics | Wild Oregano Oil | Handpicked Mediterranean Oil of Oregano | 1:2 Mediterranean Blend | Great for Colds, Coughs and Sore Throats | Money Back Guarantee! | 1 fl oz',
        ]);

        Product::create([
            'slug'=>'black-seed-oil',
            'name'=>'Organic Black Seed Oil',
            'short_name'=>'Black Seed',
            'sku'=>'TRUSTED-BSEED-OIL',
            'mpn'=>'TRUSTED-BSEED-OIL',
            'upc'=>'00861691000355',
            'asin'=>'B074JG9VYR',
            'price'=>'17.47',
            'excerpt'=>'Our Black Seed Oil is ethically sourced and sustainably harvested by local farmers and cold-pressed by local craftsmen in rural India.',
            'description'=>'<p><b>Trusted Organics - Cold-Pressed Organic Black Seed Oil</b></p> <p>Our black seed oil comes from native Nigella sativa plants that were <b>sustainably harvested</b> by local farmers and then cold-pressed by expert craftsmen in outlands of India. </p> <br /> <p><b>PURE, UNDILUTED, NO HARSH EXTRACTANTS: At Trusted Organics we believe the best, most potent healer is Mother Nature herself. Thats why we only use the purest, least processed ingredients available in the world. </b><br /></p> <p><b>WEIGHT LOSS, BLOOD SUGAR, BLOOD PRESSURE:</b> The amazing medicinal properties of black seed oil has been used in Ayurvedic healing for generations.</p> <p><b>NATURAL ANTI-INFLAMMATORY:</b> The anti-inflammatory properties of black seed oil make it an impression all-natural treatment for many inflammation related ailments such as joint pain, as well as nose and throat inflammation</p> <p><b>100% Satisfaction Guarantee</b> We provide the highest quality cold-pressed organic black seed oil in the world. If you are not fully satisfied for any reason, just return your bottle within 30 days for a full refund of your purchase price.</p> <p>Join the tens of thousands of people who have used organic black seed oil as an all-natural treatment for dozens of different ailments all over the world!</p> <p><b>Buy Now With a 100% Guarantee, You Have Nothing To Lose; So Scroll Up To The Top, Click "Add to Cart" and Get Your Cold-Pressed Organic Black Seed Oil To Your Door In 2 Days!</b></p>',
            'category_id'=> $org->id,
            'brand_id'=> $to->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Trusted Organics | Organic Black Seed Oil | Cold-pressed, Additive-Free, Pure Nigella Sativa Oil | Great For Skin Care, Inflammation, and Appetite Control | Money Back Guarantee! | 8 Fl oz Bottle',
        ]);

        Product::create([
            'slug'=>'premium-prebiotic-oligo30',
            'name'=>'Oligo30 Premium Prebiotic Dietary Fiber',
            'short_name'=>'Oligo30',
            'sku'=>'KL-EATE-CWNT',
            'mpn'=>'KL-EATE-CWNT',
            'upc'=>'00861691000362',
            'asin'=>'B01B4ZE4LK',
            'price'=>'26.47',
            'excerpt'=>'Six clinically supported prebiotics that nourish good bacteria and suppress bad bacteria. Balance your biome with a tested and effective broad spectrum prebiotic blend.',
            'description'=>'Prebiotics provide the nutrition healthy gut bacteria (microbiota) need to function properly. With Oligo30s broad spectrum blend of clinically-tested FOS, XOS, GOS & gluten-free starch fortified with natural Kiwi Pectin & Polyphenols, you can discover the immune boost, mood, and metabolic benefits of balanced, healthy gut bacteria. Oligo30 is manufactured in an FDA-certified facility in the United States. <br><br> Gastroenterologists, hepatology experts, doctors and medical researchers support the role gut microbiota play in regulating physical, mental, and metabolic health. Oligo30s broad spectrum compound includes an essential prebiotic blend for gut health. Oligo30 contains a complete research-backed "4 Horseman of Gut Health" prebiotic formula including potent all-natural sources of Fructo-oligosaccharides (FOS), Xylo-oligosaccharides (XOS), Galactooligosaccharides (GOS), and gluten-free potato-based resistant starch. <br><br>Compare the Oligo30 prebiotic compound to other prebiotic supplements: A powder base helps your body absorb more of the active nutrients your gut needs. Dont count on pills to completely digest, especially if your microbiota is already imbalanced. <br><br>PROTECT: Our dietary fiber powder supports the growth of good bacteria while inhibiting bad bacteria. Promoting gut microbiome and overall health encouraging the growth of "friendly" gut flora for optimal digestion and healthy metabolism <br><br>FORMULA: FOS, GOS, & XOS and resistant starch feeds a wide array of gut bacteria featuring soluble and insoluble dietary fiber, resistant starch and prebiotic fiber powder. <br><br>REVITALIZE: Our resistance starch uses the gut-brain connection: heal your gut, help your brain <br><br>CLINICALLY TESTED & PROVEN: 300+ clinical studies support the effectiveness of RS, GOS, XOS, & FOS, the key ingredients in our prebiotic fiber powder <br><br>GUARANTEE: Using our resistance starch provides you with a 30-day money back guarantee',
            'category_id'=> $pre->id,
            'brand_id'=> $gg->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Great Gut Prebiotics | Oligo30 Premium Dietary Fiber Supplement | Broad Spectrum Prebiotic | Resistant Starch, GOS, FOS, XOS | Great For Digestive Health | Money Back Guarantee! | 41 Servings',
        ]);


        Product::create([
            'slug'=>'extra-strength-prebiotic-fiber',
            'name'=>'Extra-Strength Prebiotic Dietary Fiber',
            'short_name'=>'Extra Strength',
            'sku'=>'GGUT-PRE-FIBERX-30',
            'mpn'=>'GGUT-PRE-FIBERX-30',
            'upc'=>'00861691000300',
            'asin'=>'B01MTJXNGS',
            'price'=>'27.47',
            'sale_price'=>'25.77',
            'on_sale'=>true,
            'sale_ends_date'=> Carbon::now()->addDays(3)->format('Y-m-d H:i:s'),
            'excerpt'=>'Four researched backed effective prebiotics to nourish good bacteria and suppress bad bacteria. Including a full serving of the most medically prescribed fiber in the world, Sunfiber.',
            'description'=>'<b>Extra Strength Great Gut® Prebiotic Fiber Helps You Feel Normal After Every Meal</b><br/>Great Gut® is an exclusive blend of four research-based prebiotic fibers. It does what other fiber supplements can’t. Great Gut feeds more kinds of beneficial gut bacteria, which promotes normal digestive health and supports regularity. Great Gut® Extra-Strength Prebiotic Fiber dissolves easily and has a mildly sweet taste. And it’s well tolerated, when used as directed.<br/><br/>NORMALIZE HEALTH - Just add one scoop to every meal.<br/>ALL-NATURAL - We never use artificial sweeteners, dyes, perfumes or other gut-harming industrial chemicals.<br/> NON-GMO - Our extra-strength prebiotic dietary fiber is a powder supplement made from clean, all natural sources. It does not contain any genetically modified ingredients.<br/> NON-DAIRY - Our formula is always free of dairy, which is a known source of potential allergens.<br/> RESEARCH-BASED - Our trademarked prebiotic fibers deliver the best gut health benefits available on the market today.<br/><br/><b>TRY IT RISK-FREE FOR 30-DAYS</b><br/>We have a 30 day, no hassle, no questions asked refund policy.',
            'category_id'=> $pre->id,
            'brand_id'=> $gg->id,
            'is_available'=>true,
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Great Gut Prebiotics | Extra-Strength Dietary Fiber Supplement | Immune System Booster, Aids In Digestive Health | Money Back Guarantee! | 90 Servings',
        ]);

        Product::create([
            'slug'=>'low-fodmap-fiber',
            'name'=>'Low FODMAP Dietary Fiber',
            'short_name'=>'FODMAP',
            'sku'=>'GGUT-FODMAP-30',
            'mpn'=>'GGUT-FODMAP-30',
            'upc'=>'00861691000379',
            'asin'=>'B0754KLBSZ',
            'price'=>'26.47',
            'excerpt'=>'Three researched backed, effective prebiotics to nourish good bacteria and suppress bad bacteria. Including a full serving of the most medically prescribed fiber in the world, Sunfiber.',
            'description'=>'<b>Low FODMAP Prebiotic Fiber Helps You Feel Normal After Every Meal</b><br/>Great Gut® is an exclusive blend of three research-based low FODMAP fibers. It does what other fiber supplements can’t. Great Gut will get you the fiber you need without fast-fermenting fibers, which promotes normal digestive health and supports regularity. Great Gut® Low FODMAP Fiber dissolves easily and has a mildly sweet taste. And it’s well tolerated, when used as directed.<br/><br/>NORMALIZE HEALTH - Just take one scoop every day.<br/>ALL-NATURAL - We never use artificial sweeteners, dyes, perfumes or other gut-harming industrial chemicals.<br/> NON-GMO - Our Low FODMAP prebiotic dietary fiber is a powder supplement made from clean, all natural sources. It does not contain any genetically modified ingredients.<br/> NON-DAIRY - Our formula is always free of dairy, which is a known source of potential allergens.<br/> RESEARCH-BASED - Our fiber blend deliver the best gut health benefits available on the market today.<br/><br/><b>TRY IT RISK-FREE FOR 30-DAYS</b><br/>We have a 30 day, no hassle, no questions asked refund policy.',
            'category_id'=> $pre->id,
            'brand_id'=> $gg->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Great Gut Prebiotics | Low FODMAP Fiber Blend | Premium Dietary Fiber | Boost Digestive Health and Fight Bloating | Guaranteed to Work Or Your Money Back! | 30 Servings',
        ]);

        Product::create([
            'slug'=>'premium-pagg-stack',
            'name'=>'Premium PAGG Stack',
            'short_name'=>'PAGG',
            'sku'=>'DE-Q617-KE0S',
            'mpn'=>'DE-Q617-KE0S',
            'upc'=>'013964662528',
            'is_gtin'=>false,
            'asin'=>'B005OOE4S',
            'price'=>'28.47',
            'excerpt'=>' Calcium and Magnesium are exclusively in our formula, they work synergistically together and improve each other’s effectiveness; this is the real stacking dietary supplement – and no garlicky odor or after taste.',
            'description'=>'<p><b>Are you looking for the best quality PAGG stack that will help you get your 4-Hour Body. </b></p><p>Are you tired of mix and matching Tims ingredients or cheap imported clones that do not work or do not match Tims research?</p><p>Are you tired of slow-carbing it and working out to get fit, but not seeing the results than you would like? </p><p>If yes, this is your lucky day, because that is what our PAGG Stack is here for! </p><p><b>Benefit From The ONLY Biotin-Free PAGG Stack WITH Electrolytes In The World! </b></p><p>Yes, you read right! Our formula contains the exact stacked ingredients based upon Tims research and help you accelerate your weight loss when following the Slow Carb Diet – but, since it was shown to be competing with ALA for uptake, Biotin was removed from our ingredient list for good; and we are the FIRST ONES to offer you a completely Biotin-Free PAGG Stack dietary supplement! <p>Moreover, our supplement has no odor or garlicky taste and is absolutely safe for your body, without any side effects whatsoever!</p></p><p><b>Get the Results You Dream Of – Or We’ll Give You Your Money Back! </b></p><p>We are confident that you will LOVE our PAGG and it will help you build the best body you have ever had! In fact, we are so confident, that we offer you a hassle-free, no questions asked, full money back guarantee! </p><p><b>With a Satisfaction Guarantee, You Have Nothing To Lose; So Scroll Up To The Top, Click "Add to Cart" and Get Your PAGG Stack To Your Door In 2 Days!</b></p>',
            'category_id'=> $wght->id,
            'brand_id'=> $ksl->id,
            'is_available'=>true,
            'in_stock'=>true,
            'stock_quantity' => '100',
            'is_shop_featured'=>true,
            'is_category_featured' => true,
            'meta_title'=>'Kirkland Science Labs | Premium PAGG Stack | Boost Metabolism and Build Muscle | Top Quality Electrolytes, Green Tea and Aged Garlic Extract | For The Slow Carb Diet | No Risk Guarantee',
        ]);

        // Seed carts table
        DB::statement('INSERT INTO `carts` (`user_id`, `product_id`, `quantity`) VALUES (1, 2, 1), (1, 6, 1), (1, 7, 1), (1, 3, 1)');


        $faker = Faker::create();

        $orderStatusEnum = OrderStatus::lists();
        for ($i = 0; $i < 4; $i++) {
            $total_tax = rand(100, 200);
            $hash = substr(strtoupper(bin2hex(random_bytes(16))), 22);
            $order = Order::create([
                'user_id' => 1,
                'hash' => $hash,
                'status' => $orderStatusEnum[array_rand($orderStatusEnum)],
                'amount' => 0,
                'total_discount' => 0,
                'total_tax' => $total_tax,
                'bill_amount' => $total_tax,
                'order_date' => Carbon::today()->subDays(3)->addDays(rand(0,5)),
                'expected_delivery_date' => Carbon::today()->addDays(rand(5, 15)),
                'delivery_date' => Carbon::today()->addDays(rand(5, 10)),
            ]);

            if($order->status != $orderStatusEnum['ADDRESS']) {
                AddressOrder::create([
                    'order_id' => $order->id,
                    'first_name' => $faker->firstName,
                    'email' => $faker->email,
                    'phone' => $faker->phoneNumber,
                    'name' => $faker->buildingNumber,
                    'line1' => $faker->streetName,
                    'line2' => '',
                    'city' => $faker->city,
                    'country' => $faker->country,
                    'zip' => $faker->postcode
                ]);
                if ($order->status != $orderStatusEnum['PENDING']) {
                    Payment::create([
                        'order_id' => $order->id,
                        'payment_method' => 'stripe',
                        'status' => 'success',
                        'response'=> json_encode(['amount' => '0'])
                    ]);
                }
            }
        }

        // Seed order_products table
        for ($i = 0; $i < 12; $i++) {
            $order = Order::find(rand(1, 4));
            $product = Product::find(rand(1,7));
            $quantity = rand(1, 10);
            $discount = rand(0, 5);
            $amount = ($product->on_sale ? $product->sale_price : $product->price);
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'amount' => $product->price,
                'quantity' => $quantity,
                'on_sale' => $product->on_sale,
                'sale_amount' => $product->sale_price,
                'discount' => $discount]);

            $order->amount += $amount * $quantity;
            $order->total_discount += $discount * $quantity;
            $order->bill_amount += ($amount - $discount) * $quantity;
            $order->save();
        }

    }
}
