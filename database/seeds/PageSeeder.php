<?php

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Page::create([
            'slug' => 'deals',
            'title' => 'Current Deals',
            'layout' => 'one-column',
            'content' => "<h1>Our Deals</h1>",
        ]);

        Page::create([
            'slug' => 'shipping-policy',
            'title' => 'Shipping Policy',
            'layout' => 'one-column',
            'content' => "<h1>Shipping Policy</h1>",
        ]);

        Page::create([
            'slug' => 'returns-policy',
            'title' => 'Returns Policy',
            'layout' => 'one-column',
            'content' => "<h1>Returns Policy</h1>",
        ]);

        Page::create([
            'slug' => 'privacy-policy',
            'title' => 'Privacy Policy',
            'layout' => 'one-column',
            'content' => "<h1>Privacy Policy</h1>",
        ]);

        Page::create([
            'slug' => 'our-story',
            'title' => 'Our Story',
            'layout' => 'one-column',
            'content' => "<h1>Our Story</h1>",
        ]);

        Page::create([
            'slug' => 'faq',
            'title' => 'FAQ',
            'layout' => 'one-column',
            'content' => "<h1>Frequently Asked Questions</h1>",
        ]);
    }
}
