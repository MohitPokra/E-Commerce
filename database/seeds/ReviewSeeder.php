<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/23/18
 * Time: 4:09 PM
 */

use App\Models\Review;
use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    public function run(){

        $users = User::all()->toArray();
        $products = Product::all()->toArray();
        $review = [
            [
                'subject' => 'Average quality for the price',
                'text' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.'
            ],
            [
                'subject' => 'My husband love his new...',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
            ],
            [
                'subject' => 'Soft, comfortable, quite durable...',
                'text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.'
            ]
        ];

        for($i = 0; $i < 15; $i++) {
            $user = $users[array_rand($users)];
            $rev = $review[array_rand($review)];
            Review::create([
                'user_id' => $user['id'],
                'product_id' => $products[array_rand($products)]['id'],
                'review' => $rev['text'],
                'rating' => rand(1,5),
                'name' => $user['first_name'].' '.$user['last_name'],
                'email' => $user['email'],
                'subject' => $rev['subject']
            ]);
        }
    }
}
