<?php

use App\Models\StaticPage;
use Illuminate\Database\Seeder;

class StaticPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aboutUs = ['Our Story', 'Blog', 'Frequently Asked Questions', 'Contact Us', 'Affiliates'];
        $contactInfo = ['Your Account', 'Order Tracking', 'Shipping Rate & Policies', 'Refund & Replacement', 'Privacy Policy'];

        $aboutUsSetting = [];
        foreach($aboutUs as $about) {

            $page = StaticPage::create([
                'name' => $about,
                'detail' => '<h3>'.$about.'</h3>',
            ]);

            array_push($aboutUsSetting, [
                'text' => $about,
                'url' => route('static.page.show', ['slug' => $page->slug])
            ]);
        }

        Settings::set('footer_about_us', $aboutUsSetting);

        $accountInfo = [];
        foreach($contactInfo as $info) {

            $page = StaticPage::create([
                'name' => $info,
                'detail' => '<h3>'.$info.'</h3>',
            ]);

            array_push($accountInfo, [
                'text' => $info,
                'url' => route('static.page.show', ['slug' => $page->slug])
            ]);
        }

        Settings::set('footer_account_info', $accountInfo);

        Settings::save();

    }
}
