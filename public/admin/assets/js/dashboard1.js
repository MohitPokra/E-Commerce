"use strict";

$(document).ready(function () {

    $('#count-box').CountUpCircle({
        duration: 2500,
        opacity_anim: true,
        step_divider: 2
    });
    $('#count-box2').CountUpCircle({
        duration: 2500,
        opacity_anim: true,
        step_divider: 5
    });
    $('#count-box3').CountUpCircle({
        duration: 2500,
        opacity_anim: true,
        step_divider: 7
    });
    $('#count-box4').CountUpCircle({
        duration: 2500,
        opacity_anim: true,
        step_divider: 10
    });


    /*
     Background slideshow
     */
    $('.index-header').backstretch([
        "assets/img/4.jpg", "assets/img/3.jpg", "assets/img/2.jpg"
    ], {duration: 3000, fade: 750});


    // top sales visits and income widgets gradient

    var granimInstance1 = new Granim({
        element: '#canvas-interactive1',
        name: 'interactive-gradient1',
        elToSetClassOn: '.canvas-interactive-wrapper1',
        direction: 'diagonal',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        states: {
            "default-state": {
                gradients: [
                    ['#834d9b', '#d04ed6'],
                    ['#1CD8D2', '#93EDC7']
                ],
                transitionSpeed: 12000
            }
        }
    });
    var granimInstance2 = new Granim({
        element: '#canvas-interactive2',
        name: 'interactive-gradient2',
        elToSetClassOn: '.canvas-interactive-wrapper2',
        direction: 'diagonal',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        states: {
            "default-state": {
                gradients: [
                    ['#834d9b', '#d04ed6'],
                    ['#1CD8D2', '#93EDC7']
                ],
                transitionSpeed: 12000
            }
        }
    });
    var granimInstance3 = new Granim({
        element: '#canvas-interactive3',
        name: 'interactive-gradient3',
        elToSetClassOn: '.canvas-interactive-wrapper3',
        direction: 'diagonal',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        states: {
            "default-state": {
                gradients: [
                    ['#834d9b', '#d04ed6'],
                    ['#1CD8D2', '#93EDC7']
                ],
                transitionSpeed: 12000
            }
        }
    });

    var granimInstance4 = new Granim({
        element: '#canvas-interactive4',
        name: 'interactive-gradient4',
        elToSetClassOn: '.canvas-interactive-wrapper4',
        direction: 'diagonal',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        states: {
            "default-state": {
                gradients: [
                    ['#834d9b', '#d04ed6'],
                    ['#1CD8D2', '#93EDC7']
                ],
                transitionSpeed: 12000
            }
        }
    });

    function getMonthName(numericMonth) {
        var monthArray = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var alphaMonth = monthArray[numericMonth];

        return alphaMonth;
    }

    function convertToDate(timestamp) {
        var newDate = new Date(timestamp);
        var dateString = newDate.getMonth();
        var monthName = getMonthName(dateString);

        return monthName;
    }

});

