@extends('master')
@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>Contact / Shipping Address</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li><a href="{{ url('account-orders') }}">Account</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>Contact Address</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-1">
        <div class="row">
            @include('account.sidebar')
            <div class="col-lg-8">
                <div class="padding-top-2x mt-2 hidden-lg-up"></div>
                <h4>Contact Address</h4>
                <hr class="padding-bottom-1x">
                <form class="row" id="address_form">
                    {{ csrf_field() }}
                    <div class="col-md-6" id="contact-name">
                        <div class="form-group">
                            <label for="contact-company">Company</label>
                            <input class="form-control" type="text" name="name" @if($contactAddress != null)value="{{ $contactAddress->name ? : '' }}"@endif>
                            <div class="form-control-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6" id="contact-country">
                        <div class="form-group">
                            <label for="contact-country">Country</label>
                            <select class="form-control" name="country">
                                <option value="">Choose country</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($contactAddress != null && $contactAddress->country == $country->id) selected="selected"@endif>{{$country->name}}</option>
                                @endforeach
                            </select>
                            <div class="form-control-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6" id="contact-city">
                        <div class="form-group">
                            <label for="contact-city">City</label>
                            <input class="form-control" type="text" name="city" @if($contactAddress != null)value="{{ $contactAddress->city ? : '' }}"@endif required>
                            <div class="form-control-feedback"></div>
                            {{--<select class="form-control" id="contact-city">--}}
                                {{--<option>Choose city</option>--}}
                                {{--<option>Amsterdam</option>--}}
                                {{--<option>Berlin</option>--}}
                                {{--<option>Geneve</option>--}}
                                {{--<option selected>New York</option>--}}
                                {{--<option>Paris</option>--}}
                            {{--</select>--}}
                        </div>
                    </div>
                    <div class="col-md-6" id="contact-zip">
                        <div class="form-group">
                            <label for="contact-zip">ZIP Code</label>
                            <input class="form-control" type="text" name="zip" @if($contactAddress != null)value="{{ $contactAddress->zip ? : '' }}"@endif required>
                            <div class="form-control-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6" id="contact-line1">
                        <div class="form-group">
                            <label for="contact-address1">Address 1</label>
                            <input class="form-control" type="text" name="line1" @if($contactAddress != null)value="{{ $contactAddress->line1 ? : ''}}"@endif required>
                            <div class="form-control-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6" id="contact-line2">
                        <div class="form-group">
                            <label for="contact-address2">Address 2</label>
                            <input class="form-control" type="text" name="line2" @if($contactAddress != null) value="{{ $contactAddress->line2 ? : '' }}" @endif>
                            <div class="form-control-feedback"></div>
                        </div>
                    </div>
                    <div class="col-12 padding-top-1x">
                        <h4>Shipping Address</h4>
                        {{--@if(!isset($shippingAddress))--}}
                            <hr class="padding-bottom-1x">
                            <div class="custom-control custom-checkbox d-block">
                                <input class="custom-control-input" type="checkbox" onchange="toggleShippingAddress()" id="contact-same_address" name="same_address" id="same_address" @if($contactAddress == null || ($contactAddress != null && $contactAddress->type == 'same')) checked @endif>
                                <label class="custom-control-label" for="same_address">Same as Contact Address</label>
                            </div>
                        {{--@endif--}}
                        <hr class="margin-top-1x margin-bottom-1x">
                        <div id="shipping_address_container" class="row" @if(!isset($shippingAddress)) style="display: none" @endif>
                            <div class="col-md-6" id="contact-ship_name">
                                <div class="form-group">
                                    <label for="contact-company">Name</label>
                                    <input class="form-control" type="text" name="ship_name" @if(isset($shippingAddress))value="{{ $shippingAddress->name ? : '' }}"@endif>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6" id="contact-ship_country">
                                <div class="form-group">
                                    <label for="contact-country">Country</label>
                                    <select class="form-control" name="ship_country">
                                        <option value="">Choose country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if(isset($shippingAddress) && $shippingAddress->country == $country->id) selected="selected" @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6" id="contact-ship_city">
                                <div class="form-group">
                                    <label for="contact-city">City</label>
                                    <input class="form-control" type="text" name="ship_city" @if(isset($shippingAddress)) value="{{ $shippingAddress->city ? : '' }}" @endif required>
                                    <div class="form-control-feedback"></div>
                                    {{--<select class="form-control" id="contact-city">--}}
                                    {{--<option>Choose city</option>--}}
                                    {{--<option>Amsterdam</option>--}}
                                    {{--<option>Berlin</option>--}}
                                    {{--<option>Geneve</option>--}}
                                    {{--<option selected>New York</option>--}}
                                    {{--<option>Paris</option>--}}
                                    {{--</select>--}}
                                </div>
                            </div>
                            <div class="col-md-6" id="contact-ship_zip">
                                <div class="form-group">
                                    <label for="contact-zip">ZIP Code</label>
                                    <input class="form-control" type="text" name="ship_zip" @if(isset($shippingAddress))value="{{ $shippingAddress->zip ? : '' }}" @endif required>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6" id="contact-ship_line1">
                                <div class="form-group">
                                    <label for="contact-address1">Address 1</label>
                                    <input class="form-control" type="text" name="ship_line1" @if(isset($shippingAddress ))value="{{ $shippingAddress->line1 ? : ''}}" @endif required>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6" id="contact-ship_line2">
                                <div class="form-group">
                                    <label for="contact-address2">Address 2</label>
                                    <input class="form-control" type="text" name="ship_line2" @if(isset($shippingAddress )) value="{{ $shippingAddress->line2 ? : '' }}" @endif>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary margin-bottom-none" type="submit">Update Address</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    function toggleShippingAddress() {
        if($('#shipping_address_container').is(':hidden')){
            $('#shipping_address_container').show();
        }else {
            $('#shipping_address_container').hide();
        }
    }
    $('#address_form').on('submit', function (evt) {
        evt.preventDefault();
        const url = '{{ route('address.update') }}',
            data = $(this).serialize();

        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').text('').hide();
        $.ajax({
            type: 'POST',
            data: data,
            url: url,
            success: function () {
                showToaster({
                    type: 'success',
                    title: 'Success!',
                    'message': 'Contact address updated successfully.'
                })
            },
            error: function (error) {
                const statusCode = error.status;
                if (statusCode === 422) {
                    const errors = error.responseJSON;
                    _.forEach(errors, function (error, key) {
                        $('#contact-' + key + '> .form-group').addClass('has-danger');
                        $('#contact-' + key + '> .form-group > .form-control-feedback').text(error[0]).show();
                    });
                }
                else {
                    $('.container.padding-bottom-3x.mb-2').prepend('<div class="alert alert-danger alert-notification"><ul><li>An Unkonwn error has occured Please try again Later.</li></ul></div>');
                }
                showToaster({
                    type: 'danger',
                    title: 'Error',
                    'message': 'Some error has occur.'
                })
            }
        });
    });
</script>
@endsection
