@extends('master')
@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>My Orders</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li><a href="{{ url('account-orders') }}">Account</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>My Orders</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        <div class="row">
            @include('account.sidebar')
            <div class="col-lg-8">
                @if (count( $orders ) > 0)
                <div class="padding-top-2x mt-2 hidden-lg-up"></div>
                <div class="table-responsive">
                    <table class="table table-hover margin-bottom-none">
                        <thead>
                        <tr>
                            <th>Order #</th>
                            <th>Date Purchased</th>
                            <th>Status</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td><a class="text-medium navi-link"
                                            onclick="showOrderDetailsModal(event, '{{ $order->hash }}')"
                                    >{{ $order->hash }}</a></td>
                                <td>{{ $order->order_date->toFormattedDateString() }}</td>
                                <td>
                                    @switch ($order->status)
                                        @case($orderStatus['ADDRESS'])
                                            <a href="{{ route('order.address.get', $order->hash) }}" class="navi-link text-warning">Address</a>
                                            @break
                                        @case($orderStatus['PENDING'])
                                            <a href="{{ route('order.payment.get', $order->hash) }}" class="navi-link text-warning">Pending</a>
                                            @break
                                        @case($orderStatus['UNSHIPPED'])
                                            <a href="{{ route('order.review.get', $order->hash) }}" class="navi-link text-info">Unshipped</a>
                                            @break
                                        @case($orderStatus['SHIPPED'])
                                            <a href="{{ route('order.review.get', $order->hash) }}" class="navi-link text-info">Shipped</a>
                                            @break
                                        @case($orderStatus['DELIVERED'])
                                            <a href="{{ route('order.review.get', $order->hash) }}" class="navi-link text-success">Delivered</a>
                                            @break
                                        @case($orderStatus['CANCELLED'])
                                            <a href="#" class="navi-link text-danger">Canceled</a>
                                            @break
                                        @default
                                            <a href="#" class="navi-link text-danger">-</a>
                                    @endswitch
                                </td>
                                <td><span class="text-medium">${{ $order->bill_amount }}</span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <hr>
                {{--<div class="text-right"><a class="btn btn-link-primary margin-bottom-none" href="#"><i--}}
                                {{--class="icon-download"></i>&nbsp;Order Details</a></div>--}}
                @else
                    <div class="text-center">
                        <span class="btn btn-link-primary margin-bottom-none">No orders to display</span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section ('scripts')
    <!-- Open Ticket Modal-->
    <div class="modal fade" id="orderDetails" tabindex="-1">
    </div>
    <script type="text/javascript">
        function showOrderDetailsModal(evt, hash) {
            evt.preventDefault();
            let url= '{{ route('order.details.get', ':hash') }}';
            url = url.replace(':hash', hash);

            $.ajax({
                url: url,
                success: function (resp) {
                    $('#orderDetails').html(resp.data).modal('toggle');
                },
                error: function () {
                    return false;
                }
            });
        }
    </script>
@endsection
