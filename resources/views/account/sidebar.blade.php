<div class="col-lg-4">
    <aside class="user-info-wrapper">
        <div class="user-cover" style="background-image: url(template/dist/img/account/user-cover-img.jpg);">
            <div class="info-label" data-toggle="tooltip" title="You currently have 290 Reward Points to spend"><i class="icon-medal"></i>290 points</div>
        </div>
        <div class="user-info">
            <div class="user-avatar"><a class="edit-avatar" href="#"></a><img src="{{ $account->gravatar() }}" alt="User"></div>
            <div class="user-data">
                <h4>{{ $account->first_name.' '.$account->last_name }}</h4><span>Joined {{ $account->created_at->toFormattedDateString() }}</span>
            </div>
        </div>
    </aside>
    <nav class="list-group">
        <a class="list-group-item with-badge @if(Route::currentRouteName() == 'account.orders' ) active @endif"
           href="{{ url('account-orders') }}">
            <i class="icon-bag"></i>Orders
            <span class="badge badge-primary badge-pill">{{ $orderCount }}</span>
        </a>
        <a class="list-group-item @if(Route::currentRouteName() == 'account.profile' ) active @endif"
           href="{{ url('account-profile') }}">
            <i class="icon-head"></i>Profile
        </a>
        <a class="list-group-item @if(Route::currentRouteName() == 'account.address' ) active @endif"
           href="{{ url('account-address') }}">
            <i class="icon-map"></i>Addresses
        </a>
        {{--<a class="list-group-item with-badge @if(Route::currentRouteName() == 'account.wishList' ) active @endif"--}}
           {{--href="{{ url('account-wishlist') }}">--}}
            {{--<i class="icon-heart"></i>Wishlist--}}
            {{--<span class="badge badge-primary badge-pill">{{ $wishListCount }}</span>--}}
        {{--</a>--}}
        {{--<a class="list-group-item with-badge @if(Route::currentRouteName() == 'account.tickets' )active@endif" href="{{ url('account-tickets') }}">--}}
            {{--<i class="icon-tag"></i>My Tickets--}}
            {{--<span class="badge badge-primary badge-pill">4</span>--}}
        {{--</a>--}}
    </nav>
</div>
