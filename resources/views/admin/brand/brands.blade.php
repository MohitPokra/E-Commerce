@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Brands
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/buttons.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/scroller.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/datatables_custom.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>
    <!--end of page level css-->

@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            Brands
            <button type="button" data-target="#add" data-toggle="modal"  data-placement="top" class="btn btn-primary btn-sm float-right" id="addButton"><i class="fa fa-plus" aria-hidden="true"></i> Add Brand</button>
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Brands</a>
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content no-padding">
        <div class="table-responsive container-no-padding">
            <table class="table table-striped table-bordered table-hover" id="category">
                <thead>
                <tr id="Brand-header">
                    <th>#</th>
                    <th> Name </th>
                    <th> Slug </th>
                    <th>Actions</th>
                </thead>
            </table>
        </div>

        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading">Add Brand</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form id="add-brand-form">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control " type="text" name="name" placeholder="Enter Brand Name">
                                <small class="help-block"></small>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <input class="form-control " type="text" name="meta_title" placeholder="Enter Meta Title">
                                <small class="help-block"></small>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <input class="form-control " type="text" name="meta_description" placeholder="Enter Meta Description">
                                <small class="help-block"></small>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="addBrand">
                            <span class="fa fa-check"></span> Add
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading5">Delete this entry</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <span class="glyphicon glyphicon-info-sign"></span>&nbsp; Are you sure you want to
                            delete this Brand ?
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteBrand">
                            <span class="fa fa-check"></span> Yes
                        </button>
                        <button type="button" class="btn btn-success">
                            <span class="glyphicon glyphicon-remove"></span> No
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    
    <!-- /.modal ends here -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>


    <script>

        var brandSlug = null;
        $(document).ready(function(){
            dataTables();
        });

        function dataTables() {

            $('#category').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true ,
                ajax: '{!! route('admin.ajax_brands') !!}',
                "order": [[ 0, "desc" ]],
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_Row_Index', orderable: true, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'slug', name: 'slug' },
                    { data: 'actions', name: 'actions', width: '20%' }
                ]
            });
        }

        function OpenEditModel(brand,slug) {
            let url = '{{ route('admin.brands.edit',':slug') }}';
            url = url.replace(':slug', slug);
            window.location.href = url;
        }

        function OpenDeleteModel(el, id) {
            brandSlug = id;
        }

        $('#deleteBrand').on('click', function () {

            var url = "{{route('admin.brands.destroy', 'id')}}";
            url = url.replace('id', brandSlug);
            var token = "{{csrf_token()}}";
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: "JSON",
                data: { '_token': token},
                success: function (response) {
                    if(response.status === 'success') {
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error:function () {

                }
            })
        })

        $('#addBrand').on('click', function () {
            var data = $("#add-brand-form").serialize();
            var token = "{{csrf_token()}}";
            $.ajax({
                type: 'POST',
                url: "{{route('admin.brands.store')}}",
                dataType: "JSON",
                data:  data + "&_token=" + token,
                success: function (response) {
                    if(response.status === 'success') {
                        $('#add').modal('hide');
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#add-brand-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }
                }
            })
        })


        $('#updateBrand').on('click', function () {
            var url = "{{route('admin.brands.update', 'id')}}";
            url = url.replace('id', brandSlug);
            $.ajax({
                type: 'PUT',
                url:  url,
                dataType: "JSON",
                data: $("#edit-brand-form").serialize(),
                success: function (response) {
                    if(response.status === 'success') {
                        $('#edit').modal('hide');
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error:function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#edit-brand-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }
                }
            })
        })
    </script>
@stop
