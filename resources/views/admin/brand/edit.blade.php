@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Brands
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/dropzone.css') }}" />
@stop

{{-- Page content --}}
@section('content')
        <section class="content-header">
            <h1>
                Edit Brand
                <button type="button" data-target="#add" data-toggle="modal"  data-placement="top" class="btn btn-primary btn-sm float-right" id="addButton"><i class="fa fa-plus" aria-hidden="true"></i> Edit Brand</button>
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Edit Brand</a>
                </li>
            </ol>
        </section>

        <section>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card border-info ">
                        <div class="card-header bg-info text-white">
                            <h3 class="card-title d-inline">
                                <i class="fa fa-fw fa-info-circle"></i> Images
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-{{ 6 - count($brand->images) }}"></div>
                                @foreach($brand->images as $image)
                                    <div class="col-sm-2 hover-delete">
                                        <span class="hove-delete-button" data-name="{{ $image->given_name }}" data-type="brand" data-id="{{ $brand->id }}"><i class="fa fa-times"></i></span>
                                        <img width="100%" style="border-radius: 5px;" src="{{ $image->brand_image_url }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('image.upload') }}"
                                  class="dropzone"
                                  id="my-awesome-dropzone" style=" border: dashed 1px #666; border-radius: 5px;">
                                <input type="hidden" name="type" value="brand">
                                <input type="hidden" name="id" value="{{ $brand->id }}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Main content -->
        <section class="content no-padding">
            <form id="edit-brand-form">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card border-info ">
                            <div class="card-header bg-info text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-fw fa-info-circle"></i> Details
                                </h3>
                            </div>
                            <div class="card-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name" value="{{ $brand->name }}" placeholder="Enter Brand Name">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Slug</label>
                                    <input class="form-control" type="text" name="slug" value="{{ $brand->slug }}" placeholder="Enter Slug">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Meta title</label>
                                    <input class="form-control" type="text" value="{{ $brand->meta_title }}" name="meta_title" placeholder="Enter Meta Title">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <input class="form-control" type="text" value="{{ $brand->meta_description }}" name="meta_description" placeholder="Enter Meta Description">
                                    <small class="help-block"></small>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-success" id="updateBrand">
                                        <span class="fa fa-check"></span> Update
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->

<script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>

<script>
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        maxFiles: 2,
        accept: function(file, done) {
            console.log("uploaded");
            done();
        },
        init: function() {
            this.on("maxfilesexceeded", function(file){
                alert("No more files please!");
            });
        }
    };

    $('#edit-brand-form').on('submit', function (event) {
        event.preventDefault();
        const url = "{{route('admin.brands.update', $brand->slug)}}";
        const data =  $(this).serialize();
          $.ajax({
              type: 'PUT',
              url:  url,
              data: data,
              success: function (response) {
                  window.location.reload();
              },
              error:function (response) {
                  if(response.status === 422 || response.responseJSON) {
                      $(".has-error").removeClass("has-error");
                      $(".help-block").text('');
                      $.each(response.responseJSON, function (key, value) {
                          const input = '#edit-brand-form input[name=' + key + ']';
                          $(input + '+small').text(value[0]);
                          $(input).parent().addClass('has-error');
                      })
                  }
                  else {
                      toastr["fail"]("Unknown error has occurred");
                  }

              }

          })
    });

</script>
@stop
