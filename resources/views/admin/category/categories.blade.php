@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Categories
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/buttons.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/scroller.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/datatables_custom.css')}}">
    <!--end of page level css-->

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


@stop

{{-- Page content --}}
@section('content')
        <section class="content-header">
            <h1>
                Categories
                <button type="button" data-target="#add" data-toggle="modal"  data-placement="top" class="btn btn-primary btn-sm float-right" id="addButton"><i class="fa fa-plus" aria-hidden="true"></i> Add Category</button>
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Categories</a>
                </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content no-padding">
            <div class="table-responsive container-no-padding">
                <table class="table table-striped table-bordered table-hover" id="category">
                    <thead>
                    <tr id="category-header">
                        <th>#</th>
                        <th> Name </th>
                        <th> Slug </th>
                        <th> Is Featured </th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title custom_align" id="Heading">Add Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form id="add-category-form">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control " type="text" name="name" placeholder="Enter Category Name">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Meta Title</label>
                                    <input class="form-control " type="text" name="meta_title" placeholder="Enter Meta Title">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <input class="form-control " type="text" name="meta_description" placeholder="Enter Meta Description">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <div class="form-check abc-checkbox abc-checkbox-success ">
                                        <input class="form-check-input"  type="hidden" name="is_featured" value="0">
                                        <input class="form-check-input" id="checkbox1" type="checkbox" name="is_featured" value="1" checked>
                                        <label class="form-check-label" for="checkbox1">
                                            Is Featured
                                        </label>
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="addCategory">
                                <span class="fa fa-check"></span> Add
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title custom_align" id="Heading5">Delete this Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info">
                                <span class="glyphicon glyphicon-info-sign"></span>&nbsp; Are you sure you want to
                                delete this Category ?
                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteCategory">
                                <span class="fa fa-check"></span> Yes
                            </button>
                            <button type="button" class="btn btn-success" data-dismiss="modal">
                                <span class="glyphicon glyphicon-remove"></span> No
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        
            <!-- /.modal ends here -->
        </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>


<script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>


    <script>

        var categorySlug = null;
        $(document).ready(function(){
            dataTables();
        });

        function dataTables() {

           $('#category').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true ,
                ajax: '{{ route('admin.ajax_categories') }}',
                "order": [[ 0, "desc" ]],
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_Row_Index', orderable: true, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'slug', name: 'slug' },
                    { data: 'is_featured', name: 'is_featured' },
                    { data: 'actions', name: 'actions', width: '20%' }
                ]
            });
        }

        function editCategory(category, slug) {
            let url = '{{ route('admin.categories.edit',':slug') }}';
            url = url.replace(':slug', slug);
            window.location.href = url;
        }

        function destroyCategory(el, id) {
            categorySlug = id;
        }

        $('#deleteCategory').on('click', function () {
            var url = "{{route('admin.categories.destroy', 'id')}}";
            var token = "{{csrf_token()}}";
            url = url.replace('id', categorySlug);
             $.ajax({
                 type: 'DELETE',
                 url: url,
                 dataType: "JSON",
                 data: { '_token': token},
                 success: function (response) {
                     if(response.status === 'success') {
                         toastr[response.status](response.message);
                         dataTables();
                     }
                 },
                 error: function () {
                     toastr["fail"]("Unknown error has occurred");
                 },
             })
        });

        $('#addCategory').on('click', function () {
            var data = $("#add-category-form").serialize();
            var token = "{{csrf_token()}}";
            $.ajax({
                type: 'POST',
                url: "{{route('admin.categories.store')}}",
                dataType: "JSON",
                data:  data + "&_token=" + token,
                success: function (response) {
                    if(response.status === 'success') {
                        $('#add').modal('hide');
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#add-category-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }
                }
            })
        })

    </script>
@stop
