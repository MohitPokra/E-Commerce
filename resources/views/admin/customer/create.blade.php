@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
  Add User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .checkbox-display {
            display: inline !important;
        }
    </style>

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/iCheck/css/all.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/bootstrap-fileinput/css/fileinput.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/gridforms/css/gridforms.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/form_layouts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Add User
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.customers.index') }}">Users</a>
            </li>
            <li class="breadcrumb-item active">
               Add User
            </li>
        </ol>
    </section>

    <form id="create-customer-form" enctype="multipart/form-data">
        <!-- Main content -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card border-info ">
                    <div class="card-header bg-info text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-fw fa-user"></i> General Details
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label">
                                User Profile Image
                            </label>
                            <div class="mb-1">
                                    <img height="150px" width="150px" class="img-responsive mb-2 user-image"  src="{{asset_url('defaults/profile_image.jpg')}}"/>
                            </div>
                            <input type="hidden" class="user-image-url" name="avatar" value="">
                            <input type="file" data-attr=".user-image" class="file-loading input-21 image-picker" name="avatar">
                            <small class="help-block"></small>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control " type="text" name="first_name">
                                    <small class="help-block"></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control " type="text" name="last_name">
                                    <small class="help-block"></small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input class="form-control " type="text" name="username">
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control " type="email" name="email">
                            <small class="help-block"></small>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control " type="password" name="password">
                                    <small class="help-block"></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password Confirmation</label>
                                    <input class="form-control" type="password" name="password_confirmation">
                                    <small class="help-block"></small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control form-control-sm" name="role_id">
                                @foreach($roles as $key => $role)
                                    <option value="{{$key}}" selected>{{$role}}</option>
                                @endforeach
                            </select>
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card border-success ">
                    <div class="card-header bg-success text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-fw fa-user"></i> Contact & Other Information
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Phone</label>
                            <input class="form-control " type="text" name="phone">
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control " type="text" name="address" row="2"></textarea>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <select class="form-control form-control-sm" name="country_id">
                                @foreach($countries as $key => $country)
                                    <option value="{{$key}}" selected>{{$country}}</option>
                                @endforeach
                            </select>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Birthday</label>
                            <input class="form-control date-picker"  name="birthday"  placeholder="MM/DD/YYY" type="text"/>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Reward amount</label>
                            <input class="form-control " type="number" name="reward_amount">
                            <small class="help-block"></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 offset-md-4 text-center">
                <button type="button" class="btn btn-success" id="add-customer">
                    <span class="fa fa-check"></span> Add
                </button>
            </div>
        </div>
    </form>
    
@stop

@section('footer_scripts')

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}" ></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_layouts.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/theme.js')}}">  </script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_elements.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            var date_input=$('.date-picker');
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });

        function removeImage(id, el) {
            $(id+'-url').val(null);
            $(el).hide();
            $(id).hide();
        }


        $('.image-picker').on('change', function () {
            var id = $(this).data('attr');
            $(id).hide();
            $('.fileinput-remove').on('click', function() {
                $(id).show();
            });
        })

        $('#add-customer').on('click', function () {

            var data = new FormData($("#create-customer-form")[0]);
            var token = "{{csrf_token()}}";
            data.append('_token', token);
            $.ajax({
                type: 'POST',
                url: "{{route('admin.customers.store')}}",
                data: data,
                contentType: false,
                processData: false,
                success: function (response) {

                    if(response.status == 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.customers.index")}}';
                        }, 1000);
                    }
                },
                error: function (response) {
                    if(response.status === 422 && response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#create-customer-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        });
    </script>
@stop

