@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
     Edit/View User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .checkbox-display {
            display: inline !important;
        }
        .two-factor-div {
            padding-right: 75px;
        }
    </style>

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/iCheck/css/all.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/bootstrap-fileinput/css/fileinput.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/gridforms/css/gridforms.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/form_layouts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit User
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.customers.index') }}">Users</a>
            </li>
            <li class="breadcrumb-item active">
               Edit User
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
            <div class="row">
                <form class="col-md-12 mb-4" id="edit-customer-form" enctype="multipart/form-data">
                   <div class="col-lg-12 row">
                        <div class="col-lg-6 col-12">
                            <div class="card border-primary">
                                <div class="card-header text-white bg-primary">
                                    <h3 class="card-title d-inline">
                                        <i class="fa fa-fw fa-user"></i> General User Details
                                    </h3>
                                    <span class="pull-right hidden-xs">
                                                                <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                                <i class="fa fa-fw fa-times removepanel"></i>
                                                            </span>
                                </div>
                                <div class="card-body">
                                        {{method_field('PUT')}}
                                        <div class="form-group">
                                            <label class="control-label">
                                                User Image
                                            </label>
                                            <div class="mb-1">
                                                @if($user->avatar)
                                                    <img height="150px" width="150px" class="img-responsive mb-2 user-image"  src="{{$user->avatar}}"/>
                                                @else
                                                    <img height="150px" width="150px" class="img-responsive mb-2 user-image"  src="{{asset_url('defaults/profile_image.jpg')}}"/>
                                                @endif
                                            </div>
                                            <input type="hidden" class="user-image-url" name="avatar" value="{{$user->avatar}}">
                                            <input type="file" data-attr=".user-image" class="file-loading input-21 image-picker" name="avatar">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input class="form-control " type="text" name="first_name" value="{{$user->first_name}}">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input class="form-control " type="text" name="last_name" value="{{$user->last_name}}">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Role</label>
                                            <select class="form-control form-control-sm" name="role_id">
                                                @foreach($roles as $key => $role)
                                                    @if($key === $user->role_id)
                                                        <option value="{{$key}}" selected>{{$role}}</option>
                                                    @else
                                                        <option value="{{$key}}">{{$role}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control form-control-sm" name="status">
                                                @foreach($statuses as $status)
                                                    @if($status === $user->$status)
                                                        <option value="{{$status}}" selected>{{$status}}</option>
                                                    @else
                                                        <option value="{{$status}}">{{$status}}</option>
                                                    @endif

                                                @endforeach
                                            </select>
                                            <small class="help-block"></small>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="card border-success">
                                <div class="card-header text-white bg-success">
                                    <h3 class="card-title d-inline">
                                        <i class="fa fa-fw fa-check-circle-o"></i> User LogIn Details
                                    </h3>
                                    <span class="pull-right hidden-xs">
                                                                <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                                <i class="fa fa-fw fa-times removepanel"></i>
                                                            </span>
                                </div>
                                <div class="card-body">
                                        {{method_field('PUT')}}
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control " type="text" name="username" value="{{$user->username}}">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control " type="email" name="email" value="{{$user->email}}">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control " type="password" name="password">
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Password Confirmation</label>
                                            <input class="form-control" type="password" name="password_confirmation">
                                            <small class="help-block"></small>
                                        </div>
                                </div>
                            </div>
                            <div class="card border-success ">
                                 <div class="card-header bg-success text-white">
                                     <h3 class="card-title d-inline">
                                         <i class="fa fa-fw fa-user"></i> Contact & Other Information
                                     </h3>
                                 </div>
                                 <div class="card-body">
                                     <div class="form-group">
                                         <label>Phone</label>
                                         <input class="form-control " type="text" name="phone">
                                         <small class="help-block"></small>
                                     </div>
                                     <div class="form-group">
                                         <label>Address</label>
                                         <textarea class="form-control " type="text" name="address" row="2"></textarea>
                                         <small class="help-block"></small>
                                     </div>
                                     <div class="form-group">
                                         <label>Country</label>
                                         <select class="form-control form-control-sm" name="country_id">
                                             @foreach($countries as $key => $country)
                                                 <option value="{{$key}}" selected>{{$country}}</option>
                                             @endforeach
                                         </select>
                                         <small class="help-block"></small>
                                     </div>
                                     <div class="form-group">
                                         <label>Birthday</label>
                                         <input class="form-control date-picker"  name="birthday"  placeholder="MM/DD/YYY" type="text"/>
                                         <small class="help-block"></small>
                                     </div>
                                     <div class="form-group">
                                         <label>Reward amount</label>
                                         <input class="form-control " type="number" name="reward_amount">
                                         <small class="help-block"></small>
                                     </div>
                                 </div>
                             </div>
                        </div>
                   </div>
                    <div class="row">
                        <div class="col-md-4 offset-md-4 text-center">
                            <button type="button" class="btn btn-success" id="edit-customer">
                                <span class="fa fa-check"></span> Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
    </section>

    <section>
        <div class="row">
            <div class="col-md-12 two-factor-div">
            @if (settings('2fa.enabled'))
                <div class="card border-success">
                    <div class="card-header text-white bg-success">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-shield" aria-hidden="true"></i> Two-Factor Authenticaton
                        </h3>
                        <span class="pull-right hidden-xs">
                                                                <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                                <i class="fa fa-fw fa-times removepanel"></i>
                                                            </span>
                    </div>
                    <div class="card-body">
                        @if (! Authy::isEnabled($account))
                            <form id="two-factor-auth-enable">
                                {{csrf_field()}}
                                <div class="alert alert-warning">
                                    @lang('app.in_order_to_enable_2fa_you_must') <a target="_blank" href="https://www.authy.com/">Authy</a> @lang('app.application_on_your_phone').
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="country_code">@lang('app.country_code')</label>
                                        <input type="text" class="form-control" id="country_code" placeholder="381"
                                               name="country_code" value="{{ $account->two_factor_country_code }}">
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone_number">@lang('app.cell_phone')</label>
                                        <input type="text" class="form-control" id="phone_number" placeholder="@lang('app.phone_without_country_code')"
                                               name="phone_number" value="{{ $account->two_factor_phone }}">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 offset-md-4 text-center">
                                        <button type="submit" class="btn btn-primary align-content-center" data-toggle="loader" data-loading-text="@lang('app.enabling')" id="enableTwoFactorAuth">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            @lang('app.enable')
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <form id="two-factor-auth-disable">
                                <div class="row">
                                    <div class="col-md-4 offset-md-4 text-center">
                                        <div class="form-group">
                                            {{csrf_field()}}
                                            <input type="checkbox" checked data-toggle="toggle" onchange="disableTwoFactorAuth()">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        </div>
    </section>

@stop

@section('footer_scripts')

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}" ></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_layouts.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/theme.js')}}">  </script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_elements.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script>

        $(document).ready(function(){
            var date_input=$('.date-picker');
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });

        function removeImage(id, el) {
            $(id+'-url').val(null);
            $(el).hide();
            $(id).hide();
        }

        $('.image-picker').on('change', function () {
            var id = $(this).data('attr');
            $(id).hide();
            $('.fileinput-remove').on('click', function() {
                $(id).show();
            });
        });


        $('#enableTwoFactorAuth').on('click', function(e) {
            e.preventDefault();
            var data = $('#two-factor-auth-enable').serialize();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.user.two-factor.enable', [$user->id])}}",
                data: data,
                dataType: "JSON",
                success: function (response) {

                    if(response.status == 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.customers.index")}}';
                        }, 1000);

                    }
                    else if (response.status === 'error') {
                        toastr[response.status](response.message);
                    }

                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            console.log(key, value);
                            var input = '#two-factor-auth-enable input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        });

        function disableTwoFactorAuth () {

            var data = $('#two-factor-auth-disable').serialize();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.user.two-factor.disable', [$user->id])}}",
                data: data,
                dataType: "JSON",
                success: function (response) {

                    if(response.status == 'success') {
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.customers.index")}}';
                        }, 1000);

                    }
                    else if (response.status === 'error') {
                        toastr[response.status](response.message);
                    }

                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        }

        $('#update-login-details').on('click', function (e) {
            e.preventDefault();
            var data = $('#login-details-form').serialize();

            $.ajax({
                type: 'POST',
                url: "{{route('admin.user.update.login-details', [$user->id])}}",
                data: data,
                dataType: "JSON",
                success: function (response) {

                    if(response.status == 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.customers.index")}}';
                        }, 1000);

                    }
                    else if (response.status === 'error') {
                        toastr[response.status](response.message);
                    }

                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            console.log(key, value);
                            var input = '#login-details-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        });


        $('#edit-customer').on('click', function () {

            var data = new FormData($("#edit-customer-form")[0]);
            var token = "{{csrf_token()}}";
            data.append('_token', token);
            $.ajax({
                type: 'POST',
                url: "{{route('admin.customers.update.detail', [$user->id])}}",
                data: data,
                contentType: false,
                processData: false,
                success: function (response) {

                    if(response.status == 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.customers.index")}}';
                        }, 1000);

                    }

                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#edit-customer-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        });
    </script>
@stop

