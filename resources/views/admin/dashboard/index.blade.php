@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Core + Admin Template
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .sale-chart-filter{
            display: flex;
            justify-content: flex-end;
        }

        #demo-wrapper {
            max-width: 1000px;
            margin: 0 auto;
            height: 560px;
        }
        #mapBox {
            width: 100%;
            float: left;
        }
        #container {
            height: 500px;
        }
        #sideBox {
            float: right;
            width: 16%;
            margin: 100px 1% 0 1%;
            padding-left: 1%;
            border-left: 1px solid silver;
            display: none;
        }
        #infoBox {
            margin-top: 10px;
        }
        .or-view-as {
            margin: 0.5em 0;
        }
        #up {
            height: 20px;
            max-width: 400px;
            margin: 0 auto;
        }
        #up a {
            cursor: pointer;
            padding-left: 40px;
        }
        .selector {
            height: 40px;
            max-width: 400px;
            margin: 0 auto;
            position: relative;
        }
        .selector .prev-next {
            position: absolute;
            padding: 0 10px;
            font-size: 30px;
            line-height: 20px;
            background: white;
            font-weight: bold;
            color: #999;
            top: -2px;
            display: none;
            border: none;
        }
        .selector .custom-combobox {
            display: block;
            position: absolute;
            left: 40px;
            right: 110px;
        }
        .selector .custom-combobox .custom-combobox-input {
            position: absolute;
            font-size: 14px;
            color: silver;
            border-radius: 0;
            height: 24px;
            display: block;
            background: url(https://www.highcharts.com/samples/graphics/search.png) 5px 5px no-repeat white;
            padding: 1px 5px 1px 30px;
            width: 100%;
        }
        .selector .custom-combobox .ui-autocomplete-input:focus {
            color: black;
        }
        .selector .custom-combobox .ui-autocomplete-input.valid {
            color: black;
        }
        .selector .custom-combobox-toggle {
            position: absolute;
            display: block;
            right: -78px;
            border-radius: 0;
        }

        .selector #btn-next-map {
            right: -12px;
        }
        .ui-autocomplete {
            max-height: 500px;
            overflow: auto;
        }
        .ui-autocomplete .option-header {
            font-style: italic;
            font-weight: bold;
            margin: 5px 0;
            font-size: 1.2em;
            color: gray;
        }

        .loading {
            margin-top: 10em;
            text-align: center;
            color: gray;
        }
        .ui-button-icon-only .ui-button-text {
            height: 26px;
            padding: 0 !important;
            background: white;
        }
        #infoBox .button {
            border: none;
            border-radius: 3px;
            background: #a4edba;
            padding: 5px;
            color: black;
            text-decoration: none;
            font-size: 12px;
            white-space: nowrap;
            cursor: pointer;
            margin: 0 3px;
            line-height: 30px;
        }

        @media (max-width: 768px) {
            #demo-wrapper {
                width: auto;
                height: auto;
            }
            #mapBox {
                width: auto;
                float: none;
            }
            #container {
                height: 310px;
            }
            #sideBox {
                float: none;
                width: auto;
                margin-top: 0;
                border-left: none;
                border-top: 1px solid silver;
            }
        }

    </style>
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/chartist/css/chartist.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/chartist/tooltip/chartist-plugin-tooltip.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/dashboard1.css')}}" />

    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!--section ends-->
    <section class="content sec-mar">
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-row">
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 tile-bottom" hidden>
                        <div class="canvas-interactive-wrapper1">
                            <canvas id="canvas-interactive1"></canvas>
                            <div class="cta-wrapper1">
                                <div class="widget" data-count=".num" data-from="0"
                                     data-to="99.9" data-suffix="%" data-duration="2">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-shopping-cart fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box" class="count-box">119</div>
                                            <span class="title">Today Sales</span>
                                        </div>
                                        <div class="text-center">
                                            <span><i class="fa fa-level-up" aria-hidden="true"></i></span>
                                            <strong>12 more Sales</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 tile-bottom">
                        <div class="widget" data-count=".num" data-from="0"
                             data-to="{{ $visitors }}" data-duration="3">
                            <div class="canvas-interactive-wrapper2">
                                <canvas id="canvas-interactive2"></canvas>
                                <div class="cta-wrapper2">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-paw fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box2" class="count-box">{{ $visitors }}</div>
                                            <span class="title">Today Visits</span>
                                        </div>
                                        <div class="text-center">
                                            <span><i class="fa fa-level-up" aria-hidden="true"></i></span>
                                            <strong>{{ $pageView ? round(($bounces/$pageView) * 100, 2) : 0 }} Bounce Rate</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 tile-bottom">
                        <div class="widget" data-suffix="k" data-count=".num"
                             data-from="0" data-to="{{ abs($todayOrder - $yesterdayOrder) }}" data-duration="4" data-easing="false">
                            <div class="canvas-interactive-wrapper3">
                                <canvas id="canvas-interactive3"></canvas>
                                <div class="cta-wrapper3">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-shopping-cart fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box3" class="count-box">{{$todayOrder}}</div>
                                            <span class="title">Today Order</span>
                                        </div>
                                        <div class="text-center">
                                            @if($todayOrder - $yesterdayOrder >= 0)
                                              <span><i class="fa fa-level-up" aria-hidden="true"></i></span>
                                              <strong>{{$todayOrder - $yesterdayOrder}} more order</strong>
                                            @else
                                                <span><i class="fa fa-level-down" aria-hidden="true"></i></span>
                                                <strong>{{$yesterdayOrder - $todayOrder}} less order</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 tile-bottom">
                        <div class="widget">
                            <div class="canvas-interactive-wrapper4">
                                <canvas id="canvas-interactive4"></canvas>
                                <div class="cta-wrapper4">
                                    <div class="item">
                                        <div class="widget-icon pull-left icon-color animation-fadeIn">
                                            <i class="fa fa-fw fa-usd fa-size"></i>
                                        </div>
                                    </div>
                                    <div class="widget-count panel-white">
                                        <div class="item-label text-center">
                                            <div id="count-box4" class="count-box">{{$todaySale}}</div>
                                            <span class="title">Today Sales</span>
                                        </div>
                                        <div class="text-center">
                                            @if($todaySale - $yesterdaySale >= 0)
                                                <span><i class="fa fa-level-up" aria-hidden="true"></i></span>
                                                <strong>{{$todaySale - $yesterdaySale}} more Sale</strong>
                                            @else
                                                <span><i class="fa fa-level-down" aria-hidden="true"></i></span>
                                                <strong>{{$yesterdaySale - $todaySale}} less Sale</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="card border-success">
                    <div class="card-header bg-success text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-save"></i> Sales Chart
                        </h3>
                        <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                    </div>
                    <div class="card-body" style="height: 532.5px">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  in show active" id="home" aria-labelledby="home-tab" role="tabpanel">
                                <div class="sale-chart-filter mr-5">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4 text-right mt-2" for="date">Year</label>
                                        <input class="form-control col-sm-8 text-left" id="sale-chart-date" name="year"  value={{\Carbon\Carbon::now()->format('M-Y')}} onchange="getSaleData()" placeholder="YYY"  type="text"/>
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <div class="ct-chart ct-golden-section" style="height: 450px;" id="chart1"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                                <div class="mt-4">
                                    <div class="ct-chart ct-golden-section" id="chart2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-save"></i> Views Chart
                        </h3>
                        <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  in show active" id="home" aria-labelledby="home-tab" role="tabpanel">
                                <div class="mt-4">
                                    <div id="demo-wrapper">
                                        <div id="mapBox">
                                            <div id="up"></div>
                                            <div class="selector">
                                                <select id="mapDropdown" class="ui-widget combobox"></select>
                                            </div>
                                            <div id="container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- row -->
    
    <!-- right side bar end -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <div id="qn"></div>
    <!-- begining of page level js -->
    <script src="{{asset('admin/assets/js/backstretch.js')}}"></script>
    <!--sales tiles-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/countupcircle/js/jquery.countupcircle.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/granim/js/granim.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/chartist/js/chartist.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/chartist/js/chartis-plugin-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/chartist/tooltip/chartist-plugin-tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/dashboard1.js')}}"></script>
    <script src="https://code.highcharts.com/maps/highmaps.js"></script>
    <script src="https://code.highcharts.com/mapdata/index.js?1"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/jquery.ui.min.js')}}"></script>
    <script src="https://www.highcharts.com/samples/maps/demo/all-maps/jquery.combobox.js"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    <script>

        var lastJQueryTS = 0 ;// this is a global variable.

        $(document).ready(function () {
            getSaleData();
        });

        $(document).ready(function(){
            var date_input=$('input[name="year"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'M-yyyy',
                viewMode: 'months',
                minViewMode: 'months',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });

        function updateCharData (response) {

            var labels = JSON.parse(response.labels);
            var series = JSON.parse(response.series);

            new Chartist.Bar('#chart1', {
                labels:labels,
                series:  [series]
            },{
                axisX: {
                    showGrid: false
                },
                axisY: {
                    showGrid: false
                },
                height: 400,
                fullWidth: true,
                plugins: [
                    Chartist.plugins.tooltip()
                ]
            });
        }

        function getSaleData () {

            var month = $('#sale-chart-date').val();
            var url = "{{route('admin.sale.data', 'month')}}";
            var token = "{{csrf_token()}}";
            url = url.replace('month', month);
            // in the change event handler...
            var send = true;
            if (typeof(event) == 'object'){
                if (event.timeStamp - lastJQueryTS < 300){
                    send = false;
                }
                lastJQueryTS = event.timeStamp;
            }
            if (send){
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "JSON",
                    data: { '_token': token},
                    success: function (response) {
                        updateCharData(response);
                    },
                    error: function () {
                    },
                })
            }

        }

        // Views Chart

        /**
         * This is a complicated demo of Highmaps, not intended to get you up to speed
         * quickly, but to show off some basic maps and features in one single place.
         * For the basic demo, check out https://www.highcharts.com/maps/demo/geojson
         * instead.
         */

        // Base path to maps
        var baseMapPath = "https://code.highcharts.com/mapdata/",
            showDataLabels = false, // Switch for data labels enabled/disabled
            mapCount = 0, v,
            searchText,
            mapOptions = '';

        // Populate dropdown menus and turn into jQuery UI widgets
        $.each(Highcharts.mapDataIndex, function (mapGroup, maps) {
            if (mapGroup !== "version") {
                mapOptions += '<option class="option-header">' + mapGroup + '</option>';
                $.each(maps, function (desc, path) {
                    mapOptions += '<option value="' + path + '">' + desc + '</option>';
                    mapCount += 1;
                });
            }
        });
        searchText = 'Search ' + mapCount + ' maps';
        mapOptions = '<option value="custom/world.js">' + searchText + '</option>' + mapOptions;
        $("#mapDropdown").append(mapOptions).combobox();

        // Change map when item selected in dropdown
        $("#mapDropdown").change(function () {
            var $selectedItem = $("option:selected", this),
                mapDesc = $selectedItem.text(),
                mapKey = this.value.slice(0, -3),
                javascriptPath = baseMapPath + this.value,
                isHeader = $selectedItem.hasClass('option-header');

            // Dim or highlight search box
            if (mapDesc === searchText || isHeader) {
                $('.custom-combobox-input').removeClass('valid');
                location.hash = '';
            } else {
                $('.custom-combobox-input').addClass('valid');
                location.hash = mapKey;
            }

            if (isHeader) {
                return false;
            }

            // Show loading
            if (Highcharts.charts[0]) {
                Highcharts.charts[0].showLoading('<i class="fa fa-spinner fa-spin fa-2x"></i>');
            }

            // When the map is loaded or ready from cache...
            function mapReady() {

                var mapGeoJSON = Highcharts.maps[mapKey],
                    data = JSON.parse('{!! $mapData !!}'),
                    parent,
                    match;

                // Show arrows the first time a real map is shown
                if (mapDesc !== searchText) {
                    $('.selector .prev-next').show();
                    $('#sideBox').show();
                }

                // Is there a layer above this?
                match = mapKey.match(/^(countries\/[a-z]{2}\/[a-z]{2})-[a-z0-9]+-all$/);
                if (/^countries\/[a-z]{2}\/[a-z]{2}-all$/.test(mapKey)) { // country
                    parent = {
                        desc: 'World',
                        key: 'custom/world'
                    };
                } else if (match) { // admin1
                    parent = {
                        desc: $('option[value="' + match[1] + '-all.js"]').text(),
                        key: match[1] + '-all'
                    };
                }
                $('#up').html('');
                if (parent) {
                    $('#up').append(
                        $('<a><i class="fa fa-angle-up"></i> ' + parent.desc + '</a>')
                            .attr({
                                title: parent.key
                            })
                            .click(function () {
                                $('#mapDropdown').val(parent.key + '.js').change();
                            })
                    );
                }


                // Instantiate chart
                $("#container").highcharts('Map', {

                    title: {
                        text: null
                    },

                    mapNavigation: {
                        enabled: true
                    },

                    colorAxis: {
                        min: 0,
                        stops: [
                            [0, '#EFEFFF'],
                            [0.5, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).brighten(-0.5).get()]
                        ]
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        verticalAlign: 'bottom'
                    },

                    series: [{
                        data: data,
                        mapData: mapGeoJSON,
                        joinBy: ['hc-key', 'key'],
                        name: 'Site Visits',
                        states: {
                            hover: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        dataLabels: {
                            enabled: showDataLabels,
                            formatter: function () {
                                return mapKey === 'custom/world' || mapKey === 'countries/us/us-all' ?
                                    (this.point.properties && this.point.properties['hc-a2']) :
                                    this.point.name;
                            }
                        },
                        point: {
                            events: {
                                // On click, look for a detailed map
                                click: function () {
                                    var key = this.key;
                                    $('#mapDropdown option').each(function () {
                                        if (this.value === 'countries/' + key.substr(0, 2) + '/' + key + '-all.js') {
                                            $('#mapDropdown').val(this.value).change();
                                        }
                                    });
                                }
                            }
                        }
                    }, {
                        type: 'mapline',
                        name: "Separators",
                        data: Highcharts.geojson(mapGeoJSON, 'mapline'),
                        nullColor: 'gray',
                        showInLegend: false,
                        enableMouseTracking: false
                    }]
                });

                showDataLabels = $("#chkDataLabels").prop('checked');

            }

            // Check whether the map is already loaded, else load it and
            // then show it async
            if (Highcharts.maps[mapKey]) {
                mapReady();
            } else {
                $.getScript(javascriptPath, mapReady);
            }
        });

        // Toggle data labels - Note: Reloads map with new random data
        $("#chkDataLabels").change(function () {
            showDataLabels = $("#chkDataLabels").prop('checked');
            $("#mapDropdown").change();
        });

        // Switch to previous map on button click
        $("#btn-prev-map").click(function () {
            $("#mapDropdown option:selected").prev("option").prop("selected", true).change();
        });

        // Switch to next map on button click
        $("#btn-next-map").click(function () {
            $("#mapDropdown option:selected").next("option").prop("selected", true).change();
        });

        // Trigger change event to load map on startup
        if (location.hash) {
            $('#mapDropdown').val(location.hash.substr(1) + '.js');
        } else { // for IE9
            $($('#mapDropdown option')[0]).attr('selected', 'selected');
        }
        $('#mapDropdown').change();

    </script>

@stop
