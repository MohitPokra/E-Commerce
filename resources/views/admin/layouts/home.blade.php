@include('admin.layouts.head')
@include('admin.layouts.navbar')
<!-- For horizontal menu -->
@yield('horizontal_header')
<!-- horizontal menu ends -->


<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    @include('admin.layouts.sidebar')
    <aside class="right-side">
        <!-- Content -->
        @yield('content')
    </aside>
    <!-- page wrapper-->
</div>
<!-- wrapper-->
@include('admin.layouts.scripts')
</body>

</html>
