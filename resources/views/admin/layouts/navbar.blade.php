<header class="header">
    <nav class="navbar navbar-expand-md navbar-static-top">
        <a href="index " class="logo navbar-brand">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <img src="{{ settings('admin_logo') }}" alt="logo"/>
        </a>

        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                        class="fa fa-fw fa-bars"></i>
            </a>
        </div>


        <div class="navbar-collapse " id="navbarNav">
            <div class="navbar-right ml-auto">
                <ul class="nav navbar-nav ">
                    <!-- User Account: style can be found in dropdown-->
                    <li class="nav-item dropdown user user-menu">
                        <a href="#" class="nav-link dropdown-toggle padding-user pt-3">
                            <img src="{{ $account->gravatar() }}" width="35"
                                 class="rounded-circle img-fluid pull-left"
                                 height="35" alt="User Image">
                            <div class="riot">
                                <div>
                                    {{ $account->first_name }}
                                    <span>
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ $account->gravatar() }}" class="rounded-circle" alt="User Image">
                                <p>{{ $account->first_name }}</p>
                            </li>
                            <!-- Menu Body -->
                            <li class="p-t-3 nav-item" ><a href="{{ route('account.profile') }}" class="nav-link"> <i class="fa fa-fw fa-user"></i> My
                                    Profile </a>
                            </li>
                            <li class="p-t-3 nav-item" ><a href="{{ url('logout') }}" class="nav-link"> <i class="fa fa-fw fa-user"></i> Log-Out</a>
                            </li>
                            <!-- Menu Footer-->
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>