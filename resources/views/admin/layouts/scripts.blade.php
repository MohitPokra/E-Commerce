
<script src="{{asset('admin/assets/js/app.js')}}" type="text/javascript"></script>
<!-- global js -->
<script>
    $('.hover-delete').on('mouseover', function () {
        $(this.firstChild.nextSibling).show();
    }).on('mouseout', function () {
        $(this.firstChild.nextSibling).hide();
    });

    $('.hove-delete-button').on('click', function () {
        const data = {
            name : $(this).data('name'),
            type : $(this).data('type'),
            id : $(this).data('id')
        };
        $.ajax({
            data: data,
            url: '{{ route('image.delete') }}',
            type: 'post',
            success: function () {
                window.location.reload();
            }
        })

    });
</script>
<!-- end of global js -->
@yield('footer_scripts')
</body>

</html>
