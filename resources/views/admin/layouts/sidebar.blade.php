<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar-->
    <section class="sidebar">
        <div id="menu" role="navigation">
            <p>&nbsp;</p>
            <ul class="navigation">
                <li {!! (Request::is('admin/dashboard')? 'class="active"':"") !!}>
                    <a href="{{ route('admin.dashboard') }} ">
                        <i class="menu-icon fa fa-fw fa-folder-open-o"></i>
                        <span class="mm-text ">Dashboard</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/categories')? 'class="active"':"") !!}>
                    <a href="{{ route('admin.categories.index') }} ">
                        <i class="menu-icon fa fa-fw fa-indent"></i>
                        <span class="mm-text ">Categories</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/brands')? 'class="active"':"") !!}>
                    <a href="{{ route('admin.brands.index') }} ">
                        <i class="menu-icon fa fa-tags" aria-hidden="true"></i>
                        <span class="mm-text ">Brands</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/products')|| Request::is('admin/products/create')? 'class="active"':"") !!}>
                    <a href="{{route('admin.products.index')}} ">
                        <i class="menu-icon fa fa-life-ring"></i>
                        <span>Products</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/customers/*') || Request::is('admin/customers/create') || Request::is('admin/customers/{id}/edit') || Request::is('admin/customers/{id}') || Request::is('deleted_users') ? 'class="active"':"") !!}>
                    <a href="{{ route('admin.customers.index') }}">
                        <i class="menu-icon fa fa-fw fa-users"></i>
                        <span>Users</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/orders')|| Request::is('admin/orders/create')? 'class="active"':"") !!}>
                    <a href="{{ route('admin.orders.index') }}">
                        <i class="menu-icon fa fa-suitcase"></i>
                        <span>Orders</span>
                    </a>
                </li>
                <li {!! (Request::is('email-manager/*')? 'class="active"':"") !!}>
                    <a href="{{ route('mailTracker_Index') }} ">
                        <i class="menu-icon fa fa-truck"></i>
                        <span>Mail Tracking</span>
                    </a>
                </li>
                <li {!! (Request::is('admin/setting/*') ? 'class="active"':"") !!}>
                    <a href="#">
                        <i class="menu-icon fa fa-fw fa-cogs" aria-hidden="true"></i>
                        <span class="mm-text ">Settings</span>
                    </a>
                    <ul class="sub-menu">
                        <li {!! (Request::is('admin/setting/shop')? 'class="active"':"") !!}>
                            <a href="{{route('admin.setting.shop')}} ">
                                <i class="fa fa-fw fa-clock-o"></i> General
                            </a>
                        </li>
                        <li {!! (Request::is('admin/setting/static-pages')? 'class="active"':"") !!}>
                            <a href="{{route('admin.setting.static.index')}} ">
                                <i class="fa fa-fw fa-clock-o"></i> Static Pages
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- / .navigation -->
        </div>
        <!-- menu -->
    </section>
    <!-- /.sidebar -->
</aside>
