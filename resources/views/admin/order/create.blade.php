@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Order Create
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .checkbox-display {
            display: inline !important;
        }
    </style>

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/user_profile.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create Product
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Order</a>
            </li>
            <li class="breadcrumb-item active">
                Create Order
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <div class="col-lg-12">
        <div class="card border-info ">
            <div class="card-header bg-info text-white">
                <h3 class="card-title d-inline">
                    <i class="fa fa-suitcase"></i> Order
                </h3>
                <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
            </div>
            <div class="card-body">
                <form class="col-md-12" id="create-order-form">
                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control form-control-sm" name="user_id">
                            @foreach($users as $key => $user)
                                <option value="{{$key}}" selected>{{$user}}</option>
                            @endforeach
                        </select>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control form-control-sm" name="status">
                            @foreach($statuses as $status)
                                <option value="{{$status}}">{{$status}}</option>
                            @endforeach
                        </select>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Amount</label>
                        <input class="form-control " type="number" name="amount">
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Total Discount</label>
                        <input class="form-control " type="number" name="total_discount">
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Total Tax</label>
                        <input class="form-control " type="number" name="total_tax">
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Bill Amount</label>
                        <input class="form-control " type="number" name="bill_amount">
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Delivery Address</label>
                        <textarea class="form-control " type="text" name="delivery_address" row="2"></textarea>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Order date</label>
                        <input class="form-control date-picker"  name="order_date" placeholder="MM/DD/YYY" type="text"/>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Expected Delivery Date</label>
                        <input class="form-control date-picker" name="expected_delivery_date" placeholder="MM/DD/YYY" type="text"/>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <label>Delivery Date</label>
                        <input class="form-control date-picker" name="delivery_date" placeholder="MM/DD/YYY" type="text"/>
                        <small class="help-block"></small>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success" id="add-order">
                            <span class="fa fa-check"></span> Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@stop

@section('footer_scripts')

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            var date_input=$('.date-picker');
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            var options={
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });

        $('#add-order').on('click', function () {
            var data = $("#create-order-form").serialize();
            console.log(data);
            console.log(data);
            var token = "{{csrf_token()}}";
            $.ajax({
                type: 'POST',
                url: "{{route('admin.orders.store')}}",
                dataType: "JSON",
                data:  data + "&_token=" + token,
                success: function (response) {

                    if(response.status == 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        //calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{route("admin.products.index")}}';
                        }, 1000);

                    }
                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        $.each(response.responseJSON, function (key, value) {
                            var input = '#create-order-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }

                }
            })
        });
    </script>
@stop














