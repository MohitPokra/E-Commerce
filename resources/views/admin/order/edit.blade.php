@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Order Edit/View
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .checkbox-display {
            display: inline !important;
        }
    </style>

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css"
          href="{{asset('admin/assets/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/user_profile.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit/View Order
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Order</a>
            </li>
            <li class="breadcrumb-item active">

                Edit/View Order
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content_body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card border-dark">
                    <div class="card-header bg-dark text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-save"></i> Order Status
                        </h3>
                        <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                    </div>
                    <div class="card-body">
                        <form id="status-update-form">
                            <input type="hidden" name="hash", value="{{ $order->hash }}"/>
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-6 text-right">
                                    <h4>Current Status</h4></label>
                                <div class="col-6 padding-right-30">
                                    <select class="form-control col-6 text-left padding-right-30" name="status">
                                        @foreach($orderStatus as $status)
                                            <option value="{{$status}}" @if($order->status === $status) selected @endif>{{$status}}</option>
                                        @endforeach
                                    </select>
                                    <small class="help-block"></small>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success" id="update-order">
                                    <span class="fa fa-check"></span> Change Status
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="row">
                    {{--Order Details--}}
                    <div class="col-12">
                        <div class="card border-info">
                            <div class="card-header bg-info text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-suitcase"></i> Order Details
                                </h3>
                                <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                            </div>
                            <div class="card-body">
                                <form class="col-md-12" id="update-order-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                                    <div class="form-group">
                                        <label>User</label>
                                        <input class="form-control " type="text" name=""
                                               value="{{$order->user->first_name.' '.$order->user->last_name}}" disabled>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input class="form-control " type="number" name="amount"
                                               value="{{$order->amount}}" disabled>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Total Discount</label>
                                        <input class="form-control " type="number" name="total_discount"
                                               value="{{$order->total_discount}}" @if($order->status !== $orderStatus['ADDRESS'] || $order->status !== $orderStatus['PENDING']) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Total Tax</label>
                                        <input class="form-control " type="number" name="total_tax"
                                               value="{{$order->total_tax}}" @if($order->status !== $orderStatus['ADDRESS'] || $order->status !== $orderStatus['PENDING']) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Bill Amount</label>
                                        <input class="form-control " type="number" name="bill_amount"
                                               value="{{$order->bill_amount}}" disabled>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Order date</label>
                                        <input class="form-control" name="order_date"
                                               value="{{$order->order_date->format('Y-m-d')}}" placeholder="MM/DD/YYY" type="date" disabled/>
                                        <small class="help-block"></small>
                                    </div>
                                    @if(isset($order->delivery_date) && !empty($order->delivery_date))
                                        <div class="form-group">
                                            <label>Delivery Date</label>
                                            <input class="form-control" type="date" name="delivery_date"
                                                   value="{{$order->delivery_date->format('Y-m-d')}}" placeholder="MM/DD/YYY" disabled/>
                                            <small class="help-block"></small>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <label>Expected Delivery Date</label>
                                            <input class="form-control" name="expected_delivery_date"
                                                   value="{{$order->expected_delivery_date->format('Y-m-d')}}" placeholder="MM/DD/YYY"
                                                   type="date"/>
                                            <small class="help-block"></small>
                                        </div>
                                    @endif
                                    @if($order->status !== $orderStatus['DELIVERED'] || $order->status !== $orderStatus['DELIVERED'])
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success" id="update-order">
                                                <span class="fa fa-check"></span> Update
                                            </button>
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>

                    {{--Payment Details--}}
                    @if($order->payment)
                        <div class="col-12">
                            <div class="card border-danger">
                                <div class="card-header bg-danger text-white">
                                    <h3 class="card-title d-inline">
                                        @if(count($order->payment) > 0)
                                        <i class="fa {{ strtolower(trim($order->payment[0]->payment_method)) == 'stripe' ? 'fa-cc-stripe' : 'fa-paypal' }}"></i>
                                        @endif
                                        Payment Details
                                    </h3>
                                    <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                                </div>
                                <div class="card-body">
                                    <h4><a href="#">Payment Info</a></h4>

                                    @forelse($order->payment as $paymentInfo)
                                    <hr>
                                    <div class="table-responsive shopping-cart">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Amount:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>${{ $order->bill_amount }}</h4>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Payment Method:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>{{ strtolower(trim($paymentInfo->payment_method)) == 'stripe' ? 'Stripe' : 'PayPal' }}</h4>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Payment Status:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        <h4 class="badge @if($paymentInfo->status == 'success') badge-success @else badge-danger @endif">{{ $paymentInfo->status }}</h4>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    @empty
                                    <h4>No payment has created for this order</h4>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    @endif

                    {{--Shipping Details--}}
                    @if($order->status != $orderStatus['ADDRESS'] && $order->status != $orderStatus['PENDING'])
                        <div class="col-12">
                            <div class="card border-secondary">
                                <div class="card-header bg-secondary text-white">
                                    <h3 class="card-title d-inline">
                                        <i class="fa {{ strtolower(trim($paymentInfo->payment_method)) == 'stripe' ? 'fa-cc-stripe' : 'fa-paypal' }}"></i>
                                        Shipping Details
                                    </h3>
                                    <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                                </div>
                                <div class="card-body">
                                    <h4><a href="#">Shipping Info</a></h4>
                                    <hr>
                                    @if($order->status != $orderStatus['UNSHIPPED'])
                                    <div class="table-responsive shopping-cart">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Shipping Date:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        @if( $order->shipping_date )
                                                        <h4>{{ $order->shipping_date->toFormattedDateString() }}</h4>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Shipping Method:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>{{ $order->shipping_service }}</h4>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>Tracking Number:</h4>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-item">
                                                        <h4>{{ $order->shipping_tracking_no }}</h4>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                        <form class="col-md-12" id="order-shipping-info-form">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                                            <div class="form-group">
                                                <label>Shipping Date</label>
                                                <input class="form-control " type="date" name="shipping_date">
                                                <small class="help-block"></small>
                                            </div>
                                            <div class="form-group">
                                                <label>Tracking No.</label>
                                                <input class="form-control " type="text" name="shipping_tracking_no">
                                                <small class="help-block"></small>
                                            </div>
                                            <div class="form-group">
                                                <label>Shipping Service</label>
                                                <input class="form-control " type="text" name="shipping_service">
                                                <small class="help-block"></small>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success" id="update-order">
                                                    <span class="fa fa-check"></span> Update
                                                </button>
                                            </div>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card border-success">
                            <div class="card-header bg-success text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-life-ring"></i> Order Products
                                </h3>
                                <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive shopping-cart">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th class="text-center">Subtotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orderProducts as $oProduct)
                                            <tr>
                                                <td>
                                                    <div class="product-item">
                                                        <a class="product-thumb"
                                                           href="{{ route('shopSingle', $oProduct->slug) }}">
                                                            <img width="100px"
                                                                 src="@if(count($oProduct->images) > 0) {{ $oProduct->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif">
                                                        </a>
                                                        <div class="product-info">
                                                            <h4>
                                                                <a class="product-thumb"
                                                                   href="{{ route('shopSingle', $oProduct->slug) }}">{{ $oProduct->name }}</a>
                                                            </h4>
                                                            <small class="text-secondary">
                                                                x {{ $oProduct->quantity }}</small>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="text-center text-lg text-medium">${{ number_format(($oProduct->on_sale ? $oProduct->sale_price : $oProduct->price) * $oProduct->quantity, 2, '.', ',') }}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex flex-wrap justify-content-between align-items-center pb-2">
                                    <div class="px-2 py-1">Subtotal: <span
                                                class='text-medium'>${{ $order->amount }}</span></div>
                                    <div class="px-2 py-1">Discount: <span
                                                class='text-medium'>${{ $order->total_discount }}</span></div>
                                    <div class="px-2 py-1">Tax: <span
                                                class='text-medium'>${{ $order->total_tax }}</span></div>
                                    <div class="text-lg px-2 py-1">Bill Amount: <span
                                                class='text-medium'>${{ $order->bill_amount }}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card border-primary">
                            <div class="card-header bg-primary text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-share"></i> Delivery Address
                                </h3>
                                <span class="pull-right hidden-xs">
                                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                                    <i class="fa fa-fw fa-times removepanel"></i>
                                                </span>
                            </div>
                            <div class="card-body">
                                <form class="col-md-12" id="order-address-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input class="form-control " type="text" name="first_name"
                                               @if($addressAvailable) value="{{ $address->first_name }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input class="form-control " type="text" name="last_name"
                                               @if($addressAvailable) value="{{ $address->last_name }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail Address</label>
                                        <input class="form-control " type="email" name="email"
                                               @if($addressAvailable) value="{{ $address->email }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input class="form-control " type="text" name="phone"
                                               @if($addressAvailable) value="{{ $address->phone }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input class="form-control " type="text" name="name"
                                               @if($addressAvailable) value="{{ $address->name }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select class="form-control" name="country"
                                                @if($disableAddressEdit) disabled @endif>
                                            <option value="">Choose country</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}"
                                                        @if($addressAvailable && $address->country == $country->id) selected="selected"@endif>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>City</label>
                                        <input class="form-control " type="text" name="city"
                                               @if($addressAvailable) value="{{ $address->city }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input class="form-control " type="text" name="zip"
                                               @if($addressAvailable) value="{{ $address->zip }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Address1</label>
                                        <input class="form-control " type="text" name="line1"
                                               @if($addressAvailable) value="{{ $address->line1 }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Address2</label>
                                        <input class="form-control " type="text" name="line2"
                                               @if($addressAvailable) value="{{ $address->line2 }}"
                                               @endif @if($disableAddressEdit) disabled @endif>
                                        <small class="help-block"></small>
                                    </div>
                                    @if(!$disableAddressEdit)
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" id="update-order">
                                            <span class="fa fa-check"></span> Update
                                        </button>
                                    </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@section('footer_scripts')

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var date_input = $('.date-picker');
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };
            date_input.datepicker(options);
        });

        function errorHandle(error, formGetter) {
            if (error.status === 422 || error.responseJSON) {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                $.each(error.responseJSON, function (key, value) {
                    let input = formGetter + ' input[name=' + key + ']';
                    if (typeof $(input).val() === "undefined") {
                        input = formGetter + ' textarea[name=' + key + ']';
                        if (typeof $(input).val() === "undefined") {
                            input = formGetter + ' select[name=' + key + ']';
                        }
                    }
                    $(input).closest('.form-group').addClass('has-error');
                    $(input + '+small').text(value[0]);
                    if ($(input).attr('type') === 'hidden') {
                        toastr["fail"](value[0]);
                    }
                })
            }
            else {
                toastr["fail"]("Unknown error has occurred");
            }
        }

        function redirectWithSuccess(success, destination) {
            if (success.status === 'success') {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                toastr[success.status](success.message);
                //calls click event after a certain time
                setTimeout(function () {
                    window.location.href = destination;
                }, 1000);
            }
        }

        @if(!$disableAddressEdit)
        $('#order-address-form').on('submit', function (evt) {
            evt.preventDefault();
            const data = $(this).serialize();
            const url = '{{ route('admin.orders.update.address') }}';
            $.ajax({
                type: 'PUT',
                url: url,
                data: data,
                success: function (response) {
                    redirectWithSuccess(response, '{{route("admin.orders.index")}}');
                },
                error: function (response) {
                    errorHandle(response, '#order-address-form');
                }
            });
        });
        @endif

        $('#order-shipping-info-form').on('submit', function (evt) {
            evt.preventDefault();
            const data = $(this).serialize();
            const url = '{{ route('admin.orders.update.shipping.info') }}';
            $.ajax({
                type: 'PUT',
                url: url,
                data: data,
                success: function (response) {
                    redirectWithSuccess(response, '{{route("admin.orders.index")}}');
                },
                error: function (response) {
                    errorHandle(response, '#order-shipping-info-form');
                }
            });
        });

        $('#status-update-form').on('submit', function (evt) {
            evt.preventDefault();
            const data = $(this).serialize();
            const url = '{{ route('admin.orders.update.status') }}';
            $.ajax({
                type: 'PUT',
                url: url,
                data: data,
                success: function (response) {
                    redirectWithSuccess(response, '{{route("admin.orders.index")}}');
                },
                error: function (response) {
                    errorHandle(response, '#order-shipping-info-form');
                }
            });
        });

        $('#update-order-form').on('submit', function () {
            var data = $(this).serialize();
            $.ajax({
                type: 'PUT',
                url: "{{route('admin.orders.update')}}",
                dataType: "JSON",
                data: data,
                success: function (response) {
                    redirectWithSuccess(response, '{{route("admin.orders.index")}}');
                },
                error: function (response) {
                    errorHandle(response, '#update-order-form');
                }
            })
        });
    </script>
@stop














