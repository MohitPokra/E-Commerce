@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Orders
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->

    <style>
        .order-column {
            height: 100px;
            overflow-y: auto;
        }
    </style>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/buttons.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/scroller.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/datatables_custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            Orders
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item active">
               Orders
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content no-padding">
        <div class="table-responsive container-no-padding">
            <table class="table table-striped table-bordered table-hover" id="orders">
                <thead>
                <tr id="category-header">
                    <th>#</th>
                    <th> Username </th>
                    <th> Email </th>
                    <th> Status </th>
                    <th> Bill Amount </th>
                    {{--<th> Delivery Address</th>--}}
                    <th> Order Date</th>
                    <th> Expected Delivery Date</th>
                    <th> Delivery Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading5">Delete this Category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <span class="glyphicon glyphicon-info-sign"></span>&nbsp; Are you sure you want to
                            delete this record ?
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteOrder">
                            <span class="fa fa-check"></span> Yes
                        </button>
                        <button type="button" class="btn btn-success" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> No
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    
    <!-- /.modal ends here -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>

    <script>

        $(document).ready(function() {
            dataTables();
        });

       var orderId = null;

        function dataTables() {

            $('#orders').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true ,
                ajax: '{!! route('admin.ajax_orders') !!}',
                "order": [[ 0, "desc" ]],
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_Row_Index', orderable: true, searchable: false },
                    { data: 'username', name: 'username' },
                    { data: 'email', name: 'email' },
                    { data: 'status', name: 'status' },
                    { data: 'bill_amount', name: 'bill_amount'},
                    // { data: 'delivery_address', name: 'delivery_address' },
                    { data: 'order_date', name: 'order_date' },
                    { data: 'expected_delivery_date', name: 'expected_delivery_date' },
                    { data: 'delivery_date', name: 'delivery_date' },
                    { data: 'actions', name: 'actions', width: '20%' }
                ]
            });
        }

        function destroyOrder(id) {
            orderId = id;
        }

        $('#deleteOrder').on('click', function () {
            var url = "{{route('admin.orders.destroy', 'id')}}";
            var token = "{{csrf_token()}}";
            url = url.replace('id', orderId);
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: "JSON",
                data: { '_token': token},
                success: function (response) {
                    if(response.status === 'success') {
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error: function () {

                },
            })
        });

    </script>
@stop
