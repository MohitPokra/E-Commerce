@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Edit Product
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .checkbox-display {
            display: inline !important;
        }
        .modal-header {
            display: block;
        }
        .note-editor{
            margin: 0 !important;
        }
        .hove-delete {
            position: relative;
        }
    </style>
    {{--Todo: have to add theme datepicker cdn added temporary --}}

    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">
    <link href="{{asset('admin/assets/vendors/summernote/download/css/summernote.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/bootstrap-fileinput/css/fileinput.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dropzone.css') }}" />


    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Product
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Products</a>
            </li>
            <li class="breadcrumb-item active">
                Edit Product
            </li>
        </ol>
    </section>

    <section>
        <div class="row">
            <div class="col-lg-12">
                <div class="card border-info ">
                    <div class="card-header bg-info text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-fw fa-info-circle"></i> Images
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-{{ 6 - count($product->images) }}"></div>
                        @foreach($product->images as $image)
                            <div class="col-sm-2 hover-delete">
                                <span class="hove-delete-button" data-name="{{ $image->given_name }}" data-type="product" data-id="{{ $product->id }}"><i class="fa fa-times"></i></span>
                                <img width="100%" style="border-radius: 5px;" src="{{ $image->product_image_url }}">
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('image.upload') }}"
                              class="dropzone"
                              id="my-awesome-dropzone" style=" border: dashed 1px #666; border-radius: 5px;">
                            <input type="hidden" name="type" value="product">
                            <input type="hidden" name="id" value="{{ $product->id }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
      <form id="edit-product-form">
        <div class="row">
            <!-- Main content -->
            <div class="col-lg-6">
                <div class="card border-info ">
                    <div class="card-header bg-info text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-fw fa-info-circle"></i> Basic Details
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" type="text" name="name" value="{{$product->name}}" placeholder="Enter Product Name">
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Short Name</label>
                            <input class="form-control" type="text" name="short_name" value="{{$product->short_name}}" placeholder="Enter Short Name">
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Excerpt</label>
                            <textarea class="form-control" type="text" name="excerpt" row="3" placeholder="Enter Excerpt" style="height: 100px;">{{$product->excerpt}}</textarea>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea id="summernote" name="description">{{$product->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select class="form-control form-control-sm" name="category_id">
                                @foreach($categories as $key => $category)
                                    @if($key === $product->category_id)
                                        <option value={{$key}} selected>{{$category}}</option>
                                    @else
                                        <option value={{$key}}>{{$category}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <label>Brand</label>
                            <select class="form-control form-control-sm" name="brand_id">
                                @foreach($brands as $key => $brand)
                                    @if($key === $product->brand_id)
                                        <option value={{$key}} selected>{{$brand}}</option>
                                    @else
                                        <option value={{$key}}>{{$brand}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group">
                            <div class="form-inline">
                                <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                    <input  type="hidden" name="is_gtin" value="0">
                                    <input class="form-check-input edit-checkbox" id="checkbox-4" type="checkbox" name="is_gtin" value="1" checked>
                                    <label class="form-check-label" for="checkbox-4">
                                        Is GTIN
                                    </label>
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                    <input  type="hidden" name="is_shop_featured" value="0">
                                    <input class="form-check-input edit-checkbox"  id="checkbox-5" type="checkbox" name="is_shop_featured" value="1" checked>
                                    <label class="form-check-label" for="checkbox-5">
                                        Shop featured
                                    </label>
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                    <input  type="hidden" name="is_category_featured" value="0">
                                    <input class="form-check-input edit-checkbox" id="checkbox-6" type="checkbox" name="is_category_featured" value="1" checked>
                                    <label class="form-check-label" for="checkbox-6">
                                        Category featured
                                    </label>
                                    <small class="help-block"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card border-success">
                            <div class="card-header bg-success text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-fw fa-dollar"></i> Pricing
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input class="form-control" type="text" name="price" value="{{ round($product->price, 2) }}" placeholder="Enter Price">
                                    <small class="help-block"></small>
                                </div>
                                <h4>Sale</h4><hr/>
                                <div class="form-group">
                                    <div class="form-inline">
                                        <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                            <input  type="hidden" name="on_sale" value="0">
                                            <input class="form-check-input edit-checkbox" id="checkbox-1"  type="checkbox" name="on_sale" value="1" checked>
                                            <label class="form-check-label" for="checkbox-1">
                                                On Sale
                                            </label>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sale Price</label>
                                            <input class="form-control" type="text" name="sale_price" value="{{ round($product->sale_price, 2) }}" placeholder="Enter Sale Price">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="date">Sale End Date</label>
                                            <input class="form-control" id="date" name="sale_ends_date" value="{{$product->sale_ends_date}}" placeholder="MM/DD/YYY" type="text"/>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                                <h4>Availability</h4><hr/>
                                <div class="form-group">
                                    <div class="form-inline">
                                        <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                            <input class="form-check-input edit-checkbox" id="checkbox-2" type="checkbox" name="in_stock" value="1" @if($product->in_stock) checked @endif>
                                            <label class="form-check-label" for="checkbox-2">
                                                In stock
                                            </label>
                                            <small class="help-block"></small>
                                        </div>
                                        <div class="form-check abc-checkbox abc-checkbox-success checkbox-display col-md-4">
                                            <input  type="hidden" name="is_available" value="0">
                                            <input class="form-check-input edit-checkbox"  id="checkbox-3" type="checkbox" name="is_available" value="1" @if($product->is_available) checked @endif>
                                            <label class="form-check-label" for="checkbox-3">
                                                Is available
                                            </label>
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="stock-quantity-toggle" @if(!$product->in_stock) style="display: none" @endif>
                                    <label>Stock Quantity</label>
                                    <input class="form-control" type="number" name="stock_quantity" value="{{ $product->stock_quantity }}" placeholder="Enter Stock quantiy">
                                    <small class="help-block"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card border-success">
                            <div class="card-header bg-success text-white">
                                <h3 class="card-title d-inline">
                                    <i class="fa fa-fw fa-dropbox"></i> Meta Data
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Meta title</label>
                                    <input class="form-control" type="text" name="meta_title" value="{{$product->meta_title}}" placeholder="Enter Meta Title">
                                    <small class="help-block"></small>
                                </div>
                                <div class="form-group">
                                    <label>Meta Description</label>
                                    <textarea class="form-control" type="text" name="meta_description" row="3" placeholder="Enter Meta Description">{{$product->meta_description}}</textarea>
                                    <small class="help-block"></small>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>SKU</label>
                                            <input class="form-control" type="text" name="sku" value="{{$product->sku}}" placeholder="Enter Sku">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>MPN</label>
                                            <input class="form-control" type="text" name="mpn" value="{{$product->mpn}}" placeholder="Enter Mpn">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ASIN</label>
                                            <input class="form-control" type="text" name="asin" value="{{$product->asin}}" placeholder="Enter Asin">
                                            <small class="help-block"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 offset-md-4 text-center">
                <button type="button" class="btn btn-success updateProduct">
                    <span class="fa fa-check"></span> Update
                </button>
            </div>
        </div>
    </form>
    </section>

    
@stop

@section('footer_scripts')

    {{--Todo: have to add theme datepicker cdn added temporary --}}
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/theme.js')}}">  </script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_elements.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{asset('admin/assets/vendors/summernote/download/js/summnernote.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>

    <!-- end of page level js -->
    <script>
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            maxFiles: 5,
            accept: function(file, done) {
                console.log("uploaded");
                done();
            },
            init: function() {
                this.on("maxfilesexceeded", function(file){
                    alert("No more files please!");
                });
            }
        };

        $(document).ready(function() {
            var date_input = $('input[name="sale_ends_date"]'); //our date input has the name "date"
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            var options = {
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
            };

            $('#summernote').summernote();
            var description = "{{$product->description}}";
            $(".summernote").summernote("code", description);
            $('.note-popover').remove();
            date_input.datepicker(options);
        });

        $('#checkbox-2').on('change', function () {
            if($('#stock-quantity-toggle').is(':visible')) {
                $('#stock-quantity-toggle').hide();
            }
            else {
                $('#stock-quantity-toggle').show();
            }
        });

        $('#add-image').on('click', function (e) {

            e.preventDefault();
            var data = new FormData($("#add-product-image-form")[0]);
            var token = "{{ csrf_token() }}";
            data.append('_token', token);

            $.ajax({
                type: 'POST',
                url: "{{route('image.upload')}}",
                contentType: false,
                processData: false,
                data:  data,
                success: function (response) {
                    if(response.status === 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        // Calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{ route("admin.products.index") }}';
                        }, 1000);
                    }
                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');

                        $.each(response.responseJSON, function (key, value) {
                            var input = '#add-product-image-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }
                }
            })
        });


        $('.updateProduct').on('click', function () {

            var token = "{{ csrf_token() }}";
            var data = $('#edit-product-form').serialize();

            $.ajax({
                type: 'PUT',
                url: "{{route('admin.products.update', [$product->slug])}}",
                dataType: "JSON",
                data:  data + "&_token=" + token,
                success: function (response) {
                    if(response.status === 'success') {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');
                        toastr[response.status](response.message);
                        // Calls click event after a certain time
                        setTimeout(function() {
                            window.location.href = '{{ route("admin.products.index") }}';
                        }, 1000);
                    }
                },
                error: function (response) {
                    if(response.status === 422 || response.responseJSON) {
                        $(".has-error").removeClass("has-error");
                        $(".help-block").text('');

                        $.each(response.responseJSON, function (key, value) {
                            var input = '#edit-product-form input[name=' + key + ']';
                            $(input + '+small').text(value[0]);
                            $(input).parent().addClass('has-error');
                        })
                    }
                    else {
                        toastr["fail"]("Unknown error has occurred");
                    }
                }
            })
        });
    </script>
@stop

