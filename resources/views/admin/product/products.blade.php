@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Products
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .category-column {
            height: 100px;
            overflow-y: auto;
        }
    </style>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/buttons.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/scroller.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/custom_css/datatables_custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/vendors/toastr/css/toastr.min.css') }}"/>
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
            Products
            <button type="button" class="btn btn-primary btn-sm float-right" id="addButton" onclick="window.location='{{ route("admin.products.create") }}'"><i class="fa fa-plus" aria-hidden="true"></i> Add Product</button>
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Products</a>
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content no-padding">
        <div class="table-responsive container-no-padding">
            <table class="table table-striped table-bordered table-hover" id="category">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading5">Delete Product</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <span class="glyphicon glyphicon-info-sign"></span>&nbsp; Are you sure you want to
                            delete this product?
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteProduct">
                            <span class="fa fa-check"></span> Yes
                        </button>
                        <button type="button" class="btn btn-success" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> No
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    
    <!-- /.modal ends here -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            dataTables();
        });

        var productId = null;

        function destroyProduct(el, id) {
            productId = id;
        }

        function dataTables() {
            $('#category').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true ,
                ajax: '{!! route('admin.ajax_products') !!}',
                "order": [[ 0, "desc" ]],
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_Row_Index', orderable: true, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'price', name: 'price' },
                    { data: 'status', name: 'status' },
                    { data: 'actions', name: 'actions'}
                ]
            });
        }

        $('#deleteProduct').on('click', function () {
            var url = "{{ route('admin.products.destroy', 'id') }}";
            var token = "{{ csrf_token() }}";
            url = url.replace('id', productId);

            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: "JSON",
                data: { '_token': token},
                success: function (response) {
                    if(response.status === 'success') {
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error: function () {

                },
            })
        });

    </script>
@stop
