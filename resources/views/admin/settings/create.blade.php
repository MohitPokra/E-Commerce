@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
 Create Static Page
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .about-us-updateButton {
            text-align: center;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/iCheck/css/all.css')}}" />
    <link href="{{asset('admin/assets/vendors/summernote/download/css/summernote.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/bootstrap-fileinput/css/fileinput.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/gridforms/css/gridforms.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/form_layouts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
           Create Page
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i>Dashboard</a>
            </li>
            <li class="breadcrumb-item active">
                Settings
            </li>
            <li class="breadcrumb-item active">
                Static Pages
            </li>
            <li class="breadcrumb-item active">
                Create page
            </li>
        </ol>
    </section>
    <!--section ends-->

    <section class="content no-padding">
        <div class="card border-primary">
            <div class="card-header text-white bg-primary">
                <h3 class="card-title d-inline">
                    <i class="fa fa-cog" aria-hidden="true"></i> Create Page
                </h3>
                <span class="pull-right hidden-xs">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel"></i>
                                        </span>
            </div>
            <div class="card-body">
                <form id="page-create-form">
                    <div class="page">
                                <div class="repeater">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="text" placeholder="Enter title">
                                        <small class="help-block"></small>
                                    </div>
                                    <div class="form-group">
                                        <label>Details</label>
                                        <textarea class="summernote" id="abc" name="description"></textarea>
                                        <small class="text-danger" id="summernote-error"></small>
                                    </div>
                                </div>
                    </div>
                    <div class="form-group about-us-updateButton">
                        <button type="submit" class="btn btn-primary m-t-10" id="about-us-update-btn"><span class="fa fa-check"></span> Create
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <!-- begining of page level js -->
    <script src="{{asset('admin/assets/vendors/summernote/download/js/summnernote.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>

    <!-- end of page level js -->
    <script>

        $(document).ready(function () {

            $('.summernote').summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
            });
            $('.note-popover').remove();
        });

        function redirectWithSuccess(success, destination) {
            if (success.status === 'success') {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                $('.note-frame').css({"border-color": "#a9a9a9",
                    "border-width":"1px",
                    "border-style":"solid"});
                $('#summernote-error').text('');
                toastr[success.status](success.message);
                //calls click event after a certain time
                setTimeout(function () {
                    window.location.href = destination;
                }, 1000);
            }
        }

        function errorHandle(error, formGetter) {
            if (error.status === 422 || error.responseJSON) {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                $.each(error.responseJSON, function (key, value) {
                    let input = formGetter + ' input[name=' + key + ']';
                    if (typeof $(input).val() === "undefined") {
                        input = formGetter + 'textarea[name=' + key + ']';
                        $('.note-frame').css({"border-color": "#fb8678",
                            "border-width":"1px",
                            "border-style":"solid"});
                        $('#summernote-error').text('This Description is required');
                    }
                    $(input + '+small').text(value[0]);
                    $(input).closest('.form-group').addClass('has-error');
                    if ($(input).attr('type') === 'hidden') {
                        toastr["fail"](value[0]);
                    }
                })
            }
            else {
                toastr["fail"]("Unknown error has occurred");
            }
        }

        $("#page-create-form").on('submit', function (e) {

            e.preventDefault();
            var data = new FormData($("#page-create-form")[0]);
            var token = "{{ csrf_token() }}";
            var url = "{{ route('admin.setting.static.store') }}";
            data.append('_token', token);

            $.ajax({
                type: 'post',
                url:  url,
                contentType: false,
                processData: false,
                data: data,
                success: function (response) {
                    redirectWithSuccess(response, '{{route('admin.setting.static.index')}}');
                },
                error: function (response) {
                    errorHandle(response, '#page-create-form');
                }
            });

        });

    </script>
@stop
