@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Static Pages
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/buttons.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin/assets/vendors/datatables/css/scroller.bootstrap4.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/custom_css/datatables_custom.css')}}">
    <!--end of page level css-->

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/awesomebootstrapcheckbox/css/build.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>


@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>
           Static Pages
            <button type="button"  class="btn btn-primary btn-sm float-right" onclick="window.location='{{ route("admin.setting.static.create") }}'"><i class="fa fa-plus" aria-hidden="true" ></i> Add Page</button>
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i> Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Static Pages</a>
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content no-padding">
        <div class="table-responsive container-no-padding">
            <table class="table table-striped table-bordered table-hover" id="static-pages">
                <thead>
                <tr id="category-header">
                    <th> # </th>
                    <th> Name </th>
                    <th> Url </th>
                    <th> Actions </th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading5">Delete this Page</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <span class="glyphicon glyphicon-info-sign"></span>&nbsp; Are you sure you want to
                            delete this Page ?
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deletePage">
                            <span class="fa fa-check"></span> Yes
                        </button>
                        <button type="button" class="btn btn-success" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> No
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- /.modal ends here -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>


    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>


    <script>

        var pageSlug = null;

        $(document).ready(function(){
            dataTables();
        });

        function destroyPage(el, slug) {
            pageSlug = slug;
        }

        function redirectWithSuccess(success, destination) {
            if (success.status === 'success') {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                toastr[success.status](success.message);
                //calls click event after a certain time
                setTimeout(function () {
                    window.location.href = destination;
                }, 1000);
            }
        }

        function errorHandle(error, formGetter) {
            if (error.status === 422 || error.responseJSON) {
                $(".has-error").removeClass("has-error");
                $(".help-block").text('');
                $.each(error.responseJSON, function (key, value) {
                    let input = formGetter + ' input[name=' + key + ']';
                    if (typeof $(input).val() === "undefined") {
                        input = formGetter + 'textarea[name=' + key + ']';
                    }
                    $(input + '+small').text(value[0]);
                    $(input).closest('.form-group').addClass('has-error');
                    if ($(input).attr('type') === 'hidden') {
                        toastr["fail"](value[0]);
                    }
                })
            }
            else {
                toastr["fail"]("Unknown error has occurred");
            }
        }

        function dataTables() {

            $('#static-pages').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true ,
                ajax: '{{ route('admin.ajax_static_page') }}',
                "order": [[ 0, "desc" ]],
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_Row_Index', orderable: true, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'url', name: 'url' },
                    { data: 'actions', name: 'actions', width: '20%' }
                ]
            });
        }


        $('#deletePage').on('click', function () {
            var url = "{{route('admin.setting.static.destroy', 'slug')}}";
            var token = "{{csrf_token()}}";
            url = url.replace('slug', pageSlug);
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: "JSON",
                data: { '_token': token},
                success: function (response) {
                    if(response.status === 'success') {
                        toastr[response.status](response.message);
                        dataTables();
                    }
                },
                error: function () {
                    toastr["fail"]("Unknown error has occurred");
                },
            })
        });


    </script>
@stop
