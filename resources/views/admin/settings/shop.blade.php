@extends('admin.layouts.home')

{{-- Page title --}}
@section('title')
    Shop Settings
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <style>
        .about-us-updateButton {
            text-align: center;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/iCheck/css/all.css')}}" />
    <link href="{{asset('admin/assets/vendors/summernote/download/css/summernote.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/bootstrap-fileinput/css/fileinput.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/gridforms/css/gridforms.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/form_layouts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendors/toastr/css/toastr.min.css')}}"/>
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Shop Settings
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item pt-1"><a href="index"><i class="fa fa-fw fa-home"></i>Dashboard</a>
            </li>
            <li class="breadcrumb-item active">
                Shop Settings
            </li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content no-padding">
        <form id="form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6 col-12">

                    <div class="card border-primary">
                        <div class="card-header text-white bg-primary">
                            <h3 class="card-title d-inline">
                                <i class="fa fa-cog" aria-hidden="true"></i>  General Settings
                            </h3>
                            <span class="pull-right hidden-xs">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel"></i>
                                        </span>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="hero-main-header">Site Name</label>
                                <input type="text" class="form-control" id="site-name" value="{{setting('site_name')}}" name="site_name"
                                       placeholder="Enter Site Name">
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Admin logo
                                </label>
                                <div>
                                    @if(setting('admin_logo'))
                                        <img height="81px" width="220px" class="img-responsive mb-2 admin-logo" src="{{ setting('admin_logo') }}"/>
                                    @endif
                                </div>
                                <div class="mb-2">
                                    @if(setting('admin_logo'))
                                        <button type="button" class="btn btn-danger m-t-10 update-button admin-logo" onclick="removeImage('.admin-logo', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                        </button>
                                    @endif
                                </div>
                                <input type="hidden"  name="admin_logo" class="admin-logo-url" value="{{setting('admin_logo')}}">
                                <input type="file"  data-attr=".admin-logo" class="file-loading input-21 image-picker"  name="admin_logo">
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                   Front  Logo
                                </label>
                                <div>
                                    @if(setting('admin_logo'))
                                        <img height="81px" width="220px" class="img-responsive mb-2 front-logo" src="{{ setting('front_logo') }}"/>
                                    @endif
                                </div>
                                <div class="mb-2">
                                    @if(setting('admin_logo'))
                                        <button type="button" class="btn btn-danger m-t-10 update-button front-logo" onclick="removeImage('.front-logo', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                        </button>
                                    @endif
                                </div>
                                <input type="hidden"  name="front_logo" class="front-logo-url" value="{{setting('front_logo')}}">
                                <input type="file"  data-attr=".front-logo" class="file-loading input-21 image-picker"  name="front_logo">
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Favicon
                                </label>
                                <div>
                                    @if(setting('favicon'))
                                        <img height="81px" width="220px" class="img-responsive mb-2 favicon" src="{{ setting('favicon') }}"/>
                                    @endif
                                </div>
                                <div class="mb-2">
                                    @if(setting('favicon'))
                                        <button type="button" class="btn btn-danger m-t-10 update-button favicon" onclick="removeImage('.favicon', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                        </button>
                                    @endif
                                </div>
                                <input type="hidden"  name="favicon" class="favicon-url" value="{{setting('favicon')}}">
                                <input type="file"  data-attr=".favicon" class="file-loading input-21 image-picker"  name="favicon">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary m-t-10 update-button">Update
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card border-primary">
                        <div class="card-header text-white bg-primary">
                            <h3 class="card-title d-inline">
                                <i class="fa fa-arrows" aria-hidden="true"></i>  Hero Section Layout
                            </h3>
                            <span class="pull-right hidden-xs">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel"></i>
                                        </span>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="hero-background-image-url">Background Image Url</label>
                                <input type="text" class="form-control" id="hero-background-image-url" value="{{setting('home_hero_background_image_url')}}" name="home_hero_background_image_url"
                                       placeholder="Enter Background image url">
                            </div>
                            <div class="form-group">
                                <label for="hero-main-header">Main Header</label>
                                <input type="text" class="form-control" id="hero-main-header" value="{{setting('home_hero_main_header')}}" name="home_hero_main_header"
                                       placeholder="Enter Hero Main Header">
                            </div>
                            <div class="form-group">
                                <label for="hero-header">Header</label>
                                <input type="text" class="form-control" id="hero-header" value="{{setting('home_hero_header')}}" name="home_hero_header"
                                       placeholder="Enter header">
                            </div>
                            <div class="form-group">
                                <label for="hero-sub-header">Sub Header</label>
                                <input type="text" class="form-control" id="hero-sub-header" value="{{setting('home_hero_sub_header')}}" name="home_hero_sub_header"
                                       placeholder="Enter Sub-header">
                            </div>
                            <div class="form-group">
                                <label for="hero-action-url">Action Url</label>
                                <input type="text" class="form-control" id="hero-action-url" value="{{setting('home_hero_action_url')}}" name="home_hero_action_url"
                                       placeholder="Enter Action Url">
                            </div>
                            <div class="form-group">
                                <label for="hero-action-text">Action Text</label>
                                <input type="text" class="form-control" id="hero-action-text" value="{{setting('home_hero_action_text')}}" name="home_hero_action_text"
                                       placeholder="Enter Action Text">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary m-t-10 update-button">Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="card border-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title d-inline">
                                <i class="fa fa-fw fa-check-circle-o"></i> Guarantee Section Layout
                            </h3>
                            <span class="pull-right hidden-xs">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel"></i>
                                        </span>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="guarantee-text">Guarantee Text</label>
                                <input type="text" class="form-control" id="guarantee-text" value="{{setting('home_guarantee_text')}}" name="home_guarantee_text"
                                       placeholder="Enter Guarantee text">
                            </div>
                            <div class="form-group">
                                <label for="guarantor-name">Guaranabout-ustor Name</label>
                                <input type="text" class="form-control" id="guarantor-name" value="{{setting('home_guarantee_guarantor_name')}}" name="home_guarantee_guarantor_name"
                                       placeholder="Enter Guarantor Name">
                            </div>
                            <div class="form-group">
                                <label for="guarantor-designation">Guarantor Designation</label>
                                <input type="text" class="form-control" id="guarantor-designation" value="{{setting('home_guarantee_guarantor_designations')}}" name="home_guarantee_guarantor_designations"
                                       placeholder="Enter Guarantor Designation">
                            </div>
                            <div class="form-group">
                                    <label class="control-label">
                                        Signature Image
                                    </label>
                                    <div>
                                        @if(setting('home_guarantee_signature_image_url'))
                                         <img height="81px" width="220px" class="img-responsive mb-2 signature-image" src="{{ setting('home_guarantee_signature_image_url') }}"/>
                                        @endif
                                    </div>
                                <div class="mb-2">
                                    @if(setting('home_guarantee_signature_image_url'))
                                    <button type="button" class="btn btn-danger m-t-10 update-button signature-image" onclick="removeImage('.signature-image', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                    @endif
                                </div>
                                    <input type="hidden"  name="home_guarantee_signature_image_url" class="signature-image-url" value="{{setting('home_guarantee_signature_image_url')}}">
                                    <input type="file"  data-attr=".signature-image" class="file-loading input-21 image-picker"  name="home_guarantee_signature_image_url">
                            </div>
                            <div class="form-group">
                                    <label class="control-label">
                                        Guarantor Image
                                    </label>
                                    <div class="mb-1">
                                        @if(setting('home_guarantee_guarantor_image_url'))
                                            <img width="81px" width="220px" class="img-responsive mb-2 guarantor-image" src="{{ setting('home_guarantee_guarantor_image_url') }}"/>
                                        @endif
                                    </div>
                                <div class="mb-2">
                                    @if(setting('home_guarantee_guarantor_image_url'))
                                    <button type="button" class="btn btn-danger m-t-10 update-button guarantor-image" onclick="removeImage('.guarantor-image', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                    @endif
                                </div>
                                 <input type="hidden" name="home_guarantee_guarantor_image_url"  class="guarantor-image-url" value="{{setting('home_guarantee_guarantor_image_url')}}">
                                 <input type="file"  data-attr=".guarantor-image" class="file-loading input-21 image-picker"  name="home_guarantee_guarantor_image_url">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary m-t-10 update-button" >Update
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card border-success">
                        <div class="card-header bg-success text-white">
                            <h3 class="card-title d-inline">
                                <i class="fa fa-asterisk" aria-hidden="true"></i> Footer Section Layout
                            </h3>
                            <span class="pull-right hidden-xs">
                                            <i class="fa fa-fw fa-chevron-up clickable"></i>
                                            <i class="fa fa-fw fa-times removepanel"></i>
                                        </span>
                        </div>
                        <div class="card-body">
                                <div class="form-group">
                                    <label for="footer-address">Address Name</label>
                                    <input type="text" class="form-control" id="footer-address-name" value="{{setting('footer_address_name')}}" name="footer_address_name"
                                           placeholder="Enter Address">
                                </div>
                                <div class="form-group">
                                    <label for="footer-address-line1">Address line 1</label>
                                    <input type="text" class="form-control" id="footer-address-line1" value="{{setting('footer_address_line1')}}" name="footer_address_line1"
                                           placeholder="Enter Address Line 1">
                                </div>
                                <div class="form-group">
                                    <label for="footer-address-line2">Address line 2</label>
                                    <input type="text" class="form-control" id="footer-address-line2" value="{{setting('footer_address_line2')}}" name="footer_address_line2"
                                           placeholder="Enter Address Line 2">
                                </div>
                                <div class="form-group">
                                    <label for="footer-address-city">Address City</label>
                                    <input type="text" class="form-control" id="footer-address-city" value="{{setting('footer_address_city')}}" name="footer_address_city"
                                           placeholder="Enter Address City">
                                </div>
                                <div class="form-group">
                                    <label for="footer-address-state">Address State</label>
                                    <input type="text" class="form-control" id="footer-address-state" value="{{setting('footer_address_state')}}" name="footer_address_state"
                                           placeholder="Enter Address State">
                                </div>
                                <div class="form-group">
                                    <label for="footer-address-zip">Address Zip</label>
                                    <input type="text" class="form-control" id="footer-address-zip" value="{{setting('footer_address_zip')}}" name="footer_address_zip"
                                           placeholder="Enter Address Zip">
                                </div>
                                <div class="form-group">
                                    <label for="footer-email">Email</label>
                                    <input type="email" class="form-control" id="footer-email" value="{{setting('footer_email')}}" name="footer_email"
                                           placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                    <label for="footer-email-handle">Email Click Handle Url</label>
                                    <input type="text" class="form-control" id="footer-email-handle" value="{{setting('footer_email_click_handle_url')}}" name="footer_email_click_handle_url"
                                           placeholder="Enter Email Click Handle Url">
                                </div>
                                <div class="form-group">
                                    <label for="footer-phone">Phone</label>
                                    <input type="email" class="form-control" id="footer-phone" value="{{setting('footer_phone')}}" name="footer_phone"
                                           placdescriptioneholder="Enter Phone">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Promotional Image
                                    </label>
                                    <div>
                                        @if(setting('footer_promotional_image_url'))
                                         <img height="81px" width="220px" class="img-responsive mb-2 promotional-image"  src="{{ setting('footer_promotional_image_url') }}"/>
                                        @endif
                                    </div>
                                    <div class="mb-2">
                                        @if(setting('footer_promotional_image_url'))
                                            <button type="button" class="btn btn-danger m-t-10 update-button promotional-image" onclick="removeImage('.promotional-image', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                            </button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="footer_promotional_image_url" class="promotional-image-url" value="{{setting('footer_promotional_image_url')}}" >
                                    <input type="file" data-attr=".promotional-image" class="file-loading input-21 image-picker"  name="footer_promotional_image_url">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">
                                        Payment Method Image
                                    </label>
                                    <div class="mn-1">
                                        @if(setting('footer_payment_method_image_url'))
                                          <img height="81px" width="220px" class="img-responsive mb-2 payment-method-image"  src="{{ setting('footer_payment_method_image_url') }}"/>
                                        @endif
                                    </div>
                                    <div class="mb-2">
                                        @if(setting('footer_payment_method_image_url'))
                                            <button type="button" class="btn btn-danger m-t-10 update-button payment-method-image" onclick="removeImage('.payment-method-image', this)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                            </button>
                                        @endif
                                    </div>
                                    <input type="hidden" class="payment-method-image-url" name="footer_payment_method_image_url" value="{{setting('footer_payment_method_image_url')}}">
                                    <input type="file" data-attr=".payment-method-image" class="file-loading input-21 image-picker" name="footer_payment_method_image_url">
                                </div>
                            <hr>
                                <div>
                                    <button type="submit" class="btn btn-primary m-t-10 update-button">Update
                                    </button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- row -->
    
    <!-- right side bar end -->
    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <!-- begining of page level js -->
    <script src="{{asset('admin/assets/vendors/repeater/jquery.repeater.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}" ></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_layouts.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script src="{{asset('admin/assets/vendors/summernote/download/js/summnernote.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/bootstrap-fileinput/js/theme.js')}}">  </script>
    <script type="text/javascript" src="{{asset('admin/assets/js/custom_js/form_elements.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/vendors/toastr/js/toastr.min.js')}}"></script>

    <!-- end of page level js -->
    <script>

        function removeImage (id, el) {
            $(id+'-url').val(null);
            $(el).hide();
            $(id).hide();
        }

        $('.image-picker').on('change', function () {
            var id = $(this).data('attr');
            $(id).hide();
            $('.fileinput-remove').on('click', function() {
                $(id).show();
            });
        });


        $("#form").on('submit', function (e) {

            e.preventDefault();
            var data = new FormData($("#form")[0]);
            var token = "{{ csrf_token() }}";
            data.append('_token', token);

            var url = "{{ route('admin.setting.shop.store') }}";

            $.ajax({
                type: 'post',
                url:  url,
                contentType: false,
                processData: false,
                data: data,
                success: function (response) {
                    $('#name').text('');
                    $('#edit').modal('hide');
                    if(response.status === 'success') {
                        toastr[response.status](response.message);
                    }
                    window.location.reload();
                },
                error:function (response) {
                    var allErrors = JSON.parse(response.responseText).errors;
                    if(allErrors.name) {
                        $('#name').text(allErrors.name[0]);
                    }
                }
            });
        })
    </script>
@stop
