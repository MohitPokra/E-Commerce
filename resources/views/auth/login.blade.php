@extends('master')

@section('page-title', trans('app.login'))

@section('pageContent')

    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    <h1>Login / Register Account</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li><a href="{{ route('account.profile') }}">Account</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Login / Register</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-2">
            {{-- This will simply include partials/messages.blade.php view here --}}
            @include('partials/messages')
            <div class="row">
                <div class="col-md-6">
                    <form id="login-form" action="{{url('login')}}" method="post" autocomplete="off">

                        {{ csrf_field() }}
                        @if (Input::has('to'))
                            <input type="hidden" value="{{ Input::get('to') }}" name="to">
                        @endif

                        <div class="row margin-bottom-1x">
                            <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block facebook-btn"
                                                                       href="{{ url('auth/facebook/login') }}"><i
                                            class="socicon-facebook"></i>Facebook login</a></div>
                            <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block twitter-btn"
                                                                       href="{{ url('auth/twitter/login') }}"><i
                                            class="socicon-twitter"></i>&nbsp;Twitter login</a></div>
                            <div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block google-btn"
                                                                       href="{{ url('auth/google/login') }}"><i
                                            class="socicon-googleplus"></i>&nbsp;Google+ login</a></div>
                        </div>

                        <h4 class="margin-bottom-1x">Or using form below</h4>
                        <div class="form-group input-group">
                            <input name="username" name="username" class="form-control" type="email" placeholder="Email"
                                   required><span class="input-group-addon"><i class="icon-mail"></i></span>
                        </div>
                        <div class="form-group input-group">
                            <input name="password" name="password" class="form-control" type="password"
                                   placeholder="Password" required><span class="input-group-addon"><i
                                        class="icon-lock"></i></span>
                        </div>
                        <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" name="remember" id="remember"
                                       value="1" checked>
                                <label class="custom-control-label" for="remember">Remember me</label>
                            </div>
                            <a class="navi-link" href="{{ route('account.remind') }}">Forgot password?</a>
                        </div>
                        <div class="text-center text-sm-right">
                            <button class="btn btn-primary margin-bottom-none" type="submit">Login</button>
                        </div>
                    </form>
                </div>

                <div class="col-md-6">
                    <div class="padding-top-3x hidden-md-up"></div>
                    <h3 class="margin-bottom-1x">No Account? Register</h3>
                    <p>Registration takes less than a minute but gives you full control over your orders.</p>
                    <form class="row" id="registration-form">
                        <div id="reg-username" class="col-sm-12">
                            <div class="form-group">
                                <label for="reg-fn">User Name</label>
                                <input class="form-control" name="username" type="text" id="reg-fn" required>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div id="reg-email" class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-email">E-mail Address</label>
                                <input class="form-control" type="email" name="email" id="reg-email" required>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div id="reg-phone" class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-phone">Phone Number</label>
                                <input class="form-control" type="text" name="phone" id="reg-phone" required>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div id="reg-password" class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-pass">Password</label>
                                <input class="form-control" type="password" name="password" id="reg-pass" required>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div id="reg-password_confirmation" class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-pass-confirm">Confirm Password</label>
                                <input class="form-control" type="password" name="password_confirmation"
                                       id="reg-pass-confirm" required>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-12 text-center text-sm-right">
                            <button class="btn btn-primary margin-bottom-none" id="register-btn">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

@stop

@section('scripts')
    <script>

        $("#registration-form").on('submit', function (event) {
            var data = $("#registration-form").serialize();
            event.preventDefault();
            $('.form-group').removeClass('has-danger');
            $.ajax({
                type: "POST",
                url: "{{url('api/register')}}",
                dataType: 'json',
                data: data,
                success: function (response) {
                    window.location.replace('/');
                },
                error: function (error) {
                    const statusCode = error.status;
                    if (statusCode === 422) {
                        const errors = error.responseJSON;
                        _.forEach(errors, function (error, key) {
                            $('#reg-' + key + '> .form-group').addClass('has-danger');
                            $('#reg-' + key + '> .form-group > .form-control-feedback').text(error[0]).show();
                        });
                    }
                    else {
                        $('.container.padding-bottom-3x.mb-2').prepend('<div class="alert alert-danger alert-notification"><ul><li>An Unkonwn error has occured Please try again Later.</li></ul></div>');
                    }
                }
            })
        });

        {{--$("#login-form").on('submit',function (event) {--}}
        {{--var data = $("#login-form").serialize();--}}
        {{--event.preventDefault();--}}
        {{--$.ajax({--}}
        {{--type: "POST",--}}
        {{--url: "{{url('login')}}",--}}
        {{--dataType: 'json',--}}
        {{--data: data,--}}
        {{--success: function (response) {--}}
        {{--window.location.replace('/');--}}
        {{--}--}}
        {{--})--}}
        {{--});--}}
    </script>
    {{--{!! HTML::script('assets/js/as/login.js') !!}--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\Auth\LoginRequest', '#login-form') !!}--}}
@stop
