@extends('master')

@section('pageContent')
    <section class="container padding-top-3x padding-bottom-3x">
        <h1 class="text-center">@lang('app.forgot_your_password')</h1>

        @include('partials.messages')
        <div class="col-sm-6 offset-sm-3 text-center">
            <form role="form" action="<?= url('password/remind') ?>" method="POST" id="remind-password-form" autocomplete="off">
                <input type="hidden" value="<?= csrf_token() ?>" name="_token">

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.email')</label>
                    <i class="fa fa-at"></i>
                    <input type="email" name="email" id="email" class="form-control" placeholder="@lang('app.your_email')">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-secondary" id="btn-reset-password">
                        @lang('app.reset_password')
                    </button>
                </div>
            </form>
        </div>
    </section>

@stop

@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Auth\Social\SaveEmailRequest', '#remind-password-form') !!}
@stop

