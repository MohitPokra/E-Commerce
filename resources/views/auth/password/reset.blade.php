@extends('master')

@section('pageContent')
    <section class="container padding-top-3x padding-bottom-3x">
        <h1 class="text-center">@lang('app.reset_your_password')</h1>

        @include('partials.messages')
        <div class="col-sm-6 offset-sm-3 text-center">
            <form role="form" action="{{ url('password/reset') }}" method="POST" id="reset-password-form" autocomplete="off">

                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.your_email')</label>
                    <i class="fa fa-lock"></i>
                    <input type="email" name="email" id="email" class="form-control" placeholder="@lang('app.your_email')">
                </div>

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.new_password')</label>
                    <i class="fa fa-lock"></i>
                    <input type="password" name="password" id="password" class="form-control" placeholder="@lang('app.new_password')">
                </div>

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.confirm_new_password')</label>
                    <i class="fa fa-lock"></i>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="@lang('app.confirm_new_password')">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-custom btn-lg btn-block" id="btn-reset-password">
                        @lang('app.update_password')
                    </button>
                </div>

            </form>
        </div>
    </section>

@stop

@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Auth\Social\SaveEmailRequest', '#reset-password-form') !!}
@stop
