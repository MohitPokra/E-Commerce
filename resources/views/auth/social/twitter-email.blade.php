@extends('master')

@section('pageContent')
    <section class="container padding-top-3x padding-bottom-3x">
        <h2 class="text-center mb-30">@lang('app.hey') {{ $twitterAccount->getName() }},</h2>
        <div class="col-sm-6 offset-sm-3 text-center">
            <div class="alert alert-warning">
                <strong>@lang('app.one_more_thing')...</strong>
                @lang('app.twitter_does_not_provide_email')
            </div>

            @include('partials.messages')

            <form role="form" action="<?= url('auth/twitter/email') ?>" method="POST" id="email-form" autocomplete="off">
                <input type="hidden" value="<?= csrf_token() ?>" name="_token">

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.email')</label>
                    <i class="fa fa-at"></i>
                    <input type="email" name="email" id="email" class="form-control" placeholder="@lang('app.your_email')">
                </div>

                <div class="form-group">
                    <button class="btn btn-secondary" type="submit">@lang('app.log_me_in')</button>
                </div>
            </form>
        </div>
    </section>

@stop

@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Auth\Social\SaveEmailRequest', '#email-form') !!}
@stop
