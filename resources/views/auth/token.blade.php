@extends('master')

@section('pageContent')
    <section class="container padding-top-3x padding-bottom-3x">
        <h1 class="text-center">@lang('app.two_factor_authentication')</h1>

        @include('partials.messages')
        <div class="col-sm-6 offset-sm-3 text-center">
            <form role="form" action="<?= route('auth.token.validate') ?>" method="POST" autocomplete="off">
                <input type="hidden" value="<?= csrf_token() ?>" name="_token">

                <div class="form-group password-field input-icon">
                    <label for="password" class="sr-only">@lang('app.token')</label>
                    <i class="fa fa-lock"></i>
                    <input type="text" name="token" id="token" class="form-control" placeholder="@lang('app.authy_2fa_token')">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-secondary" id="btn-reset-password">
                        @lang('app.validate')
                    </button>
                </div>
            </form>
        </div>
    </section>

@stop

