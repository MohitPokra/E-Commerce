<!-- Poduct Gallery-->
<div class="col-md-6">
    <div class="product-gallery">
        @if($product->on_sale)
            <span class="product-badge text-danger">{{ $product->getPercentOff() }}% Off</span>
        @endif
        <div class="gallery-wrapper">
            @foreach($product->images as $image)
                <div class="gallery-item active">
                    <a href="{{ $image->product_image_url }}" data-hash="{{$image->meta_type}}" data-size="1000x667"></a>
                </div>
            @endforeach
        </div>
        <div class="product-carousel owl-carousel">
            @foreach($product->images as $image)
                <div data-hash="{{$image->meta_type}}"><img src="{{ $image->product_image_url }}" alt="Product"></div>
            @endforeach
        </div>
        <ul class="product-thumbnails">
            @foreach($product->images as $key => $image)
                <li @if($key == 0)class="active"@endif><a href="#{{$image->meta_type}}"><img src="{{ $image->product_image_url }}" alt="Product"></a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

