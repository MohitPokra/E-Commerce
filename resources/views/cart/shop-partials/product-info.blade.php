<!-- Product Info-->
<div class="@if(count($product->images) > 0) col-md-6 @else col-md-12 @endif">
    <div class="padding-top-2x mt-2 hidden-md-up"></div>
    {{--Todo: handle this star section after reviews section completed.--}}
    <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>
    </div><span class="text-muted align-middle">&nbsp;&nbsp;4.2 | 3 customer reviews</span>

    <h2 class="padding-top-1x text-normal">{{ $product->name }}</h2>
    <span class="h2 d-block">
        @if($product->on_sale)<del class="text-muted text-normal">${{ number_format($product->price, 2, '.', ',') }}</del>&nbsp; ${{ number_format($product->sale_price, 2, '.', ',') }}
        @else
            ${{ number_format($product->price, 2, '.', ',') }}
        @endif
    </span>
    <p>{{ $product->excerpt }}</p>
    @if($product->in_stock)
        <div class="row margin-top-1x">
            <div class="col-sm-5">
                <label for="color" class="text-primary">In Stock</label>
                <span class="h2 d-block">{{ $product->stock_quantity }}</span>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <select class="form-control" name="quantity" id="product_add_quantity">
                        @for($i = 1; $i < $product->stock_quantity; $i++)
                            <option value="{{ $i }}" @if(auth()->check() && $productCart && $productCart->quantity === $i) selected @endif>{{ $i }}</option>
                        @endfor
                        @if(auth()->check() && $productCart && $productCart->quantity > $i)
                            <option value="{{ $productCart->quantity }}" selected>{{ $productCart->quantity }}</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
    @endif
    <div class="pt-1 mb-2"><span class="text-medium">SKU:</span> {{ $product->sku }}</div>
    <div class="padding-bottom-1x mb-2">
        @php $category = $product->category()->first(); @endphp
        <span class="text-medium">Category:&nbsp;</span>
        <a class="navi-link" href="{{ route('products.category', $category->slug) }}">{{ $category->name }}</a>
    </div>
    {{--<div class="padding-bottom-1x mb-2">--}}
        {{--<span class="text-medium">Categories:&nbsp;</span>--}}
        {{--<a class="navi-link" href="">Men’s shoes,</a>--}}
        {{--<a class="navi-link" href="#"> Snickers,</a>--}}
        {{--<a class="navi-link" href="#"> Sport shoes</a>--}}
    {{--</div>--}}
    <hr class="mb-3">
    <div class="d-flex flex-wrap justify-content-between">
        <div class="sp-buttons mt-2 mb-2">
            @if($product->in_stock)
                @if(auth()->check() && $productCart)
                    <button class="btn btn-primary" id="update_cart_button" onclick="addToCart(event, { quantityGetter: '#product_add_quantity', product_id: '{{ $product->id }}'})" disabled><i class="icon-bag"></i> Update Cart</button>
                    <button class="btn btn-danger" onclick="removeFromCart(event, '{{ $productCart->id }}')"><i class="icon-circle-cross"></i> Remove from Cart</button>
                @else
                    <button class="btn btn-primary" onclick="addToCart(event, { quantity: -1, product_id: '{{ $product->id }}'})"><i class="icon-bag"></i> Add to Cart</button>
                @endif
@else
    <button class="btn btn-secondary" data-toast data-toast-type="danger" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Sorry" data-toast-message="This product has already sold out.">Sold out &nbsp<i class="icon-open"></i></button>
@endif
</div>
</div>
</div>
