<div id="reviews">
    @foreach($product->review as $review)
        <!-- Review-->
            <div class="comment">
                <div class="comment-author-ava"><img src="{{ $review->user->gravatar() }}" alt="Review author"></div>
                <div class="comment-body">
                    <div class="comment-header d-flex flex-wrap justify-content-between">
                        <h4 class="comment-title">{{ $review->subject }}</h4>
                        <div class="mb-2">
                            @php $star = $review->rating @endphp
                            @include('layouts.stars')
                        </div>
                    </div>
                    <p class="comment-text">{{ $review->review }}</p>
                    <div class="comment-footer"><span class="comment-meta">{{ $review->name }}</span></div>
                </div>
            </div>
    @endforeach

    <!-- Review Form-->
    <h5 class="mb-30 padding-top-1x">Leave Review</h5>
    <form class="row" id="review_form">
        {{ csrf_field() }}
        <input type="hidden" name="product_id" value="{{ $product->id }}"/>
        <div id="review-name" class="col-sm-6">
            <div class="form-group">
                <label for="review_name">Your Name</label>
                <input class="form-control form-control-rounded" type="text" name="name" id="name" required>
                <div class="form-control-feedback"></div>
            </div>
        </div>
        <div id="review-email" class="col-sm-6">
            <div class="form-group">
                <label for="review_email">Your Email</label>
                <input class="form-control form-control-rounded" type="email" name="email" id="email">
                <div class="form-control-feedback"></div>
            </div>
        </div>
        <div id="review-subject" class="col-sm-6">
            <div class="form-group">
                <label for="review_subject">Subject</label>
                <input class="form-control form-control-rounded" type="text" name="subject" id="subject" required>
                <div class="form-control-feedback"></div>
            </div>
        </div>
        <div id="review-rating" class="col-sm-6">
            <div class="form-group">
                <label for="review_rating">Rating</label>
                <select class="form-control form-control-rounded" name="rating" id="rating">
                    <option value="5">5 Stars</option>
                    <option value="4">4 Stars</option>
                    <option value="3">3 Stars</option>
                    <option value="2">2 Stars</option>
                    <option value="1">1 Star</option>
                </select>
                <div class="form-control-feedback"></div>
            </div>
        </div>
        <div id="review-review" class="col-12">
            <div class="form-group">
                <label for="review_text">Review </label>
                <textarea class="form-control form-control-rounded" name="review" id="review" rows="8" required></textarea>
                <div class="form-control-feedback"></div>
            </div>
        </div>
        <div class="col-12 text-right">
            <button class="btn btn-outline-primary" @if(auth()->check()) type="submit" @else type="button" data-toast="" data-toast-position="topRight" data-toast-type="danger" data-toast-icon="icon-ban" data-toast-title="Please" data-toast-message="login first." @endif>Submit Review</button>
        </div>
    </form>
</div>
