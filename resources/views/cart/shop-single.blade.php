@extends('master')
@section('pageContent')
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>{{ $product->name }}</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{ route('home') }}">Home</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li><a href="{{ route('shop.search', '') }}">Shop</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>Single Product</li>
            </ul>
        </div>
    </div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
    <div class="row">
        @if(count($product->images) > 0)
            @include('cart.shop-partials.gallery')
        @endif
        @include('cart.shop-partials.product-info')
    </div>
    @include('cart.shop-partials.info-icons-first')
    <div class="row padding-top-3x mb-3">
        <div class="col-lg-10 offset-lg-1">
            <h2 class="padding-top-1x text-normal">Description</h2>
            @include('cart.shop-partials.description')
        </div>
    </div>
    @include('cart.shop-partials.info-icons-second')
    <div class="row padding-top-3x mb-3">
        <div class="col-lg-10 offset-lg-1">
            <h2 class="padding-top-1x text-normal">Reviews</h2>
            @include('cart.shop-partials.review')
        </div>
    </div>
    @if(count($similarProducts) > 0)
        @include('cart.shop-partials.similar-products')
    @endif
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/lodash.js') }}"></script>
    <script type="text/javascript">
        $('#review_form').on('submit', function (evt) {
            evt.preventDefault();
            const data = $('#review_form').serialize();

            $.ajax({
                type: 'POST',
                url: '{{ route('review.store') }}',
                data: data,
                success: function (resp) {
                    window.location.reload();
                },
                error: function (error) {
                    const statusCode = error.status;
                    if (statusCode === 422) {
                        const errors = error.responseJSON;
                        _.forEach(errors, function (error, key) {
                            $('#review-' + key + '> .form-group').addClass('has-danger');
                            $('#review-' + key + '> .form-group > .form-control-feedback').text(error[0]).show();
                        });
                    }
                    else {
                        $('.container.padding-bottom-3x.mb-2').prepend('<div class="alert alert-danger alert-notification"><ul><li>An Unkonwn error has occured Please try again Later.</li></ul></div>');
                    }
                }
            });
        })

        $('#product_add_quantity').on('change', function () {
            $('#update_cart_button').removeAttr("disabled")
        })
    </script>
@endsection
