@extends('master')
@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>Checkout</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="../templateFile/index.html">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>Checkout</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        @include('partials.messages')
        <div class="row">
            <!-- Checkout Adress-->
            <div class="col-xl-9 col-lg-8">
                <div class="checkout-steps">
                    <a href="{{ route('order.review.get', $order->hash) }}">3. Review</a>
                    <a class="@if($order->status != $orderStatus['ADDRESS'] && $order->status != $orderStatus['PENDING']) completed @endif" href="{{ route('order.payment.get', $order->hash) }}">
                        <span class="angle"></span>2. Payment</a>
                    <a class="active" href="#"><span
                                class="angle"></span>1. Address</a>
                </div>

                <h4>Shipping Address</h4>
                <hr class="padding-bottom-1x">
                <form id="address_form">
                    {{ csrf_field() }}
                    <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                    <div class="row">
                        <div class="col-sm-6" id="contact-first_name">
                            <div class="form-group" id="contact-first_name">
                                <label for="checkout-fn">First Name</label>
                                <input class="form-control" type="text" name="first_name"  value="{{ $disableAddressEdit ? $shippingAddress->first_name : auth()->user()->first_name }}" id="checkout-fn" required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" id="contact-last_name">
                            <div class="form-group">
                                <label for="checkout-ln">Last Name</label>
                                <input class="form-control" type="text" name="last_name"  value="{{ $disableAddressEdit ? $shippingAddress->last_name : auth()->user()->last_name }}" id="checkout-ln" @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" id="contact-email">
                            <div class="form-group">
                                <label for="checkout-email">E-mail Address</label>
                                <input class="form-control" type="email" name="email" value="{{ $disableAddressEdit ? $shippingAddress->email : auth()->user()->email }}" required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" id="contact-phone">
                            <div class="form-group">
                                <label for="checkout-phone">Phone Number</label>
                                <input class="form-control" type="text"  value="{{ $disableAddressEdit ? $shippingAddress->phone : auth()->user()->phone }}" name="phone" required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" id="contact-name">
                            <div class="form-group">
                                <label for="contact-company">Company</label>
                                <input class="form-control" type="text" name="company" @if($shippingAddress != null)value="{{ $shippingAddress->name ? : '' }}"@endif @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6" id="contact-country">
                            <div class="form-group">
                                <label for="contact-country">Country</label>
                                <select class="form-control" name="country" @if($disableAddressEdit) disabled @endif>
                                    <option value="">Choose country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($shippingAddress != null && $shippingAddress->country == $country->id) selected="selected"@endif>{{$country->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="contact-city">
                            <div class="form-group">
                                <label for="contact-city">City</label>
                                <input class="form-control" type="text" name="city" @if($shippingAddress != null)value="{{ $shippingAddress->city ? : '' }}"@endif required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6" id="contact-zip">
                            <div class="form-group">
                                <label for="contact-zip">ZIP Code</label>
                                <input class="form-control" type="text" name="zip" @if($shippingAddress != null)value="{{ $shippingAddress->zip ? : '' }}"@endif required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-1x">
                        <div class="col-md-6" id="contact-line1">
                            <div class="form-group">
                                <label for="contact-address1">Address 1</label>
                                <input class="form-control" type="text" name="line1" @if($shippingAddress != null)value="{{ $shippingAddress->line1 ? : ''}}"@endif required @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6" id="contact-line2">
                            <div class="form-group">
                                <label for="contact-address2">Address 2</label>
                                <input class="form-control" type="text" name="line2" @if($shippingAddress != null) value="{{ $shippingAddress->line2 ? : '' }}" @endif @if($disableAddressEdit) disabled @endif>
                                <div class="form-control-feedback"></div>
                            </div>
                        </div>
                    </div>
                        <div class="checkout-footer">
                            {{--<div class="column"><a class="btn btn-outline-secondary" href="cart.html"><i--}}
                                        {{--class="icon-arrow-left"></i><span--}}
                                        {{--class="hidden-xs-down">&nbsp;Back To Cart</span></a></div>--}}

                            @if($disableAddressEdit)
                                <div class="column"><a class="btn btn-primary" href="{{ route('order.payment.get', $order->hash) }}"><span
                                                class="hidden-xs-down">Continue&nbsp;</span><i class="icon-arrow-right"></i></a>
                                </div>
                            @else
                                <div class="column"><button class="btn btn-primary" type="submit"><span
                                                class="hidden-xs-down">Continue&nbsp;</span><i class="icon-arrow-right"></i></button>
                                </div>
                            @endif
                        </div>
                </form>
            </div>

            <!-- Sidebar          -->
            @include('checkout.partials.right-sidebar')
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#address_form').on('submit', function (evt) {
            evt.preventDefault();
            const url = '{{ route('order.address.post') }}',
                data = $(this).serialize();

            $('.form-group').removeClass('has-danger');
            $('.form-control-feedback').text('').hide();
            $.ajax({
                type: 'POST',
                data: data,
                url: url,
                success: function (resp) {
                    let redirectUrl = '{{ route('order.payment.get', ':hash') }}';
                    window.location.href = redirectUrl.replace(':hash', resp.hash);
                },
                error: function (error) {
                    const statusCode = error.status;
                    if (statusCode === 422) {
                        const errors = error.responseJSON;
                        _.forEach(errors, function (error, key) {
                            $('#contact-' + key + '> .form-group').addClass('has-danger');
                            $('#contact-' + key + '> .form-group > .form-control-feedback').text(error[0]).show();
                        });
                    }
                    else {
                        $('.container.padding-bottom-3x.mb-2').prepend('<div class="alert alert-danger alert-notification"><ul><li>An Unkonwn error has occured Please try again Later.</li></ul></div>');
                    }
                    showToaster({
                        type: 'danger',
                        title: 'Error',
                        'message': 'Some error has occur.'
                    })
                }
            });
        });
    </script>
@endsection
