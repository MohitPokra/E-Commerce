@extends('master')
@section('pageContent')
  <!-- Page Title-->
  <div class="page-title">
    <div class="container">
      <div class="column">
        <h1>Checkout</h1>
      </div>
      <div class="column">
        <ul class="breadcrumbs">
          <li><a href="../templateFile/index.html">Home</a>
          </li>
          <li class="separator">&nbsp;</li>
          <li>Checkout</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Page Content-->
  <div class="container padding-bottom-3x mb-2">
    @include('partials.messages')
    <div class="card text-center">
      <div class="card-body padding-top-2x">
        <h3 class="card-title">Thank you for your order!</h3>
        <p class="card-text">Your order has been placed and will be processed as soon as possible.</p>
        <p class="card-text">Make sure you make note of your order number, which is <span class="text-medium">{{ $order->hash }}</span></p>
        <p class="card-text">You will be receiving an email shortly with confirmation of your order.
          <u>You can now:</u>
        </p>
        <div class="padding-top-1x padding-bottom-1x">
          <a class="btn btn-outline-secondary" href="{{ route('products.all') }}">Go Back Shopping</a>
          <a class="btn btn-outline-primary" href="{{ route('order.tracking.get', $order->hash) }}"><i class="icon-location"></i>&nbsp;Track order</a></div>
      </div>
    </div>
  </div>
@endsection

