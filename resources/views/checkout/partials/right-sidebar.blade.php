<div class="col-xl-3 col-lg-4">
    <aside class="sidebar">
        <div class="padding-top-2x hidden-lg-up"></div>
        <!-- Order Summary Widget-->
        <section class="widget widget-order-summary">
            <h3 class="widget-title">Order Summary</h3>
            <table class="table">
                <tr>
                    <td>Cart Subtotal:</td>
                    <td class="text-medium">${{ number_format($order->amount, 2, '.', ',') }}</td>
                </tr>
                <tr>
                    <td>Estimated tax:</td>
                    <td class="text-medium">${{ number_format($order->total_tax, 2, '.', ',') }}</td>
                </tr>
                <tr>
                    <td>Discount:</td>
                    <td class="text-medium">${{ number_format($order->total_discount, 2, '.', ',') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="text-lg text-medium">
                        ${{ number_format($order->bill_amount, 2, '.', ',') }}</td>
                </tr>
            </table>
        </section>
        <!-- Featured Products Widget-->
        <section class="widget widget-featured-products">
            <h3 class="widget-title">Recently Viewed</h3>
            <!-- Entry-->
            @foreach($featuredProduct as $product)
                <div class="entry">
                    <div class="entry-thumb"><a href="{{ route('shopSingle', $product->slug) }}">
                            <img src="@if(count($product->images) > 0) {{ $product->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif"
                                 alt="Product"></a></div>
                    <div class="entry-content">
                        <h4 class="entry-title"><a href="{{ route('shopSingle', $product->slug) }}">{{ $product->name }}</a></h4><span class="entry-meta">
                                        @if ($product->on_sale)
                                <del>${{ number_format($product->price, 2, '.', ',') }}</del>${{ number_format($product->sale_price, 2, '.', ',') }}
                            @else
                                ${{ number_format($product->price, 2, '.', ',') }}
                            @endif</span>
                    </div>
                </div>
            @endforeach
        </section>
        <!-- Promo Banner-->
        <section class="promo-box"
                 style="background-image: url({{ setting('footer_promotional_image_url') }});">
            <span class="overlay-dark" style="opacity: .4;"></span>
            <div class="promo-box-content text-center padding-top-2x padding-bottom-2x">
                <h4 class="text-light text-thin text-shadow">New Collection of</h4>
                <h3 class="text-bold text-light text-shadow">Sunglasses</h3><a
                        class="btn btn-outline-white btn-sm" href="#">Shop Now</a>
            </div>
        </section>
    </aside>
</div>
