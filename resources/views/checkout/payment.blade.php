@extends('master')
@section('style')
    <link rel="stylesheet" media="screen" href="{{asset('css/card.min.css')}}">
@endsection

@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>Checkout</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>Checkout</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        @include('partials.messages')
        <div class="row mt-2">
            <!-- Checkout Adress-->
            <div class="col-xl-9 col-lg-8">
                <div class="checkout-steps">
                    <a href="{{ route('order.review.get', $order->hash) }}">3. Review</a>
                    <a class="active" href="#">
                        <span class="angle"></span>2. Payment</a>
                    <a class="@if($order->status != $orderStatus['ADDRESS']) completed @endif" href="{{ route('order.address.get', $order->hash) }}">
                        <span class="step-indicator icon-circle-check"></span>
                        <span class="angle"></span>1. Address</a>
                </div>
                <h4>Choose Payment Method</h4>
                <hr class="padding-bottom-1x">
                <div class="accordion" id="accordion" role="tablist">
                    @if(! $disablePayment)
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6>
                                <a href="#card" data-toggle="collapse"><i class="icon-columns"></i>Pay with Credit Card</a>
                            </h6>
                        </div>
                        <div class="collapse show" id="card" data-parent="#accordion" role="tabpanel">
                            <div class="card-body">
                                <p>We accept following credit cards:&nbsp;<img class="d-inline-block align-middle"
                                                                               src="../../../../../../Desktop/template-1/dist/img/credit-cards.png"
                                                                               style="width: 120px;" alt="Cerdit Cards">
                                </p>
                                <div class="card-wrapper"></div>
                                <form action="{{ route('payments.stripe') }}" method="POST" id="stripe_payment_form">
                                    <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                                    {{ csrf_field() }}
                                    <script
                                            src="https://checkout.stripe.com/checkout.js"
                                            class="stripe-button d-flex flex-wrap justify-content-between align-items-center"
                                            data-email="{{ auth()->user()->email }}"
                                            data-key="{{ env('STRIPE_KEY') }}"
                                            data-amount="{{ $order->bill_amount * 100 }}"
                                            data-button-name="Create Payment"
                                            data-description="Payment through debit card."
                                            data-image="{{ setting('favicon') }}"
                                            data-locale="auto"
                                            data-currency="{{ env('CURRENCY_NAME') }}">
                                    </script>
                                    <script>
                                        // Hide default stripe button, be careful there if you
                                        // have more than 1 button of that class
                                        document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                                    </script>
                                    <button type="submit" class="btn btn-outline-primary margin-top-none">Create Payment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab">
                            <h6><a class="collapsed" href="#paypal" data-toggle="collapse"><i
                                            class="socicon-paypal"></i>Pay with PayPal</a></h6>
                        </div>
                        <div class="collapse" id="paypal" data-parent="#accordion" role="tabpanel">
                            <div class="card-body">
                                <p>PayPal - the safer, easier way to pay</p>
                                <form class="row" action="{{ route('payments.paypal') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="hash" value="{{ $order->hash }}"/>
                                    <div class="col-12">
                                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                                            <button class="btn btn-outline-primary margin-top-none" type="submit">Create Payment</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="card" role="tab">
                            <div class="card-header" role="tab">
                                @if(count($order->payment) > 0)
                                <h6><a class="collapsed" href="#paypal" data-toggle="collapse"><i
                                                class="{{ strtolower(trim($order->payment[0]->payment_method)) == 'stripe' ? 'icon-columns' : 'socicon-paypal' }}"></i>Payment done with {{ strtolower(trim($order->payment[0]->payment_method)) == 'stripe' ? 'Stripe' : 'PayPal' }}</a></h6>
                                @else
                                    <h6><a class="collapsed" href="#paypal" data-toggle="collapse">You have not made any payment for this order.</a></h6>
                                @endif
                            </div>
                            <div class="collapse show" id="paypal" data-parent="#accordion" role="tabpanel">
                                <div class="card-body">
                                    @foreach($order->payment as $payment)
                                    <p>Payment Info</p>
                                    <ul class="list-unstyled">
                                        <li><span class="text-muted">Amount:</span>{{ $order->bill_amount }}</li>
                                        <li><span class="text-muted">Payment Method:</span>{{ strtolower(trim($payment->payment_method)) == 'stripe' ? 'Stripe' : 'PayPal' }}</li>
                                        <li><span class="text-muted">Payment Status:</span>{{ $payment->status }}</li>
                                    </ul>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="checkout-footer margin-top-1x">
                    <div class="column"><a class="btn btn-outline-secondary" href="{{ route('order.address.get', $order->hash) }}"><i
                                    class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Back</span></a></div>
                    <div class="column"><a class="btn btn-primary" href="{{ route('order.review.get', $order->hash) }}"><span
                                    class="hidden-xs-down">Continue&nbsp;</span><i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Sidebar          -->
            @include('checkout.partials.right-sidebar')
        </div>
    </div>
@endsection

@section('scripts')
@endsection
