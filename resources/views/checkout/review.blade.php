@extends('master')
@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>Checkout</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>Checkout</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        @include('partials.messages')
        <div class="row">
            <!-- Checkout Adress-->
            <div class="col-xl-9 col-lg-8">
                <div class="checkout-steps">
                    <a class="active" href="#">3. Review</a>
                    <a class="@if($order->status != $orderStatus['ADDRESS'] && $order->status != $orderStatus['PENDING']) completed @endif" href="{{ route('order.payment.get', $order->hash) }}">
                        <span class="step-indicator icon-circle-check"></span>
                        <span class="angle"></span>2. Payment</a>
                    <a class="@if($order->status != $orderStatus['ADDRESS']) completed @endif" href="{{ route('order.address.get', $order->hash) }}">
                        <span class="step-indicator icon-circle-check"></span>
                        <span class="angle"></span>1. Address</a>
                </div>
                <h4>Review Your Order</h4>
                <div class="table-responsive shopping-cart">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th class="text-center">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orderProducts as $product)
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <a class="product-thumb" href="{{ route('shopSingle', $product->slug) }}">
                                            <img src="@if(count($product->images) > 0) {{ $product->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif" alt="Product"></a>
                                        <div class="product-info">
                                            <h4 class="product-title"><a href="{{ route('shopSingle', $product->slug) }}">{{ $product->name }}
                                                    <small>x {{ $product->quantity }}</small>
                                                </a></h4>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-lg text-medium">${{ number_format($product->on_sale ? $product->sale_price : $product->price, 2, '.', ',') }}</td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="shopping-cart-footer">
                    <div class="column"></div>
                    <div class="column text-lg">Subtotal: <span class="text-medium">${{ $order->bill_amount }}</span></div>
                </div>
                <div class="row padding-top-1x mt-3">
                    <div class="col-sm-6">
                        <h5>Shipping to:</h5>
                        @if($order->status != $orderStatus['ADDRESS'] && $order->address)
                        @php $address = $order->address @endphp
                        <ul class="list-unstyled">
                            <li><span class="text-muted">Client:</span>{{ $address->first_name.' '.$address->last_name }}</li>
                            <li><span class="text-muted">Address:</span>{{ $address->name.', '.$address->line1.', '.$address->line2.', '.$address->city.', '.$address->zip.', '.$address->getCountry()->name }}</li>
                            <li><span class="text-muted">Phone:</span>{{ $address->phone }}</li>
                        </ul>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        @if(count($order->payment) > 0)
                            @forelse($order->payment as $paymentInfo)
                                <h5>Payment Info</h5>
                                <ul class="list-unstyled">
                                    <li><span class="text-muted">Amount:</span>{{ $order->bill_amount }}</li>
                                    <li><span class="text-muted">Payment Method:</span>{{ $paymentInfo->payment_method == 'stripe'? 'Stripe' : 'PayPal' }}</li>
                                    <li><span class="text-muted">Payment Status:</span>{{ $paymentInfo->status }}</li>
                                </ul>
                            @empty
                                <h5>You have not made any payment.</h5>
                            @endforelse
                        @endif
                    </div>
                </div>
                <div class="checkout-footer margin-top-1x">
                    <div class="column hidden-xs-down"><a class="btn btn-outline-secondary" href="{{ route('order.payment.get', $order->hash) }}"><i class="icon-arrow-left"></i>&nbsp;Back</a></div>
                    <div class="column"><a class="btn btn-primary" href="{{ route('order.complete.get', $order->hash) }}">Complete Order</a>
                    </div>
                </div>
            </div>
            <!-- Sidebar          -->
            @include('checkout.partials.right-sidebar')
        </div>
    </div>
@endsection
