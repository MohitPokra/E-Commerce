@extends('master')
@section('pageContent')
    @include('home.hero')
    @include('home.topCategories')
    @include('home.featuredProducts')
    @include('home.guarantee')
    @include('home.benefits')
@endsection

