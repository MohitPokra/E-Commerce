<!--  guarantee-->
<section class="bg-faded padding-top-1x padding-bottom-1x">
    <div class="container-fluid">
        <h3 class="text-center mb-30 pb-2">Our Guarantee</h3>
        <div class="row">
            <div class="col-sm-4 text-center">
                <img class="d-block w-150 mx-auto img-thumbnail rounded-circle mb-2" src="{{ setting('home_guarantee_guarantor_image_url') }}"
                     alt="{{ setting('home_guarantee_guarantor_name') }}">
                <h6>{{ setting('home_guarantee_guarantor_name') }}</h6>
                <p class="text-muted mb-2">{{ setting('home_guarantee_guarantor_designations') }}</p>

            </div>
            <div class="col-sm-8 ">
                <blockquote>
                    {{ setting('home_guarantee_text') }}
                </blockquote>
                <cite class="float-right"><img height="81px" width="220px" class="img-responsive" src="{{ setting('home_guarantee_signature_image_url') }}"/></cite>
            </div>
        </div>
    </div>
</section>
