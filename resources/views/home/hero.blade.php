<!-- Main Slider-->
<section id="hero" class="container-fluid mt-5">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="fw-section rounded padding-top-4x padding-bottom-4x" style="background-image: url({{ setting('home_hero_background_image_url') }});">
                <span class="overlay rounded" style="opacity: .35;"></span>
                <div class="text-center">
                    <h3 class="display-4 text-normal text-white text-shadow mb-1">{{ setting('home_hero_header') }}</h3>
                    <h2 class="display-2 text-bold text-white text-shadow">{{ setting('home_hero_main_header') }}</h2>
                    <h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">{{ setting('home_hero_sub_header') }}</h4>
                    <br/>
                    <a class="btn btn-primary margin-bottom-none" href="{{ setting('home_hero_action_url') }}">{{ setting('home_hero_action_text') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
