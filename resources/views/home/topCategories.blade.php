<!-- Top Categories-->
<section class="container padding-top-3x">
    <h3 class="text-center mb-30">Top Categories</h3>
    <div class="row">
        @foreach($topCategories as $category)
        <div class="col-md-4 col-sm-6">
            <div class="card mb-30"><a class="card-img-tiles" href="{{ route('products.category', $category->slug) }}">
                    <div class="inner">
                        <div class="main-img"><img src="@if(isset($category->images[0])) {{ $category->images[0]->category_image_url }} @else {{ $noImageUrl }} @endif" alt="Category"></div>
                        <div class="thumblist"><img src="@if(isset($category->images[1])) {{ $category->images[1]->category_image_url }} @else {{ $noImageUrl }} @endif" alt="Category"><img src="@if(isset($category->images[2])) {{ $category->images[2]->category_image_url }} @else {{ $noImageUrl }} @endif" alt="Category"></div>
                    </div></a>
                <div class="card-body text-center">
                    <h4 class="card-title">{{ $category->name }}</h4>
                    <p class="text-muted">Starting from ${{ round($category->starting_price, 2) }}</p>
                    <a class="btn btn-outline-primary btn-sm" href="{{ route('products.category', $category->slug) }}">View Products</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{--todo: enable all categories button.--}}
    <div class="text-center"><a class="btn btn-outline-secondary margin-top-none" href="#">All Categories</a></div>
</section>
