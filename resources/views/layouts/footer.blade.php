<!-- Site Footer-->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <!-- Contact Info-->
                <section class="widget widget-light-skin">
                    <h3 class="widget-title">Get In Touch With Us</h3>

                    @if(setting('footer_phone'))
                        <p class="text-white"><i class="fal fa-phone"></i><span class="ml-1">{{ setting('footer_phone') }}</span></p>
                    @endif

                    <ul class="list-unstyled text-sm text-white">
                        <li>{{ setting('footer_address_name') }}</li>
                        <li>{{ setting('footer_address_line1') }}</li>
                        @if(setting('footer_address_line2'))
                            <li>{{ setting('footer_address_line2') }}</li>
                        @endif
                        <li>{{ setting('footer_address_city') }},{{ setting('footer_address_state') }} {{ setting('footer_address_zip') }} </li>
                    </ul>
                    <p><a class="navi-link-light" href="{{ setting('footer_email_click_handle_url') }}"><span class="opacity-50"><i class="fal fa-envelope"></i></span><span class="ml-1">{{ setting('footer_email') }}</span></a></p>
                </section>
            </div>
            <div class="col-lg-3 col-md-6">
                <img src="{{ setting('footer_promotional_image_url') }}"/>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- About Us-->
                @php
                    $aboutUs = setting('footer_about_us') ?? [];
                    $accountAndShippingInfo = setting('footer_account_info') ?? [];
                @endphp
                <section class="widget widget-links widget-light-skin">
                    <h3 class="widget-title">About Us</h3>
                    <ul>
                        @foreach($aboutUs as $about)
                            <li><a href="{{ $about['url'] }}">{{ $about['text'] }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
            <div class="col-lg-3 col-md-6">
                <!-- Account / Shipping Info-->
                <section class="widget widget-links widget-light-skin">
                    <h3 class="widget-title">Account &amp; Shipping Info</h3>
                    <ul>
                        @foreach($accountAndShippingInfo as $info)
                            <li><a href="{{ $info['url'] }}">{{ $info['text'] }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
        <hr class="hr-light mt-2 margin-bottom-2x">
        <div class="row">
            <div class="col-md-7 padding-bottom-1x">
                <!-- Payment Methods-->
                <div class="margin-bottom-1x" style="max-width: 615px;"><img src="{{ setting('footer_payment_method_image_url') }}" alt="Payment Methods">
                </div>
            </div>
            {{--<div class="col-md-5 padding-bottom-1x">--}}
                {{--<div class="margin-top-1x hidden-md-up"></div>--}}
                {{--<!--Subscription-->--}}
                {{--<form class="subscribe-form" action="{{('newsletter.subscribe') }}" method="post" target="_blank" novalidate>--}}
                    {{--<div class="clearfix">--}}
                        {{--<div class="input-group input-light">--}}
                            {{--<input class="form-control" type="email" name="EMAIL" placeholder="Your e-mail"><span class="input-group-addon"><i class="fal fa-at"></i></span>--}}
                        {{--</div>--}}
                        {{--<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->--}}
                        {{--<div style="position: absolute; left: -5000px;" aria-hidden="true">--}}
                            {{--<input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">--}}
                        {{--</div>--}}
                        {{--<button class="btn btn-primary" type="submit"><i class="icon-check"></i></button>--}}
                    {{--</div><span class="form-text text-sm text-white opacity-50">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</span>--}}
                {{--</form>--}}
            {{--</div>--}}
        </div>
        <!-- Copyright-->
        <p class="center-text footer-copyright">© All rights reserved. </p>
    </div>
</footer>
