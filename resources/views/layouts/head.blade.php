<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- SEO Meta Tags-->
    <title>@yield('title', settings('site_name')) - {{ settings('site_name') }}</title>
    <meta name="description" content="@yield('meta_description', 'E-Commerce Site')">
    <meta name="author" content="@yield('meta_author', 'Brad Dennis, Ph.D.')">
    @yield('meta')
    <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon and Apple Icons-->
    {{--<link rel="icon" type="image/x-icon" href="{{setting('favicon')}}">--}}
    <link rel="icon" type="image/png" href="{{setting('favicon')}}">
    <link rel="apple-touch-icon" href="{{setting('favicon')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{setting('favicon')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{setting('favicon')}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{setting('favicon')}}">

    <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{asset('css/vendor.min.css')}}">
    <!-- Main Template Styles-->
    <link rel="stylesheet" media="screen" href="{{asset('css/styles.min.css')}}">
    <!--Linear Icon-->
    <link  rel="stylesheet" media="screen" href="{{asset('css/linearicon/style.css')}}">
    <!--Page Level Styles-->
    @yield('style')
    <!--Custom Css-->
    <link  rel="stylesheet" media="screen" href="{{asset('css/custom.css')}}">
    <!-- Modernizr-->
    <script src="{{asset('template/dist/js/modernizr.min.js')}}"></script>
</head>
<!-- Body-->
<body>
