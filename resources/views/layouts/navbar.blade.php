<header class="navbar navbar-sticky">
    <!-- Search-->
    <form class="site-search" action="{{ route('shop.search') }}" method="get">
        <input type="text" name="query" placeholder="Search for....">
        <div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>
    </form>
    <div class="site-branding">
        <div class="inner">
            {{--<!-- Off-Canvas Toggle (#shop-categories)--><a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>--}}
            <!-- Off-Canvas Toggle (#mobile-menu)--><a class ="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
            <!-- Site Logo--><a class="site-logo" href="{{route('home')}}"><img src="{{ setting('front_logo') }}" alt="{{ setting('site_name') }}"></a>
        </div>
    </div>

    <div class="searchbar">
        <div class="input-group form-group">
            <span class="nav-search-form input-group-btn">
            <form action="{{ route('shop.search') }}" method="get">
                <span class="input-group-btn">
                  <button class="btn" type="submit"><i class="icon-search"></i></button>
                </span>
                <input class="form-control" type="text" name="query" placeholder="Search for...">
            </form>
            <span>
        </div>
    </div>
    <!-- Main Navigation-->
    <nav class="site-menu">
        <ul>
             {{--Home--}}
            <li class="has-megamenu active"><a href="{{ route('home') }}"><span>Home</span></a>
                {{--<ul class="mega-menu">--}}
                    {{--<li><a class="d-block img-thumbnail text-center navi-link" href="{{ route('home') }}"><img alt="Featured Products Slider" src="{{asset('template/dist/img/mega-menu-home/01.jpg')}}">--}}
                            {{--<h6 class="mt-3">Featured Products Slider</h6></a></li>--}}
                    {{--<li><a class="d-block img-thumbnail text-center navi-link" href="#"><img alt="Featured Categories" src="{{asset('template/dist/img/mega-menu-home/02.jpg')}}">--}}
                            {{--<h6 class="mt-3">Featured Categories</h6></a></li>--}}
                    {{--<li><a class="d-block img-thumbnail text-center navi-link" href="#"><img alt="Products Collection Showcase" src="{{asset('template/dist/img/mega-menu-home/03.jpg')}}">--}}
                            {{--<h6 class="mt-3">Products Collection Showcase</h6></a></li>--}}
                    {{--<li>--}}
                        {{--<div class="img-thumbnail text-center"><img alt="More To Come. Stay Tuned!" src="{{asset('template/dist/img/mega-menu-home/04.jpg')}}">--}}
                            {{--<h6 class="mt-3">More To Come. Stay Tuned!</h6>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </li>
            {{--Shop--}}
            <li class="has-megamenu"><a href="{{ route('products.all') }}"><span>Shop</span></a>
                <ul class="mega-menu">
                    <li><span class="mega-menu-title">Categories</span>
                        <ul class="sub-menu">
                            @foreach($categories as $category)
                                <li><a href="{{ route('products.category', $category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><span class="mega-menu-title">Brands</span>
                        <ul class="sub-menu">
                            @foreach($brands as $brand)
                            <li><a href="{{ route('products.brand', $brand->slug) }}">{{ $brand->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <section class="promo-box" style="background-image: url('/template/dist/img/banners/02.jpg');"><span class="overlay-dark" style="opacity: .4;"></span>
                            <div class="promo-box-content text-center padding-top-2x padding-bottom-2x">
                                <h4 class="text-light text-thin text-shadow">Lorem Ipsum</h4>
                                <h3 class="text-bold text-light text-shadow">ad minima</h3><a class="btn btn-sm btn-primary" href="#">Action</a>
                            </div>
                        </section>
                    </li>
                    <li>
                        <section class="promo-box" style="background-image: url('/template/dist/img/banners/03.jpg');"><span class="overlay-dark" style="opacity: .4;"></span>
                            <div class="promo-box-content text-center padding-top-2x padding-bottom-2x">
                                <h4 class="text-light text-thin text-shadow">Lorem Ipsum</h4>
                                <h3 class="text-bold text-light text-shadow">ad minima</h3><a class="btn btn-sm btn-primary" href="#">Action</a>
                            </div>
                        </section>
                    </li>
                </ul>
            </li>
            {{--Blog--}}
            {{--<li><a href="blog-rs.html"><span>Blog</span></a>--}}
            <li><a href="#"><span>Blog</span></a>
                {{--<ul class="sub-menu">--}}
                    {{--<li class="has-children"><a href="#"><span>Blog Layout</span></a>--}}
                        {{--<ul class="sub-menu">--}}
                            {{--<li><a href="#">Blog Right Sidebar</a></li>--}}
                            {{--<li><a href="#">Blog Left Sidebar</a></li>--}}
                            {{--<li><a href="#">Blog No Sidebar</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="has-children"><a href="#"><span>Single Post Layout</span></a>--}}
                        {{--<ul class="sub-menu">--}}
                            {{--<li><a href="#">Post Right Sidebar</a></li>--}}
                            {{--<li><a href="#">Post Left Sidebar</a></li>--}}
                            {{--<li><a href="#">Post No Sidebar</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </li>
        </ul>
    </nav>
    <!-- Toolbar-->
    <div class="toolbar">
        <div class="inner">
            <div class="tools">
                {{--Search Icon Visible on Small window size only--}}
                <div class="search"><i class="icon-search"></i></div>

                {{--Account--}}
                <div class="account">
                    {{--Todo: change account drop down after roles and permission management.--}}
                    {{--@if(Auth::check())--}}
                        {{--@if(Auth::user()->hasRole('Admin'))--}}
                            {{--@include('shop1.toolbars.admin')--}}
                        {{--@else(Auth::user()->hasRole('User'))--}}
                            {{--@include('shop1.toolbars.customer')--}}
                        {{--@endif--}}
                    {{--@else--}}
                        {{--@include('shop1.toolbars.guest')--}}
                    {{--@endif--}}
                    @if($account !== null)
                        <a href="{{ route('account.orders') }}"></a>
                    @endif

                    <i class="lnr lnr-user"></i>

                    @if($account !== null)
                    <ul class="toolbar-dropdown">
                        <li class="sub-menu-user">
                            <div class="user-ava"><img src="{{ $account->gravatar() }}" alt="{{ $account->first_name . ' ' . $account->last_name }}">
                            </div>
                            <div class="user-info">
                                <h6 class="user-name">{{ $account->first_name }}</h6><span class="text-xs text-muted">290 Reward points</span>
                            </div>
                        </li>
                        <li><a href="{{ route('account.profile') }}">My Profile</a></li>
                        <li><a href="{{ route('account.orders') }}">Orders List</a></li>
                        @if(strtolower($account->role()->first()->name) === 'admin')
                         <li><a href="{{ route('admin.dashboard') }}">Admin Dashboard</a></li>
                        @endif
{{--                        <li><a href="{{ route('account.wishList') }}">Wishlist</a></li>--}}
                        <li class="sub-menu-separator"></li>
                        <li><a href="{{ url('logout') }}"> <i class="icon-unlock"></i>Logout</a></li>
                    </ul>
                    @else
                        <ul class="toolbar-dropdown">
                            <li><a href="{{ url('login') }}"> <i class="icon-unlock"></i>Login/Register</a></li>
                            {{--<li class="sub-menu-separator"></li>--}}
                            {{--<li><a href="{{ route('account.wishList') }}">Wishlist</a></li>--}}
                        </ul>
                    @endif
                </div>

                {{--Cart--}}
                @include('layouts.partials.navbar_cart')
            </div>
        </div>
    </div>
</header>
