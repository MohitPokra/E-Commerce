@if (isset($cart))
    <div class="cart"><a href="{{ route('cart') }}"></a><i class="lnr lnr-cart"></i><span class="count">{{ count($cart) }}</span>
        <div class="toolbar-dropdown">
            @if(count($cart) > 0)
                @foreach($cart as $product)
                    <div class="dropdown-product-item"><a href="javascript:;" onclick="removeFromCart(event, '{{ $product->cart_id }}')"><span class="dropdown-product-remove"><i class="icon-cross"></i></span></a><a class="dropdown-product-thumb" href="{{ route('shopSingle', $product->slug) }}"><img src="@if(count($product->images) > 0) {{ $product->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif" alt="Item"></a>
                        <div class="dropdown-product-info"><a class="dropdown-product-title" href="{{ route('shopSingle', $product->slug) }}">{{ $product->name }}</a><span class="dropdown-product-details">{{ $product->quantity }} x $@if($product->on_sale){{ number_format($product->sale_price, 2, '.', ',') }}@else{{ number_format($product->price, 2, '.', ',') }}@endif</span></div>
                    </div>
                @endforeach
                <div class="toolbar-dropdown-group">
                    <div class="column"><span class="text-lg">Total:</span></div>
                    <div class="column text-right"><span class="text-lg text-medium">${{ $cartTotal }}&nbsp;</span></div>
                </div>
                <div class="toolbar-dropdown-group">
                    <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="{{ route('cart') }}">View Cart</a></div>
                </div>
            @else
                You have no item in cart.
            @endif
        </div>
    </div>
@endif
