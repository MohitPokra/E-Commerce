<div class="grid-item">
    <div class="product-card">
        @if($product->on_sale && $product->price > 0)
            <div class="product-badge text-danger">{{ $product->getPercentOff() }}% Off</div>
        @else
            {{--todo: set number of stars according to rating of the product.--}}
            <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i></div>
        @endif
        <a class="product-thumb" href="{{ route('shopSingle', $product->slug) }}"><img src="@if(count($product->images) > 0) {{ $product->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif" alt="Product"></a>
        <h3 class="product-title"><a href="{{ route('shopSingle', $product->slug) }}">{{ $product->name }}</a></h3>
        <h4 class="product-price">
            @if ($product->on_sale)
                <del>${{ number_format($product->price, 2, '.', ',') }}</del>${{ number_format($product->sale_price, 2, '.', ',') }}
            @else
                ${{ number_format($product->price, 2, '.', ',') }}
            @endif
        </h4>
        <div class="product-buttons">
            @if($product->in_stock)
                <button class="btn btn-outline-primary btn-sm" onclick="addToCart(event, { quantity: 1, product_id: '{{ $product->id }}'})">Add to Cart</button>
            @else
                <button class="btn btn-outline-primary btn-sm" disabled>Sold out</button>
            @endif
        </div>
    </div>
</div>
