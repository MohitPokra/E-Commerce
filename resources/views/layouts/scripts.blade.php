
<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="{{asset('template/dist/js/vendor.min.js')}}"></script>
<script src="{{asset('template/dist/js/scripts.min.js')}}"></script>
<script src="{{ asset('js/lodash.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function showToaster(data) {
        const type = data.type,
        title = data.title,
        message = data.message,
        position = data.position,
        timeout = data.timeout,
        buttons = data.buttons,
        close = data.close;
        let icon = data.icon;

        if(typeof data.icon === 'undefined') {
            switch (type) {
                case 'info':
                    icon = 'icon-bell';
                    break;
                case 'success':
                    icon = 'icon-circle-check';
                    break;
                case 'warning':
                    icon = 'icon-flag';
                    break;
                case 'danger':
                    icon = 'icon-ban';
                    break;
                default:
                    icon = 'icon-heart';
            }
        }

        const toastOptions = {
            class: 'iziToast-' + type || '',
            close: close || true,
            title: title || 'Title',
            message: message || 'toast message',
            animateInside: false,
            position: position || 'topRight',
            progressBar: false,
            icon: icon,
            timeout: timeout || 3200,
            transitionIn: 'fadeInLeft',
            transitionOut: 'fadeOut',
            transitionInMobile: 'fadeIn',
            transitionOutMobile: 'fadeOut',
            buttons: buttons || [],
        };
        iziToast.show(toastOptions);
    }

    function removeFromCart(evt, id) {
        evt.preventDefault();
        let url = '{{ route('cart.remove', ':id') }}';
        url = url.replace(':id', id);
        $.ajax({
            type: 'post',
            url: url,
            data: {_method: 'delete'},
            success: function (resp) {
                const response = resp.responseJSON;
                if (resp.status === 'success') {
                    showToaster({
                        type: 'success',
                        title: 'Removed',
                        message: 'from cart successfully.'
                    });
                }
                else {
                    showToaster({
                        type: 'danger',
                        title: 'Error',
                        message: response.message
                    });
                }
                window.location.reload();
            },
            error: function (resp) {
                let message = 'Some error has occur.';
                if (resp.status === 422) {
                    resp = resp.responseJSON;
                    if (typeof resp.quantity !== 'undefined') {
                        message = resp.quantity[0];
                    }
                }
                showToaster({
                    type: 'danger',
                    title: 'Error',
                    message: message
                });
            }
        });
    }

    function addToCart(evt, data) {
        evt.preventDefault();
        const url = '{{ route('cart.add') }}';

        if(data.quantity === -1) {
            data.quantity = $('').val();
        }
        if (typeof data.quantity === 'undefined' && typeof data.quantityGetter !== 'undefined') {
            data.quantity = $(data.quantityGetter).val();
        }

        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function () {
                showToaster({
                    type: 'success',
                    title: 'Added',
                    message: 'to cart successfully.'
                });
                window.location.reload();
            },
            error: function (resp) {
                let message = 'Some error has occur.';
                if (resp.status === 422) {
                    resp = resp.responseJSON;
                    if (typeof resp.quantity !== 'undefined') {
                        message = resp.quantity[0];
                    }
                }
                else if (resp.status === 401) {
                    showToaster({
                        type: 'danger',
                        title: 'Error',
                        message: 'Please login first.'
                    });
                }
                else {
                    showToaster({
                        type: 'danger',
                        title: 'Error',
                        message: message
                    });
                }
            }
        });
    }

    function checkout (evt) {
        evt.preventDefault();
        showToaster({
            type: '',
            title: 'Please Wait',
            message: 'checking for the availability of products.',
            position: 'topCenter',

            timeout: -1,
            close: false,
            buttons: [
                ['<button id="close_checkout_toast" hidden>Close</button>', function (instance, toast) {
                    instance.hide({
                        transitionOut: 'fadeOutUp',
                    }, toast, 'buttonName');
                }]
            ],
        });

        $('#cart-product-error').remove();

        $.ajax({
            type: 'POST',
            url: '{{ route('order.checkout') }}',
            success: function (resp) {
                $('#close_checkout_toast').click();
                let redirectUrl = '{{ route('order.address.get', ':hash') }}';
                window.location.href = redirectUrl.replace(':hash', resp.hash);
            },
            error: function (error) {
                $('#close_checkout_toast').click();
                if(error.status === 422) {
                    $('#cart-product-container').prepend(error.responseText);
                }
                $(document).scrollTop(0);
            },
        });
    }
</script>
 @yield('scripts')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ config('analytics.tracking_id') }}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ config('analytics.tracking_id') }}');
</script>

</body>
</html>
