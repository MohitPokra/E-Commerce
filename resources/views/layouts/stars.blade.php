<div class="rating-stars">
    @if(isset($star))
        @for ($i = 0; $i < 5; $i++)
            <i class="icon-star @if($i < $star)filled @endif"></i>
        @endfor
    @endif
</div>
