


<li class="account-menu">

    <a href="{{ route('admin.dashboard') }}">
        <span class="pr-2"><i class="fas fa-user-alt fa-2x"></i></span>
    </a>
    <ul class="sub-menu">
        <li> Welcome {{ Auth::user()->first_name }}! </li>
        <li>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('account.logout') }}"> <i class="icon-unlock"></i>Logout</a></li>
        </li>
    </ul>
</li>
