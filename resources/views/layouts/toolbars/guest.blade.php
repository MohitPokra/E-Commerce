
<li class="account-menu">

    <a href="{{ route('login') }}">
        <span class="pr-2"><i class="fal fa-user-alt fa-2x"></i></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a href="{{ route('login') }}">Login / Register</a>
        </li>
        <li>
            <a href="{{ route('account.remind') }}">Password Recovery</a>
        </li>
        <li>
            <a href="{{ route('account.orders') }}">Orders List</a>
        </li>
        <li>
            <a href="{{ route('account.tracking') }}">Where's My Stuff</a>
        </li>
    </ul>
</li>
