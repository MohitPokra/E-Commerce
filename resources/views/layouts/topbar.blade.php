
<!-- Topbar-->
<div class="topbar hidden-md-down">
    <div class="topbar-column">
        <a class="hidden-md-down" href="{{('shop.shipping')}}"><i class="fal fa-shipping-fast"></i><span class="ml-1">FREE SHIPPING ON ORDERS OVER $50</a>
        <a class="hidden-md-down" href="{{('contact')}}"><i class="fal fa-envelope"></i><span class="ml-1">{{ Config::get('shop.email') }}</a>
    </div>
    <div class="topbar-column">
        <a class="hidden-md-down" href="{{('shop.deals')}}"><i class="fal fa-piggy-bank"></i><span class="ml-1">SEE DEALS</a>
        <a class="hidden-md-down" href="{{('account.reorder')}}"><i class="fal fa-sync"></i><span class="ml-1">EASY REORDER</a>
    </div>
</div>
