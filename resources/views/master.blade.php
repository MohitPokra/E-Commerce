@include('layouts.head')
{{--@include('layouts.sidebar')--}}
@include('layouts.topbar')
@include('layouts.navbar')

<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">
    <!-- Page Content-->
    @yield('pageContent')
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>

@include('layouts.footer')
@include('layouts.scripts')
