<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Order No  - {{ $order->hash }}</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="table-responsive shopping-cart mb-0">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th class="text-center">Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>
                                <div class="product-item"><a class="product-thumb" href="{{ route('shopSingle', $product->slug) }}">
                                        <img src="@if(count($product->images) > 0) {{ $product->images[0]->product_image_url }} @else {{ $noImageUrl }} @endif" alt="Product"></a>
                                    <div class="product-info">
                                        <h4 class="product-title"><a href="{{ route('shopSingle', $product->slug) }}">{{ $product->name }}
                                                <small>x {{ $product->quantity }}</small>
                                            </a></h4>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center text-lg text-medium">${{ number_format($product->on_sale ? $product->sale_price : $product->price, 2, '.', ',') }}</td>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="mb-3">
            <div class="d-flex flex-wrap justify-content-between align-items-center pb-2">
                <div class="px-2 py-1">Subtotal: <span class='text-medium'>${{ $order->amount }}</span></div>
                <div class="px-2 py-1">Discount: <span class='text-medium'>${{ $order->total_discount }}</span></div>
                <div class="px-2 py-1">Tax: <span class='text-medium'>${{ $order->total_tax }}</span></div>
                <div class="text-lg px-2 py-1">Total: <span class='text-medium'>${{ $order->bill_amount }}</span></div>
            </div>
        </div>
    </div>
</div>
