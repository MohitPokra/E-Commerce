@extends('master')

@section('title', $pageTitle)
@section('meta_description', $metaDescription)

@section('pageContent')
    <!-- Page Title-->
    @include('shop.title')
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-1">
        <div class="row products-container">
            <!-- Products-->
            {{--<div class="col-xl-9 col-lg-8 order-lg-2">--}}
            <div class="col-xl-12">
                <!-- Shop Toolbar-->
{{--                @include('shop.toolbar')--}}
                <!-- Products Grid-->
                @include('shop.products')
                <!-- Pagination-->
                @include('shop.pagination')
            </div>
            <!-- Sidebar          -->.
{{--            @include('shop.sidebar')--}}
        </div>
    </div>
@endsection
@section('scripts')
<script>
    // $('.products-container > .col-xl-12 > .pagination > .column > ul > li').on('click', 'a', function () {
    //     console.log(this);
    //     const url = $(this).data('href');
    //     $.ajax({
    //         type: 'get',
    //         url: url,
    //         success: function (resp) {
    //             console.log(resp);
    //             $('.products-container').html(resp);
    //         }
    //     });
    // });
</script>
@endsection
