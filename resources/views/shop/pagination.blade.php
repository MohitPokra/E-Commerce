<nav class="pagination">
    <div class="column">
        <ul class="pages">
            @php
            $currentPage = $pagination['current_page'];
            $lastPage = $pagination['last_page'];
            $currentPageUrl = $pagination['current_page_url'];
            @endphp

            @if($currentPage > 2)
                <li><a href="{{ $currentPageUrl.'?page=1' }}">1</a></li>
            @endif

            @if($currentPage > 3)
                <li>...</li>
            @endif

            @if($currentPage > 1)
                <li><a href="{{ $currentPageUrl.'?page='.($currentPage - 1) }}">{{ $currentPage - 1 }}</a></li>
            @endif

            <li class="active"><a href="#" disabled="true">{{ $currentPage }}</a></li>

            @if(($lastPage - $currentPage) > 0)
                <li><a href="{{ $currentPageUrl.'?page='.($currentPage + 1) }}">{{ $currentPage + 1 }}</a></li>
            @endif

            @if(($lastPage - $currentPage) > 2)
                <li>...</li>
            @endif

            @if(($lastPage - $currentPage) > 1)
                <li><a href="{{ $currentPageUrl.'?page='.$lastPage }}">{{ $lastPage }}</a></li>
            @endif
        </ul>
    </div>
    @if($currentPage > 1)
    <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="{{ $currentPageUrl.'?page='.($currentPage - 1) }}"><i class="icon-arrow-left"></i>&nbsp;Prev</a></div>
    @endif
    @if(($lastPage - $currentPage) > 0)
    <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="{{ $currentPageUrl.'?page='.($currentPage + 1) }}">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
    @endif
</nav>
