{{--<div class="isotope-grid cols-3 mb-2">--}}
<div class="isotope-grid cols-4 mb-2">
    <div class="gutter-sizer"></div>
    <div class="grid-sizer"></div>
    <!-- Product-->
    @if (count($products) > 0)
        @foreach($products as $product)
            @include('layouts.productGrid')
        @endforeach
    @else
        <h3 class="text-lg-center text-"> No matching item found </h3>
    @endif
</div>
