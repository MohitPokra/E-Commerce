<div class="col-xl-3 col-lg-4 order-lg-1">
    <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
    <aside class="sidebar sidebar-offcanvas">
        <!-- Widget Categories-->
        <section class="widget widget-categories">

            <h3 class="widget-title">Shop Categories ({{ count($categories) }})</h3>
            @foreach($categories as $category)
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input category-filter" type="checkbox" id="{{ $category->slug }}">
                    <label class="custom-control-label" for="{{ $category->slug }}">{{ $category->name }}
                        {{--<span class="text-muted">(254)</span>--}}
                    </label>
                </div>
            @endforeach
            {{--<ul>--}}
                {{--<li class="has-children expanded"><a href="#">Shoes</a><span>(1138)</span>--}}
                    {{--<ul>--}}
                        {{--<li><a href="#">Women's</a><span>(508)</span>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Sneakers</a></li>--}}
                                {{--<li><a href="#">Heels</a></li>--}}
                                {{--<li><a href="#">Loafers</a></li>--}}
                                {{--<li><a href="#">Sandals</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Men's</a><span>(423)</span>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Boots</a></li>--}}
                                {{--<li><a href="#">Oxfords</a></li>--}}
                                {{--<li><a href="#">Loafers</a></li>--}}
                                {{--<li><a href="#">Sandals</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Boy's Shoes</a><span>(97)</span></li>--}}
                        {{--<li><a href="#">Girl's Shoes</a><span>(110)</span></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="has-children"><a href="#">Clothing</a><span>(2356)</span>--}}
                    {{--<ul>--}}
                        {{--<li><a href="#">Women's</a><span>(1032)</span>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Dresses</a></li>--}}
                                {{--<li><a href="#">Shirts &amp; Tops</a></li>--}}
                                {{--<li><a href="#">Swimwear</a></li>--}}
                                {{--<li><a href="#">Shorts</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Men's</a><span>(937)</span>--}}
                            {{--<ul>--}}
                                {{--<li><a href="#">Shirts &amp; Tops</a></li>--}}
                                {{--<li><a href="#">Shorts</a></li>--}}
                                {{--<li><a href="#">Swimwear</a></li>--}}
                                {{--<li><a href="#">Pants</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Kid's Clothing</a><span>(386)</span></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="has-children"><a href="#">Bags</a><span>(420)</span>--}}
                    {{--<ul>--}}
                        {{--<li><a href="#">Handbags</a><span>(180)</span></li>--}}
                        {{--<li><a href="#">Backpacks</a><span>(132)</span></li>--}}
                        {{--<li><a href="#">Wallets &amp; Accessories</a><span>(47)</span></li>--}}
                        {{--<li><a href="#">Luggage</a><span>(61)</span></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="has-children"><a href="#">Accessories</a><span>(874)</span>--}}
                    {{--<ul>--}}
                        {{--<li><a href="#">Sunglasses</a><span>(211)</span></li>--}}
                        {{--<li><a href="#">Hats</a><span>(195)</span></li>--}}
                        {{--<li><a href="#">Watches</a><span>(159)</span></li>--}}
                        {{--<li><a href="#">Jewelry</a><span>(203)</span></li>--}}
                        {{--<li><a href="#">Belts</a><span>(106)</span></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        </section>
        <!-- Widget Price Range-->
        {{--<section class="widget widget-categories">--}}
            {{--<h3 class="widget-title">Price Range</h3>--}}
            {{--<form class="price-range-slider" method="post" data-start-min="250" data-start-max="650" data-min="0" data-max="1000" data-step="1">--}}
                {{--<div class="ui-range-slider"></div>--}}
                {{--<footer class="ui-range-slider-footer">--}}
                    {{--<div class="column">--}}
                        {{--<button class="btn btn-outline-primary btn-sm" type="submit">Filter</button>--}}
                    {{--</div>--}}
                    {{--<div class="column">--}}
                        {{--<div class="ui-range-values">--}}
                            {{--<div class="ui-range-value-min">$<span></span>--}}
                                {{--<input type="hidden">--}}
                            {{--</div>&nbsp;-&nbsp;--}}
                            {{--<div class="ui-range-value-max">$<span></span>--}}
                                {{--<input type="hidden">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</footer>--}}
            {{--</form>--}}
        {{--</section>--}}
        <!-- Widget Brand Filter-->
        <section class="widget">
            <h3 class="widget-title">Filter by Brand</h3>
            @foreach($brands as $brand)
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input brand-filter" type="checkbox" id="{{ $brand->slug }}">
                    <label class="custom-control-label" for="{{ $brand->slug }}">{{ $brand->name }}
                        {{--<span class="text-muted">(254)</span>--}}
                    </label>
                </div>
            @endforeach
        </section>
        <!-- Promo Banner-->
        <section class="promo-box" style="background-image: url(../../../../../Downloads/template-1/dist/img/banners/02.jpg);">
            <!-- Choose between .overlay-dark (#000) or .overlay-light (#fff) with default opacity of 50%. You can overrride default color and opacity values via 'style' attribute.--><span class="overlay-dark" style="opacity: .45;"></span>
            <div class="promo-box-content text-center padding-top-3x padding-bottom-2x">
                <h4 class="text-light text-thin text-shadow">New Collection of</h4>
                <h3 class="text-bold text-light text-shadow">Sunglassess</h3><a class="btn btn-sm btn-primary" href="#">Shop Now</a>
            </div>
        </section>
    </aside>
</div>
