<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>@yield('title', 'Products')</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{ route('home') }}">Home</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>@yield('title', 'Products')</li>
            </ul>
        </div>
    </div>
</div>
