@extends('master')
@section('pageContent')
    <!-- Page Content-->
    <!-- Hero-->
    <section class="fw-section padding-top-10x padding-bottom-10x" style="background-image: url(../../../../../Downloads/template-1/dist/img/hero/hero-bg.jpg);">
        <div class="container padding-top-2x text-center">
            <div class="d-inline-block mb-30"><img class="d-block w-150" src="../../../../../Downloads/template-1/dist/img/hero/logo-braun.png" alt="Braun"></div>
            <h2 class="text-white text-normal mb-2">Braun premium watch collection</h2>
            <h6 class="text-white text-normal opacity-80 mb-4">Check our new collection of watches that represents true quality and functionality</h6><a class="btn btn-primary scroll-to" href="#collections">View Collection</a>
        </div>
    </section>
    <!-- Video Carousel-->
    <section class="container padding-top-3x" id="collections">
        <h3 class="text-center mb-30">Featured Collections (Video)</h3>
        <div class="row pt-1 justify-content-center">
            <div class="col-xl-10">
                <div class="gallery-wrapper owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true }">
                    <div class="gallery-item no-hover-effect"><a href="#" data-type="video" data-video="&lt;div class=&quot;wrapper&quot;&gt;&lt;div class=&quot;video-wrapper&quot;&gt;&lt;iframe class=&quot;pswp__video&quot; width=&quot;960&quot; height=&quot;640&quot; src=&quot;https://player.vimeo.com/video/138235312?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;&lt;/div&gt;&lt;/div&gt;"><img src="../../../../../Downloads/template-1/dist/img/video-covers/01.jpg" alt="Cover"></a><span class="caption">Classic Collection</span></div>
                    <div class="gallery-item no-hover-effect"><a href="#" data-type="video" data-video="&lt;div class=&quot;wrapper&quot;&gt;&lt;div class=&quot;video-wrapper&quot;&gt;&lt;iframe class=&quot;pswp__video&quot; width=&quot;960&quot; height=&quot;640&quot; src=&quot;https://player.vimeo.com/video/138290617?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;&lt;/div&gt;&lt;/div&gt;"><img src="../../../../../Downloads/template-1/dist/img/video-covers/02.jpg" alt="Cover"></a><span class="caption">Prestige Collection</span></div>
                    <div class="gallery-item no-hover-effect"><a href="#" data-type="video" data-video="&lt;div class=&quot;wrapper&quot;&gt;&lt;div class=&quot;video-wrapper&quot;&gt;&lt;iframe class=&quot;pswp__video&quot; width=&quot;960&quot; height=&quot;640&quot; src=&quot;https://player.vimeo.com/video/138235312?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;&lt;/div&gt;&lt;/div&gt;"><img src="../../../../../Downloads/template-1/dist/img/video-covers/03.jpg" alt="Cover"></a><span class="caption">Sports Collection</span></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Products Grid-->
    <section class="bg-secondary padding-top-3x padding-bottom-2x margin-top-3x">
        <div class="container">
            <h3 class="text-center mb-30">Explore Watches</h3>
            <ul class="nav nav-pills justify-content-center pb-2">
                <li class="nav-item"><a class="nav-link active" href="#" data-filter="*">All</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".classic">Classic</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".digital">Digital</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".prestige">Prestige</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".sport">Sport</a></li>
            </ul>
            <div class="isotope-grid filter-grid cols-4 mt-4">
                <div class="gutter-sizer"></div>
                <div class="grid-sizer"></div>
                <!-- Product-->
                <div class="grid-item classic">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/17.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0024 Classic</a></h3>
                        <h4 class="product-price">€185.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item digital">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/21.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0106 Digital </a></h3>
                        <h4 class="product-price">€655.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item prestige">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/25.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0095 Prestige</a></h3>
                        <h4 class="product-price">€725.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item classic">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/19.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0211 Classic</a></h3>
                        <h4 class="product-price">€350.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item digital">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/22.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0076 Digital</a></h3>
                        <h4 class="product-price">€225.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item sport">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/26.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Ladies BN0111 Sport</a></h3>
                        <h4 class="product-price">€140.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item prestige">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/28.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0221 Prestige</a></h3>
                        <h4 class="product-price">€435.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item classic">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/18.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0142 Classic</a></h3>
                        <h4 class="product-price">€215.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item prestige">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/24.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0095 Prestige</a></h3>
                        <h4 class="product-price">€655.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item classic">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/20.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0032 Classic</a></h3>
                        <h4 class="product-price">€185.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item digital">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/23.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Gents BN0106 Digital</a></h3>
                        <h4 class="product-price">€585.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
                <!-- Product-->
                <div class="grid-item sport">
                    <div class="product-card"><a class="product-thumb" href="shop-single.blade.php"><img src="../../../../../Downloads/template-1/dist/img/shop/products/27.jpg" alt="Product"></a>
                        <h3 class="product-title"><a href="shop-single.blade.php">Ladies BN0111 Sport</a></h3>
                        <h4 class="product-price">€140.00</h4>
                        <div class="product-buttons">
                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services-->
    <section class="container padding-top-3x padding-bottom-2x">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="../../../../../Downloads/template-1/dist/img/services/01.png" alt="Shipping">
                <h6>Free Worldwide Shipping</h6>
                <p class="text-muted margin-bottom-none">Free shipping for all orders over $100</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="../../../../../Downloads/template-1/dist/img/services/02.png" alt="Money Back">
                <h6>Money Back Guarantee</h6>
                <p class="text-muted margin-bottom-none">We return money within 30 days</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="../../../../../Downloads/template-1/dist/img/services/03.png" alt="Support">
                <h6>24/7 Customer Support</h6>
                <p class="text-muted margin-bottom-none">Friendly 24/7 customer support</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="../../../../../Downloads/template-1/dist/img/services/04.png" alt="Payment">
                <h6>Secure Online Payment</h6>
                <p class="text-muted margin-bottom-none">We posess SSL / Secure Certificate</p>
            </div>
        </div>
    </section>
@endsection
