@extends('master')
@section('pageContent')
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>Order Tracking</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="separator">&nbsp;</li>
                    <li>Order Tracking</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-1">
        <div class="card mb-3">
            <div class="p-4 text-center text-white text-lg bg-dark rounded-top"><span class="text-uppercase">Tracking Order No - </span><span
                        class="text-medium">{{ $order->hash }}</span></div>
            <div class="d-flex flex-wrap flex-sm-nowrap justify-content-between py-3 px-2 bg-secondary">
                @if($order->shipping_service)
                <div class="w-100 text-center py-1 px-2"><span class='text-medium'>Shipped Via:</span> {{ $order->shipping_service }}</div>
                @endif
                <div class="w-100 text-center py-1 px-2"><span class='text-medium'>Status:</span> {{ $order->status }}</div>
                @if($order->status == $orderStatus['DELIVERED'])
                <div class="w-100 text-center py-1 px-2"><span class='text-medium'>Delivery Date:</span> {{ $order->delivery_date->toFormattedDateString() }}
                </div>
                @elseif($order->status == $orderStatus['CANCELLED'])
                @else
                <div class="w-100 text-center py-1 px-2"><span class='text-medium'>Expected Date:</span> {{ $order->expected_delivery_date->toFormattedDateString() }}
                </div>
                @endif
            </div>
            <div class="card-body">
                <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">
                    <div class="step completed">
                        <div class="step-icon-wrap">
                            <div class="step-icon"><i class="pe-7s-cart"></i></div>
                        </div>
                        <h4 class="step-title">Confirmed Order</h4>
                    </div>
                    <div class="step @if($order->status != $orderStatus['ADDRESS'])completed @endif">
                        <div class="step-icon-wrap">
                            <div class="step-icon"><i class="pe-7s-config"></i></div>
                        </div>
                        <h4 class="step-title">Processing Order</h4>
                    </div>
                    <div class="step @if($order->status != $orderStatus['ADDRESS'] && $order->status != $orderStatus['PENDING'])completed @endif">
                        <div class="step-icon-wrap">
                            <div class="step-icon"><i class="pe-7s-medal"></i></div>
                        </div>
                        <h4 class="step-title">Quality Check</h4>
                    </div>
                    <div class="step @if($order->status == $orderStatus['SHIPPED'] || $order->status == $orderStatus['DELIVERED'])completed @endif">
                        <div class="step-icon-wrap"> 
                            <div class="step-icon"><i class="pe-7s-car"></i></div>
                        </div>
                        <h4 class="step-title">Product Dispatched</h4>
                    </div>
                    <div class="step @if($order->status == $orderStatus['DELIVERED'])completed @endif">
                        <div class="step-icon-wrap">
                            <div class="step-icon"><i class="pe-7s-home"></i></div>
                        </div>
                        <h4 class="step-title">Product Delivered</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap flex-md-nowrap justify-content-center justify-content-sm-between align-items-center">
            <div class="custom-control custom-checkbox mr-3">
                {{--<input class="custom-control-input" type="checkbox" id="notify_me" checked>--}}
                {{--<label class="custom-control-label" for="notify_me">Notify me when order is delivered</label>--}}
            </div>
            <div class="text-left text-sm-right">
                <a class="btn btn-outline-primary btn-rounded btn-sm"
                   href="javascript:;" data-toggle="modal" data-target="#orderDetails">
                    View Order Details</a></div>
        </div>
    </div>
@endsection

@section ('scripts')
<!-- Open Ticket Modal-->
<div class="modal fade" id="orderDetails" tabindex="-1">
    @include('partials.orderModal')
</div>
@endsection
