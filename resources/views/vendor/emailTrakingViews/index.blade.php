@extends(config('mail-tracker.admin-template.name'))
@section('title')
    Mail Tracker
    @parent
@stop
@section(config('mail-tracker.admin-template.section'))
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Mail Tracker
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">
                SNS Endpoint: {{ route('mailTracker_SNS') }}
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content-body">
        <div class="row">
            <div class="col-lg-12 text-center">
                <form action="{{ route('mailTracker_Search') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-4 offset-2">
                            <input type="text" placeholder="Search..." class="form-control" name="search" id="search" value="{{ session('mail-tracker-index-search') }}" style="line-height: 1.75"><small class="help-block"></small>
                        </div>

                        <div class="col-2">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-search"></span> Search
                            </button>
                        </div>

                        <a class="col-2 text-left" href="{{ route('mailTracker_ClearSearch') }}">
                            <div class="btn btn-danger">
                                Clear Search
                            </div>
                        </a>
                    </div>
                </form>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>SMTP</th>
                        <th>Recipient</th>
                        <th>Subject</th>
                        <th>Opens</th>
                        <th>Clicks</th>
                        <th>Sent At</th>
                        <th>View Email</th>
                        <th>Clicks</th>
                    </tr>
                @foreach($emails as $email)
                    <tr class="{{ $email->report_class }}">
                      <td>
                        <a href="{{route('mailTracker_SmtpDetail',$email->id)}}" target="_blank">
                          {{ str_limit($email->smtp_info, 20) }}
                        </a>
                      </td>
                      <td>{{$email->recipient}}</td>
                      <td>{{$email->subject}}</td>
                      <td>{{$email->opens}}</td>
                      <td>{{$email->clicks}}</td>
                      <td>{{$email->created_at->format(config('mail-tracker.date-format'))}}</td>
                      <td>
                          <a href="{{route('mailTracker_ShowEmail',$email->id)}}" target="_blank">
                            View
                          </a>
                      </td>
                      <td>
                          @if($email->clicks > 0)
                              <a href="{{route('mailTracker_UrlDetail',$email->id)}}">Url Report</a>
                          @else
                              No Clicks
                          @endif
                      </td>
                    </tr>
                @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                {!! $emails->render() !!}
            </div>
        </div>
    </section>
@endsection
