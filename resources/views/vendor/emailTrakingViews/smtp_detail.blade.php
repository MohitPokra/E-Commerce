@extends(config('mail-tracker.admin-template.name'))
@section('title')
    Mail Tracker
    @parent
@stop
@section(config('mail-tracker.admin-template.section'))
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <h1 class="col-sm-10">
                Mail Tracker
            </h1>
            <div class="col-sm-2 text-right">
                <h1>
                    <a href="{{route('mailTracker_Index',['page'=>session('mail-tracker-index-page')])}}" class='btn btn-default'>All Sent Emails</a>
                </h1>
            </div>
        </div>
    </section>

    <section class="content-body mt-5">
        <div class="row">
            <div class="col-12">
                <div class="card border-success">
                    <div class="card-header bg-success text-center text-white">
                        <h3 class="card-title d-inline">
                            <i class="fa fa-eyedropper"></i> SMTP detail for Email ID {{$details->id}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <a href="{{ route('mailTracker_ShowEmail',$details->id) }}" class="btn btn-primary" target="_blank">
                                    View Message
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                Recipient: {{$details->recipient}} <br>
                                Subject: {{$details->subject}} <br>
                                Sent At: {{$details->created_at->format(config('mail-tracker.date-format'))}} <br>
                                SMTP Details: {{ $details->smtp_info }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
