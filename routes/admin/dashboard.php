<?php

/*
|--------------------------------------------------------------------------
| Admin Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::group(['middleware' => 'auth'], function ()
Route::group( ['prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::resource('products',  'ProductsController', ['except' => ['show', 'update']]);
    Route::resource('customers', 'UsersController', ['except' => 'update']);
    Route::resource('categories', 'CategoriesController', ['except' => ['show', 'edit', 'update']]);
    Route::resource('brands', 'BrandsController', ['except' => ['show', 'edit', 'update']]);
    Route::resource('orders', 'OrderController', ['except' => ['show', 'edit', 'update']]);

    Route::get('categories/{slug}/edit', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
    Route::put('categories/{slug}/update', ['as' => 'categories.update', 'uses' => 'CategoriesController@update']);

    Route::get('brands/{slug}/edit', ['as' => 'brands.edit', 'uses' => 'BrandsController@edit']);
    Route::put('brands/{slug}/update', ['as' => 'brands.update', 'uses' => 'BrandsController@update']);

    Route::get('products/{slug}/edit', ['as' => 'products.edit', 'uses' => 'ProductsController@edit']);
    Route::put('products/{slug}/update', ['as' => 'products.update', 'uses' => 'ProductsController@update']);

    Route::get('orders/{slug}/edit', ['as' => 'orders.edit', 'uses' => 'OrderController@edit']);
    Route::put('orders/update', ['as' => 'orders.update', 'uses' => 'OrderController@update']);
    Route::put('orders/update/address', ['as' => 'orders.update.address', 'uses' => 'OrderController@addressPost']);
    Route::put('orders/update/shipping-info', ['as' => 'orders.update.shipping.info', 'uses' => 'OrderController@shippingInfoPost']);
    Route::put('orders/update/status', ['as' => 'orders.update.status', 'uses' => 'OrderController@statusUpdate']);

    Route::put('customers/{id}/update-details', ['as' => 'customers.update.detail', 'uses' => 'UsersController@updateDetails']);

    Route::get('datatable/ajax-categories', ['as' => 'ajax_categories', 'uses' => 'CategoriesController@ajax_categories']);
    Route::get('datatable/ajax-brands', ['as' => 'ajax_brands', 'uses' => 'BrandsController@ajax_brands']);
    Route::get('datatable/ajax-products', ['as' => 'ajax_products', 'uses' => 'ProductsController@ajax_products']);
    Route::get('datatable/ajax-customers', ['as' => 'ajax_customers', 'uses' => 'UsersController@ajax_customers']);
    Route::get('datatable/ajax-orders', ['as' => 'ajax_orders', 'uses' => 'OrderController@ajax_orders']);
    Route::get('datatable/ajax-orders', ['as' => 'ajax_orders', 'uses' => 'OrderController@ajax_orders']);
    Route::get('datatable/ajax-static-page', ['as' => 'ajax_static_page', 'uses' => 'StaticPageController@ajaxStaticPage']);
    Route::get('setting/shop', ['as' => 'setting.shop', 'uses' => 'SettingsController@index']);
    Route::post('setting/shop', ['as' => 'setting.shop.store', 'uses' => 'SettingsController@personalizeSetting']);

    Route::get('setting/static-pages', ['as' => 'setting.static.index', 'uses' => 'StaticPageController@index']);
    Route::get('setting/static-pages/{slug}/edit', ['as' => 'setting.static.edit', 'uses' => 'StaticPageController@edit']);
    Route::put('setting/static-pages/{slug}/update', ['as' => 'setting.static.update', 'uses' => 'StaticPageController@update']);
    Route::delete('setting/static-pages/{slug}/destroy', ['as' => 'setting.static.destroy', 'uses' => 'StaticPageController@destroy']);
    Route::get('setting/create/static-pages', ['as' => 'setting.static.create', 'uses' => 'StaticPageController@create']);
    Route::post('setting/static', ['as' => 'setting.static.store', 'uses' => 'StaticPageController@store']);

    Route::post('sale/data/{date}', ['as' => 'sale.data', 'uses' => 'DashboardController@getSalesData']);

    Route::post('user/{user}/two-factor/enable', [
        'as' => 'user.two-factor.enable',
        'uses' => 'UsersController@enableTwoFactorAuth'
    ]);

    Route::post('user/{user}/two-factor/disable', [
        'as' => 'user.two-factor.disable',
        'uses' => 'UsersController@disableTwoFactorAuth'
    ]);





//    Route::get('/test', function () {
//        return view('admin.default');
//    });
//
//    Route::get('/index', function () {
//        return view('admin.index');
//    });
//
//    Route::get('/datatables', function () {
//         return view('admin.datatables');
//    });
//    Route::get('/simple_tables', function () {
//        return view('admin.simple_tables');
//    });
//    Route::get('/advanced_datatables', function () {
//        return view('admin.advanced_datatables');
//    });
//    Route::get('/responsive_datatables', function () {
//        return view('admin.responsive_datatables');
//    });
//
//
//    Route::get('/dashboard', [
//        'as' => 'admin.dashboard',
//        'uses' => 'DashboardController@index'
//    ]);
//
//    /**
//     * User Profile
//     */
//
//    Route::get('profile', [
//        'as' => 'profile',
//        'uses' => 'ProfileController@index'
//    ]);
//
//    Route::get('profile/activity', [
//        'as' => 'profile.activity',
//        'uses' => 'ProfileController@activity'
//    ]);
//
//    Route::put('profile/details/update', [
//        'as' => 'profile.update.details',
//        'uses' => 'ProfileController@updateDetails'
//    ]);
//
//    Route::post('profile/avatar/update', [
//        'as' => 'profile.update.avatar',
//        'uses' => 'ProfileController@updateAvatar'
//    ]);
//
//    Route::post('profile/avatar/update/external', [
//        'as' => 'profile.update.avatar-external',
//        'uses' => 'ProfileController@updateAvatarExternal'
//    ]);
//
//    Route::put('profile/login-details/update', [
//        'as' => 'profile.update.login-details',
//        'uses' => 'ProfileController@updateLoginDetails'
//    ]);
//
//    Route::post('profile/two-factor/enable', [
//        'as' => 'profile.two-factor.enable',
//        'uses' => 'ProfileController@enableTwoFactorAuth'
//    ]);
//
//    Route::post('profile/two-factor/disable', [
//        'as' => 'profile.two-factor.disable',
//        'uses' => 'ProfileController@disableTwoFactorAuth'
//    ]);
//
//    Route::get('profile/sessions', [
//        'as' => 'profile.sessions',
//        'uses' => 'ProfileController@sessions'
//    ]);
//
//    Route::delete('profile/sessions/{session}/invalidate', [
//        'as' => 'profile.sessions.invalidate',
//        'uses' => 'ProfileController@invalidateSession'
//    ]);
//
//    /**
//     * User Management
//     */
//    Route::get('user', [
//        'as' => 'user.list',
//        'uses' => 'UsersController@index'
//    ]);
//
//    Route::get('user/create', [
//        'as' => 'user.create',
//        'uses' => 'UsersController@create'
//    ]);
//
//    Route::post('user/create', [
//        'as' => 'user.store',
//        'uses' => 'UsersController@store'
//    ]);
//
//    Route::get('user/{user}/show', [
//        'as' => 'user.show',
//        'uses' => 'UsersController@view'
//    ]);
//
//    Route::get('user/{user}/edit', [
//        'as' => 'user.edit',
//        'uses' => 'UsersController@edit'
//    ]);
//
//    Route::put('user/{user}/update/details', [
//        'as' => 'user.update.details',
//        'uses' => 'UsersController@updateDetails'
//    ]);
//
    Route::put('user/{id}/update/login-details', [
        'as' =>  'user.update.login-details',
        'uses' => 'UsersController@updateLoginDetails'
    ]);
//
//    Route::delete('user/{user}/delete', [
//        'as' => 'user.delete',
//        'uses' => 'UsersController@delete'
//    ]);
//
//    Route::post('user/{user}/update/avatar', [
//        'as' => 'user.update.avatar',
//        'uses' => 'UsersController@updateAvatar'
//    ]);
//
//    Route::post('user/{user}/update/avatar/external', [
//        'as' => 'user.update.avatar.external',
//        'uses' => 'UsersController@updateAvatarExternal'
//    ]);
//
//    Route::get('user/{user}/sessions', [
//        'as' => 'user.sessions',
//        'uses' => 'UsersController@sessions'
//    ]);
//
//    Route::delete('user/{user}/sessions/{session}/invalidate', [
//        'as' => 'user.sessions.invalidate',
//        'uses' => 'UsersController@invalidateSession'
//    ]);


    /**
     * Roles & Permissions
     */

    Route::get('role', [
        'as' => 'role.index',
        'uses' => 'RolesController@index'
    ]);

    Route::get('role/create', [
        'as' => 'role.create',
        'uses' => 'RolesController@create'
    ]);

    Route::post('role/store', [
        'as' => 'role.store',
        'uses' => 'RolesController@store'
    ]);

    Route::get('role/{role}/edit', [
        'as' => 'role.edit',
        'uses' => 'RolesController@edit'
    ]);

    Route::put('role/{role}/update', [
        'as' => 'role.update',
        'uses' => 'RolesController@update'
    ]);

    Route::delete('role/{role}/delete', [
        'as' => 'role.delete',
        'uses' => 'RolesController@delete'
    ]);


    Route::post('permission/save', [
        'as' => 'permission.save',
        'uses' => 'PermissionsController@saveRolePermissions'
    ]);

    Route::resource('permission', 'PermissionsController');

    /**
     * Settings
     */

    Route::get('settings', [
        'as' => 'settings.general',
        'uses' => 'SettingsController@general',
        'middleware' => 'permission:settings.general'
    ]);

    Route::post('settings/general', [
        'as' => 'settings.general.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'permission:settings.general'
    ]);

    Route::get('settings/auth', [
        'as' => 'settings.auth',
        'uses' => 'SettingsController@auth',
        'middleware' => 'permission:settings.auth'
    ]);

    Route::post('settings/auth', [
        'as' => 'settings.auth.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'permission:settings.auth'
    ]);

// Only allow managing 2FA if AUTHY_KEY is defined inside .env file
    if (env('AUTHY_KEY')) {
        Route::post('settings/auth/2fa/enable', [
            'as' => 'settings.auth.2fa.enable',
            'uses' => 'SettingsController@enableTwoFactor',
            'middleware' => 'permission:settings.auth'
        ]);

        Route::post('settings/auth/2fa/disable', [
            'as' => 'settings.auth.2fa.disable',
            'uses' => 'SettingsController@disableTwoFactor',
            'middleware' => 'permission:settings.auth'
        ]);
    }
//
//    Route::post('settings/auth/registration/captcha/enable', [
//        'as' => 'settings.registration.captcha.enable',
//        'uses' => 'SettingsController@enableCaptcha',
//        'middleware' => 'permission:settings.auth'
//    ]);
//
//    Route::post('settings/auth/registration/captcha/disable', [
//        'as' => 'settings.registration.captcha.disable',
//        'uses' => 'SettingsController@disableCaptcha',
//        'middleware' => 'permission:settings.auth'
//    ]);
//
//    Route::get('settings/notifications', [
//        'as' => 'settings.notifications',
//        'uses' => 'SettingsController@notifications',
//        'middleware' => 'permission:settings.notifications'
//    ]);
//
//    Route::post('settings/notifications', [
//        'as' => 'settings.notifications.update',
//        'uses' => 'SettingsController@update',
//        'middleware' => 'permission:settings.notifications'
//    ]);

});
