<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/25/18
 * Time: 5:22 PM
 */

// Order Checkout
Route::post('/order/checkout', ['as' => 'order.checkout', 'uses' => 'OrderController@checkout']);
Route::get('/order/address/{hash}', ['as' => 'order.address.get', 'uses' => 'OrderController@addressGet']);
Route::get('/order/payment/{hash}', ['as' => 'order.payment.get', 'uses' => 'OrderController@paymentGet']);
Route::get('/order/review/{hash}', ['as' => 'order.review.get', 'uses' => 'OrderController@reviewGet']);
Route::get('/order/complete/{hash}', ['as' => 'order.complete.get', 'uses' => 'OrderController@completeGet']);
Route::get('/order/details/{hash}', ['as' => 'order.details.get', 'uses' => 'OrderController@detailsGet']);
Route::post('/order/address', ['as' => 'order.address.post', 'uses' => 'OrderController@addressPost']);

// Order Tracking
Route::get('/order/tracking/{hash}', ['as' => 'order.tracking.get', 'uses' => 'OrderTrackController@trackingGet']);
