<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('shop.home');

Route::get('/deals', ['as' => 'shop.deals', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('deals');
}]);

Route::get('/shipping-policy', ['as' => 'shop.shipping', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('shipping-policy');
}]);

Route::get('/returns-policy', ['as' => 'shop.returns', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('returns-policy');
}]);

Route::get('/privacy-policy', ['as' => 'shop.privacy', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('privacy-policy');
}]);

Route::get('/our-story', ['as' => 'shop.story', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('our-story');
}]);

Route::get('/faq', ['as' => 'shop.faq', function () {
    return App::make('App\Http\Controllers\Web\PageController')->render('faq');
}]);


