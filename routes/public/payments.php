<?php
/**
 * Created by PhpStorm.
 * User: froiden
 * Date: 5/29/18
 * Time: 6:49 PM
 */

Route::post('/payments/stripe', ['as' => 'payments.stripe', 'uses' => 'PaymentController@stripePayment']);
Route::post('/payments/paypal', ['as' => 'payments.paypal', 'uses' => 'PaymentController@payPalPayment']);
Route::get('/payments/paypal/callback/{hash}', ['as' => 'payments.paypal.callback', 'uses' => 'PaymentController@payPalCallback']);
