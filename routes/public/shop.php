<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::post('/affiliates', ['as' => 'affiliates', 'uses' => 'AffilateController@index');
Route::get('/contact-us', ['as' => 'contact', 'uses' => 'ContactController@show']);

//Route::get('/blog', ['as' => 'blog', 'uses' => 'BlogController@show']);

Route::get('/search', ['as' => 'shop.search', 'uses' => 'ProductsController@search']);
Route::get('/shop', ['as' => 'products.all', 'uses' => 'ProductsController@all']);
Route::get('/shop/categories', ['as' => 'products.categories', 'uses' => 'ProductsController@categories']);
Route::get('/shop/brands', ['as' => 'products.brands', 'uses' => 'ProductsController@brands']);
Route::get('/shop/category/{category}', ['as' => 'products.category', 'uses' => 'ProductsController@category']);
Route::get('/shop/brand/{brand}', ['as' => 'products.brand', 'uses' => 'ProductsController@brand']);
Route::get('/shop/product/{category}/{product}', ['as' => 'product.show', 'uses' => 'ProductsController@product']);

Route::post('/newsletter', ['as' => 'newsletter.subscribe', 'uses' => 'NewsletterController@subscribe']);
