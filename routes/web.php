<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => 'auth',
], function () {
    Route::post('/review', ['as' => 'review.store', 'uses' => 'ReviewController@store']);
    Route::get('/cart', ['as' => 'cart', 'uses' => 'CartController@cart']);
    Route::post('/cart/add', ['as' => 'cart.add', 'uses' => 'CartController@add']);
    Route::delete('/cart/remove/{id}', ['as' => 'cart.remove', 'uses' => 'CartController@remove']);
    Route::get('/account-profile', ['as' => 'account.profile', 'uses' => 'AccountController@profile']);
    Route::get('/account-orders', ['as' => 'account.orders', 'uses' => 'AccountController@order']);
//    Route::get('/account-tickets', ['as' => 'account.tickets', 'uses' => 'AccountController@tickets']);
//    Route::get('/account-single-ticket', ['as' => 'account.singleTickets', 'uses' => 'AccountController@singleTickets']);
    Route::get('/account-address', ['as' => 'account.address', 'uses' => 'AccountController@address']);
//    Route::get('/account-wishlist', ['as' => 'account.wishList', 'uses' => 'AccountController@wishList']);
    Route::post('/address/update', ['as' => 'address.update', 'uses' => 'AddressController@update']);
});

Route::group([], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/pages/{slug}', ['as' => 'static.page.show', 'uses' => 'StaticPageController@show']);
    Route::get('/product/{slug}', ['as' => 'shopSingle', 'uses' => 'CartController@shopSingle']);
});
